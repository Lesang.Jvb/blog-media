<?php echo $this->Form->create(null, array('url' => DOMAINAD . 'order/add', 'type' => 'post', 'enctype' => 'multipart/form-data', 'name' => 'adminForm')); ?>
<div id="khung">
    <div id="main">
        <div class="toolbar-list" id="toolbar">
            <ul>
                <li id="toolbar-new"> <a href="javascript:void(0);" onclick="javascript:document.adminForm.submit();" class="toolbar"> <span class="icon-32-save"></span> Lưu </a> </li>
                <li id="toolbar-refresh"> <a href="javascript:void(0);" class="toolbar" onclick="javascript:document.adminForm.reset();"> <span class="icon-32-refresh"> </span> Reset </a> </li>
                <li class="divider"></li>
                <li id="toolbar-help"> <a href="#messages" rel="modal" class="toolbar"> <span class="icon-32-help"></span> Trợ giúp </a> </li>
                <li id="toolbar-unpublish"> <a href="<?php echo DOMAINAD; ?>order" class="toolbar"> <span class="icon-32-cancel"></span> Hủy </a> </li>
            </ul>
            <div class="clr"></div>
        </div>
        <div class="pagetitle icon-48-category-add">
            <h2>Thêm mới Order</h2>
        </div>
        <div class="clr"></div>
    </div>
</div>
<div class="content-box"><!-- Start Content Box -->
    <div class="content-box-header">
        <h3>Thêm mới Order</h3>
        <div class="clear"></div>
    </div>
    <!-- End .content-box-header -->
    <div class="content-box-content">
        <div class="tab-content default-tab" id="tab1">
            <table class="input">
                <tr>
                    <td width="120" class="label">Số điện thoại:</td>
                    <td><?php echo $this->Form->input('Order.phone', array('label' => '', 'class' => 'text-input medium-input', 'maxlength' => '60', 'onchange' => 'get_alias()', 'id' => 'idphone')); ?>
                    
                    </td>
                </tr>
				
               <tr>
                    <td width="120" class="label">Tên người order:</td>
                    <td><?php echo $this->Form->input('Order.name', array('label' => '', 'class' => 'text-input medium-input', 'maxlength' => '255', 'id' => 'idname')); ?></td>
               </tr>
               <tr>
                    <td width="120" class="label">Địa chỉ:</td>
                    <td><?php echo $this->Form->input('Order.address', array('label' => '', 'class' => 'text-input medium-input', 'maxlength' => '255', 'id' => 'idaddress')); ?></td>
               </tr>
               <tr>
                    <td width="120" class="label">Ghi chú:</td>
                    <td><?php echo $this->Form->input('Order.note', array('label' => '', 'class' => 'text-input hight-input', 'maxlength' => '255', 'id' => 'idnote')); ?></td>
               </tr>
               <tr>
                    <td width="120" class="label">Số lượng:</td>
                    <td><?php echo $this->Form->input('Order.quantity', array('label' => '', 'class' => 'text-input medium-input', 'maxlength' => '255', 'id' => 'idquantity')); ?></td>
               </tr>
               <tr>
                    <td width="120" class="label">Shipping:</td>
                    <td><?php echo $this->Form->input('Order.shipping', array('label' => '', 'class' => 'text-input medium-input', 'maxlength' => '255', 'id' => 'idshipping')); ?></td>
               </tr>
               <tr>
                    <td width="120" class="label">Tình trạng:</td>
                    <td><?php echo $this->Form->select('Order.status', array('1' => 'Thành công', '0' => 'Chưa duyệt','2' => 'Hủy đơn')); ?></td>
               </tr>
            </table>
            <div class="clear"></div>
        </div>
        <!-- End #tab1 -->
        <div class="tab-content" id="tab2">
            <div class="clear"></div>
            <!-- End .clear --> 
        </div>
        <!-- End #tab2 --> 
    </div>
    <!-- End .content-box-content --> 
</div>
<div id="khung">
    <div id="main">
        <div class="toolbar-list" id="toolbar">
            <ul>
                <li id="toolbar-new"> <a href="javascript:void(0);" onclick="javascript:document.adminForm.submit();" class="toolbar"> <span class="icon-32-save"></span> Lưu </a> </li>
                <li id="toolbar-refresh"> <a href="javascript:void(0);" class="toolbar" onclick="javascript:document.adminForm.reset();"> <span class="icon-32-refresh"> </span> Reset </a> </li>
                <li class="divider"></li>
                <li id="toolbar-help"> <a href="#messages" rel="modal" class="toolbar"> <span class="icon-32-help"></span> Trợ giúp </a> </li>
                <li id="toolbar-unpublish"> <a href="<?php echo DOMAINAD; ?>Order" class="toolbar"> <span class="icon-32-cancel"></span> Hủy </a> </li>
            </ul>
            <div class="clr"></div>
        </div>
        <div class="pagetitle icon-48-category-add">
            <h2>Thêm mới Order</h2>
        </div>
        <div class="clr"></div>
    </div>
</div>
<?php echo $this->Form->end(); ?>