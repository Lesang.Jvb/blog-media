<script type="text/javascript">
    function process() {
        document.frm1.action = "<?php echo DOMAINAD; ?>order/process";
        document.frm1.submit();
    }
    function MM_jumpMenu(targ,selObj,restore){ //v3.0
        eval(targ+".location='"+selObj.options[selObj.selectedIndex].value+"'");
        if (restore) selObj.selectedIndex=0;
    }
</script>
<?php echo $this->Form->create(null, array('url' => DOMAINAD . 'order/search', 'type' => 'get', 'name' => 'frm1')); ?>
<div id="khung">
    <div id="main">
        <div class="toolbar-list" id="toolbar">
            <ul>
                <li id="toolbar-new"> <a href="<?php echo DOMAINAD; ?>order/add" class="toolbar"> <span class="icon-32-new"></span> Thêm mới </a> </li>
                <li class="divider"></li>
                <li id="toolbar-help"> <a href="#messages" rel="modal" class="toolbar"> <span class="icon-32-help"></span> Trợ giúp </a> </li>
                <li id="toolbar-unpublish"> <a href="<?php echo DOMAINAD; ?>home" class="toolbar"> <span class="icon-32-unpublish"></span> Đóng </a> </li>
            </ul>
            <div class="clr"></div>
        </div>
        <div class="pagetitle icon-48-nhomtin">
            <h2>Order</h2>
        </div>
        <div class="clr"></div>
    </div>
</div>
<div class="content-box">
    <div class="content-box-header">
        <table class="timkiem">
            <tr>
                <td valign="top">Tìm kiếm</td>
                <td><select name="listCat" id="jumpMenu">
                        <option value="">--- Chọn sản phẩm ---</option>
                         <?php foreach ($list_news as $k => $v) { ?>
                        <option value="<?php echo $k; ?>" <?php if ($this->Session->read('catId') == $k) {echo 'selected="selected"'; } ?>><?php echo $v; ?></option>
                        <?php } ?>
                    </select></td>
                <td><input type="submit" name="" value="Tìm kiếm" class="button" /></td>
            </tr>
        </table>
        <!--<ul class="content-box-tabs">
            <li><a href="#tab1" class="default-tab">Danh sách tin</a></li>
            <li><a href="#tab2"></a></li>
        </ul>-->
        <div class="clear"></div>
    </div>
    <div class="content-box-content">
   <div class="tab-content default-tab" id="tab1">
            <table width="100%">
                    <thead>
                        <tr>
                            <th width="2%"><input type="checkbox" name="all" id="checkall" /></th>
                            <th width="4%">STT</th>
                            <th width="12%" style="text-align:center;">Số điện thoại</th>
                            <th width="12%" style="text-align:center;">Họ tên</th>
                            <th width="12%" style="text-align:center;">Địa chỉ</th>
                            <th width="12%" style="text-align:center;">Sản phẩm</th>
                            <th width="12%" style="text-align:center;">Thời gian</th>
                            <th width="12%" style="text-align:center;">Tình trạng</th> 
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <td colspan="9"><div class="bulk-actions align-left">
                                    <select name="process">
                                        <option value="5">Lựa chọn</option>
                                        <option value="1">Active</option>
                                        <option value="0">Chưa duyệt</option>
                                        <option value="2">Hủy Đơn</option>
                                        <option value="3">Delete</option>
                                    </select>
                                    <a class="button" href="#" onclick="process()">Thực hiện</a> </div>
                                <div class="pagination">
                                    <?php
                                        echo $this->Paginator->first('« Đầu ', null, null, array('class' => 'disabled'));     
                                        echo $this->Paginator->prev('« Trước ', null, null, array('class' => 'disabled')); 
                                        echo $this->Paginator->numbers()." ";
                                        echo $this->Paginator->next(' Tiếp »', null, null, array('class' => 'disabled')); 
                                        echo $this->Paginator->last('« Cuối ', null, null, array('class' => 'disabled')); 
                                        echo " Page ".$this->Paginator->counter();
                                    ?>
                                </div>
                                <div class="clear"></div></td>
                        </tr>
                    </tfoot>
                    <tbody>
                        <?php foreach ($order as $key => $value) { ?>
                        <tr>
                            <td><input type="checkbox" name="chon[<?php echo $value['Order']['id']; ?>]" value="1" /></td>
                            <td><?php echo $key + $startPage;?></td>
                            <td style="text-align:center;"><?php echo $value['Order']['phone']; ?></td>
                            <td style="text-align:center;"><?php echo $value['Order']['name']; ?> </td>
                            <td style="text-align:center;"><?php echo $value['Order']['address']; ?></td>
                            <td style="text-align:center;"><?php echo $value['News']['name']; ?></td>
                            <td style="text-align:center;"><?php echo date('d-m-Y', $value['Order']['timelog']); ?></td>
							
							
                            <td>
                                <img src="<?php echo DOMAINAD ?>images/icons/pencil.png" alt="Edit" /></a> 
								<a href="javascript:confirmDelete('<?php echo DOMAINAD ?>order/delete/<?php echo $value['Order']['id']; ?>')" title="Delete"><img src="<?php echo DOMAINAD ?>images/icons/cross.png" alt="Delete" /></a>
                                <?php
                                    if ($value['Order']['status'] == 0) {
                                        ?>
                                <a href="<?php echo DOMAINAD ?>order/active/<?php echo $value['Order']['id']; ?>" title="Thành công" class="icon-5 info-tooltip"><img src="<?php echo DOMAINAD ?>images/icons/Play-icon.png" alt="Thành công" /></a>
                                <?php
                                    } else if ($value['Order']['status'] == 2){
                                        ?>
                                <a href="<?php echo DOMAINAD ?>order/close/<?php echo $value['Order']['id']; ?>" title="Chưa duyệt" class="icon-4 info-tooltip"><img src="<?php echo DOMAINAD ?>images/icons/cross_circle.png" alt="Chưa duyệt" /></a>
                                <?php
                                    } else if ($value['Order']['status'] == 1) { ?>
                                <a href="<?php echo DOMAINAD ?>order/deactive/<?php echo $value['Order']['id']; ?>" title="Hủy đơn" class="icon-4 info-tooltip"><img src="<?php echo DOMAINAD ?>images/icons/success-icon.png" alt="Hủy đơn" /></a>
                                    <?php } ?></td>
                        </tr>
                        <?php } ?>
                    </tbody>
            </table>
        </div>
    </div>
</div>
<?php echo $this->Form->end(); ?>