<?php echo $this->Form->create(null, array('url' => DOMAINAD . 'administrators/edit', 'type' => 'post', 'name' => 'adminForm')); ?>
<style>
.table1 td{text-align:center; border:1px solid #dedede; }
.table1{
	border:1;
	border-color:#dedede;
}
</style>
<script>
$(function(){
	$('#selectq').change(function(){
		var a= $(this).val();
		if(a==1) {
			$('#tr_q').hide();
		} else $('#tr_q').show();
	});
		var test=$('#selectq').val();
	if(test=='' || test==1) $('#tr_q').hide();
	else $('#tr_q').show();
})
</script>
<div id="khung">
    <div id="main">
        <div class="toolbar-list" id="toolbar">
            <ul>
                <li id="toolbar-new">
                    <a href="javascript:void(0);" onclick="javascript:document.adminForm.submit();" class="toolbar">
                        <span class="icon-32-save"></span>
                        Lưu
                    </a>
                </li>
                <li id="toolbar-refresh">
                    <a href="javascript:void(0);" class="toolbar" onclick="javascript:document.adminForm.reset();">
                        <span class="icon-32-refresh">
                        </span>
                        Reset
                    </a>
                </li>
                <li class="divider"></li>
                <li id="toolbar-help">
                    <a href="#messages" rel="modal" class="toolbar">
                        <span class="icon-32-help"></span>
                        Trợ giúp
                    </a>
                </li>
                <li id="toolbar-unpublish">
                    <a href="<?php echo DOMAINAD ?>administrators" class="toolbar">
                        <span class="icon-32-cancel"></span>
                        Hủy
                    </a>
                </li>
            </ul>
            <div class="clr"></div>
        </div>
        <div class="pagetitle icon-48-category-add"><h2>Tài khoản</h2></div>
        <div class="clr"></div>
    </div>
</div>
<div class="content-box"><!-- Start Content Box -->
    <div class="content-box-header">
        <h3> Thêm mới </h3>
        <ul class="content-box-tabs">
            <li><a href="#tab1" class="default-tab">Sửa</a></li> <!-- href must be unique and match the id of target div -->
        </ul>
        <div class="clear"></div>
    </div> <!-- End .content-box-header -->
    <div class="content-box-content">
        <div class="tab-content default-tab" id="tab1">
            <table class="input">
               	<tr>
                   	<td width="140" class="label">Tên đăng nhập:</td>
                    <td>
                        <?php echo $this->Form->input('Administrator.name', array('label' => '', 'class' => 'text-input medium-input datepicker', 'maxlength' => '250', 'id' => 'name')); ?>
                        <?php echo $this->Form->input('Administrator.id'); ?>
                    </td>
                </tr>
               
                <tr>
                   	<td width="140" class="label">Mật khẩu:</td>
                    <td>
                        <?php echo $this->Form->input('Administrator.password', array('label' => '', 'class' => 'text-input medium-input datepicker', 'type' => 'password', 'maxlength' => '250', 'id' => 'pass1')); ?>
                    </td>
                </tr>
                <tr>
                   	<td width="140" class="label">Nhập lại mật khẩu:</td>
                    <td>
                        <?php echo $this->Form->input('Administrator.pass2', array('label' => '', 'class' => 'text-input medium-input datepicker', 'type' => 'password', 'maxlength' => '250', 'id' => 'pass2','value'=>$edit['Administrator']['password'])); ?>
                    </td>
                </tr>
				  <tr>
                   	<td width="140" class="label">Thuộc:</td>
                    <td>
                       <select id="selectq" name="data[Administrator][role]">
							<option <?php if($edit['Administrator']['role']) echo 'selected="selected"';?> value="1">Admin</option>
					<!--		<option <?php if($edit['Administrator']['role']==0) echo 'selected="selected"';?> value="0">User</option>
					  --> </select>
                    </td>
                </tr>
				 <tr id="tr_q">
                   	<td width="140" class="label">Chọn quyền cho user:</td>
                    <td>
                        <table class="table1" style="width:80% !important" border="1">
                          <tr>
                             <td>#</td> 
							 <td>Duyệt </br><input type="checkbox" name="all" id="checkall1" /></td>
							 
							 <td>Xóa</br><input type="checkbox" name="all" id="checkall2" /></td>
							 <td>Sửa </br><input type="checkbox" name="all" id="checkall3" /></td>
							 <td>Thêm</br><input type="checkbox" name="all" id="checkall4" /></td>
							 <td>Truy nhập</br><input type="checkbox" name="all" id="checkall5" /></td>
                            <td>Modul</td>
                          </tr>
						  <?php $quyen=explode('|',$edit['Administrator']['quyen']);
						  $mang=array();$j=0; $modul=array(); $n=1;
								for($i=0;$i<count($quyen)-1;$i++){
									$bien=explode('_',$quyen[$i]);
									$bien=explode('-',$bien[1]);
									$mang[$j++]=$bien[0];
									$mang[$j++]=$bien[1];
									$mang[$j++]=$bien[2];
									$mang[$j++]=$bien[3];
									$mang[$j++]=$bien[4];
								
								}
							//pr($mang); die;
							$i=1;
							$name[$i++]='Danh mục sản phẩm';
							$name[$i++]='Sản phẩm';
							$name[$i++]='Danh mục thư viện ảnh';
							$name[$i++]='Thư viện ảnh';
							$name[$i++]='Danh mục tin tức';
							$name[$i++]='Tin tức';
							$name[$i++]='Danh mục hỗ trợ';
							$name[$i++]='Hỗ trợ';
							$name[$i++]='Giới thiệu';
							$name[$i++]='Video';
							$name[$i++]='Quảng cáo 2 bên';
							$name[$i++]='Hỗ trợ trực tuyến';
							$name[$i++]='Câu hỏi khách hàng';
							$name[$i++]='Quản lý liên kết';
							$name[$i++]='Banner + quảng cáo + giải thưởng';
							$name[$i++]='Quản lý user';
							$i=1;
							$modul[$i++]='catproducts';
							$modul[$i++]='products';
							$modul[$i++]='cataloguesrec';
							$modul[$i++]='recruitment';
							$modul[$i++]='danhmucs';
							$modul[$i++]='tintucs';
							$modul[$i++]='catalogues';
							$modul[$i++]='news';
							$modul[$i++]='posts';
							$modul[$i++]='tins';
							$modul[$i++]='advertisements';
							$modul[$i++]='supports';
							$modul[$i++]='emaildk';
							$modul[$i++]='doitac';
							$modul[$i++]='partner';
							$modul[$i++]='usermember';
							
						   $j=0;
						  ?>
						 <?php  for($i=1;$i<17;$i++) { ?> 
                          <tr>
                             <td><?php echo $i?></td> 
							 <input type="hidden" name="modul<?php echo $i?>" value="<?php echo $modul[$i];?>" />
							 <td><input type="checkbox" <?php if(isset($mang[$j]) && $mang[$j++]==1) echo 'checked';?> class="col1" name="sub1<?php echo $i?>" value="1" /></td>
							 
							 <td><input type="checkbox" <?php if(isset($mang[$j]) && $mang[$j++]==1) echo 'checked';?> class="col2" name="sub2<?php echo $i?>" value="1" /></td>
							 <td><input type="checkbox" <?php if(isset($mang[$j]) && $mang[$j++]==1) echo 'checked';?> class="col3" name="sub3<?php echo $i?>" value="1" /></td>
							 <td><input type="checkbox" <?php if(isset($mang[$j]) && $mang[$j++]==1) echo 'checked';?> class="col4" name="sub4<?php echo $i?>" value="1" /></td>
							 <td><input type="checkbox" <?php if(isset($mang[$j]) && $mang[$j++]==1) echo 'checked';?> class="col5" name="sub5<?php echo $i?>" value="1" /></td>
							  <td><?php echo $name[$i]?></td>
                          </tr>
						  <?php } ?>
				
					
                      </table>
                    </td>
                </tr>
				
				
            </table>
            <div class="clear"></div>
        </div> <!-- End #tab1 -->
        <div class="tab-content" id="tab2">
            <div class="clear"></div><!-- End .clear -->
        </div> <!-- End #tab2 -->        
    </div> <!-- End .content-box-content -->
</div>

<div id="khung">
    <div id="main">
        <div class="toolbar-list" id="toolbar">
            <ul>
                <li id="toolbar-new">
                    <a href="javascript:void(0);" onclick="javascript:document.adminForm.submit();" class="toolbar">
                        <span class="icon-32-save"></span>
                        Lưu
                    </a>
                </li>
                <li id="toolbar-refresh">
                    <a href="javascript:void(0);" class="toolbar" onclick="javascript:document.adminForm.reset();">
                        <span class="icon-32-refresh">
                        </span>
                        Reset
                    </a>
                </li>
                <li class="divider"></li>
                <li id="toolbar-help">
                    <a href="#messages" rel="modal" class="toolbar">
                        <span class="icon-32-help"></span>
                        Trợ giúp
                    </a>
                </li>
                <li id="toolbar-unpublish">
                    <a href="<?php echo DOMAINAD ?>administrators" class="toolbar">
                        <span class="icon-32-cancel"></span>
                        Hủy
                    </a>
                </li>
            </ul>
            <div class="clr"></div>
        </div>
        <div class="pagetitle icon-48-category-add"><h2>Tài khoản</h2></div>

        <div class="clr"></div>
    </div>
</div>
<?php echo $this->Form->end(); ?>