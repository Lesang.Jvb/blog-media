<?php 
// create new empty worksheet and set default font
$this->PhpExcel->createWorksheet()
->setDefaultFont('Calibri', 12);

// define table cells
$table = array(
    array('label' => __('STT')),
    array('label' => __('Họ tên')),
    array('label' => __('Email')),
    array('label' => __('Số điện thoại')),
    array('label' => __('Trình độ')),
    array('label' => __('Ngày gửi')),
    array('label' => __('ID')),
    );

// add heading with different font and bold text
$this->PhpExcel->addTableHeader($table, array('name' => 'Cambria', 'bold' => true));

// add data
$i = 1;
foreach ($contacts as $row) {
    $this->PhpExcel->addTableRow(array(
		$i++,
        $row['Contact']['name'],
        $row['Contact']['email'],
        $row['Contact']['mobile'],
        $row['Contact']['trinhdo'],
        $row['Contact']['created'],
        $row['Contact']['id'],
        ));
}

// close table and output
$this->PhpExcel->addTableFooter()
->output();
?>