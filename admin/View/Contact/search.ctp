<?php echo $this->Form->create(null, array('url' => DOMAINAD . 'contacts/search', 'type' => 'post', 'name' => 'frm1')); ?>
<div id="khung">
	<div id="main">
		<div class="toolbar-list" id="toolbar">
                    <ul>
                       
                        <li class="divider"></li>
                        <li id="toolbar-help">
                            <a href="#messages" rel="modal" class="toolbar">
                                <span class="icon-32-help"></span>
                                Trợ giúp
                            </a>
                        </li>
                        <li id="toolbar-unpublish">
                            <a href="<?php echo DOMAINAD?>home" class="toolbar">
                                <span class="icon-32-unpublish"></span>
                                Đóng
                            </a>
                        </li>
                        <li id="toolbar-help">
                    <a href="<?= DOMAINAD ?>/Contacts/exportToExcel" target="_blank" rel="modal" class="toolbar">
                        <img src="<?= DOMAINAD ?>/images/export_file.png" width="32px" height="25px">
                        <p>Xuất Excel</p>
                    </a>
                </li>
                    </ul>
                    <div class="clr"></div>
                </div>
        <div class="pagetitle icon-48-nhomtin"><h2>Danh sách đăng ký</h2></div>
		<div class="clr"></div>
	</div>
</div>
<div class="content-box">
    <div class="content-box-header">
       <table class="timkiem">
            <tr>
                <td valign="top">Tìm kiếm</td>
                <td><input type="text" id="field2c" name="keyword" class="text-input" value=""></td>
                <td><select name="listCat" id="jumpMenu">
                        <option value="">--- Tất cả các danh mục ---</option>
                        <?php foreach ($list_product as $k => $v) { ?>
                        <option value="<?php echo $k; ?>"><?php echo $v; ?></option>
                        <?php } ?>
                    </select></td>
                <td><input type="submit" name="" value="Tìm kiếm" class="button" /></td>
            </tr>
        </table>
        <ul class="content-box-tabs">
            <li><a href="#tab1" class="default-tab"></a></li> 
            <li><a href="#tab2"></a></li>
        </ul>
        <div class="clear"></div>
    </div>
    <div class="content-box-content">
        <div class="tab-content default-tab" id="tab1"> 
            <table>
             <form action="<?php echo DOMAINAD; ?>Contacts/processing" name="form1" method="post">
            	<thead>
				
                    <tr>
                      <th width="2%"><input type="checkbox" name="all" id="checkall" /></th>
                       <th width="7%">STT</th>
					    <th>Ngày gửi</th>
						
                       <th>Tên người gửi</th>
                       <th>Email</th>
	  <th>Bài viết </th>
                       <th>Điện thoại</th>
				    
                       <th>Ghi chú</th> 
                       <th width="3%">ID</th>
                    </tr>
                </thead>
             
               
                <tbody>
                   <?php  foreach ($contact as $key =>$value){?>
                    <tr>
                        <td><input type="checkbox" name="<?php echo $value['Contact']['id']; ?>" /></td>
                        <td><?php $j=$key+1; echo $j;?></td>
						 <td><?php echo date('d-m-Y', strtotime($value['Contact']['created'])); ?></td>
						
                        <td><a href="<?php echo DOMAINAD?>contacts/view/<?php echo $value['Contact']['id']?>"><?php echo $value['Contact']['name']; ?></a></td>
                        <td><?php  echo $value['Contact']['email'];?></td>
						<td><?php  echo $value['Contact']['product_id'];?></td>
						 <td><?php  echo $value['Contact']['mobile'];?></td>
                        <td><?php  echo $value['Contact']['comment'];?></td>
					
                      <!--   <td>
                             
                             <a href="javascript:confirmDelete('<?php echo DOMAINAD?>Contacts/delete/<?php echo $value['Contact']['id'] ?>')" title="Delete"><img src="<?php echo DOMAINAD?>images/icons/cross.png" alt="Delete" /></a>
                     
                        </td> -->
                        <td align="right"><?php echo $value['Contact']['id'];?></td>
                    </tr>
                   <?php }?>
                </tbody>
				</form>
            </table>
        </div> <!-- End #tab1 -->
        <!-- End #tab2 -->        
    </div> <!-- End .content-box-content -->
 </div>
<?php echo $this->Form->end(); ?>