<?php echo $this->Form->create(null, array( 'url' => DOMAINAD.'contacts/view','type' => 'post','enctype'=>'multipart/form-data','name'=>'adminForm')); ?> <style>
 table{
	 text-align:left !important;
	 border:1px solid #999 !important;
	 }
 table td{
	 border:1px solid #999 !important;
	 padding-left:20px;
	 }
</style>
<div id="khung">
    <div id="main">
        <div class="toolbar-list" id="toolbar">
            <ul>
                <li id="toolbar-new"> <a href="javascript:void(0);" onclick="javascript:document.adminForm.submit();" class="toolbar"> <span class="icon-32-save"></span> Lưu </a> </li>
                <li id="toolbar-refresh"> <a href="javascript:void(0);" class="toolbar" onclick="javascript:document.adminForm.reset();"> <span class="icon-32-refresh"> </span> Reset </a> </li>
                <li class="divider"></li>
                <li id="toolbar-help"> <a href="#messages" rel="modal" class="toolbar"> <span class="icon-32-help"></span> Trợ giúp </a> </li>
                <li id="toolbar-unpublish"> <a href="<?php echo DOMAINAD?>products" class="toolbar"> <span class="icon-32-cancel"></span> Hủy </a> </li>
            </ul>
            <div class="clr"></div>
        </div>
        <div class="pagetitle icon-48-category-add">
            <h2>Cập nhật </h2>
        </div>
        <div class="clr"></div>
    </div>
</div>
 <div id="news">
  <div id="title-news"><p>Chi tiết đăng ký</p></div>
     <div class="list-news">
    
        <?php
            echo $this->Html->script(array('ckeditor/ckeditor','ckfinder/ckfinder'));
        ?>
             <?php echo $this->Form->input('Contact.id',array('type'=>'hidden'));?>
            <table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table">
			      <tr>
                <td>Tiêu đề: </td>
                <td>
                  <?php echo $views['Contact']['title'];?>
                </td>
              </tr>
			 
			  <tr>
                <td width="250">Họ tên:</td>
                <td><?php echo $views['Contact']['name']?></td>
              </tr>      
			  <?php if($views['Contact']['email']){?>
      
              <tr >
                <td>Email: </td>
                <td>                      
                    <?php echo $views['Contact']['email']; ?>
                    
                </td>
              </tr>
             
            		  <?php } ?> <?php if($views['Contact']['mobile']){?>
			     <tr class="alternate-row">
                <td>Điện thoại:  </td>
                <td>
               <?php echo $views['Contact']['mobile']; ?>
                </td>
              </tr>		  <?php } ?> <?php if($views['Contact']['address']){?>
           <tr class="alternate-row">
                <td>Địa chỉ </td>
                <td>
               <?php echo $views['Contact']['address']; ?>
                </td>
              </tr>  <?php } ?> 
			  <?php if($views['Contact']['product']){?>
			   <tr class="alternate-row">
                <td>Sản phẩm</td>
                <td>
               <?php echo $views['Contact']['product']; ?>
                </td>
              </tr>
			  <?php } ?> <?php if($views['Contact']['soluong']){?>
			   <tr class="alternate-row">
                <td>Số lượng</td>
                <td>
               <?php echo $views['Contact']['soluong']; ?>
                </td>
              </tr> <?php } ?>
			 
			        <tr>
                    <td class="label">Ghi chú:</td>
                    <td><?php
                            $CKEditor = new CKEditor();
                            $CKEditor->config['width'] = '98%';
                            $CKEditor->config['height'] = '300';
                            CKFinder::SetupCKEditor( $CKEditor ) ;
                            
                            $initialValue = $views['Contact']['note'];
                            echo $CKEditor->editor("data[Contact][note]", $initialValue, "");
                        ?></td>
                </tr> 
            </table>
            <!--  end product-table................................... -->
         
  </div>
</div>   <?php echo $this->Form->end(); ?>    