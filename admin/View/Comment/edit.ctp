<script type="text/javascript">

    $(function() {

        var i = $('input').size() + 1;

        $('a.add').click(function() {

            $('<p style="padding-left: 9px;" id="anh'+i+'"><input name="userfileplus[]" id="userfile" type="file"  size="50"></p>').animate({ opacity: "show" }, "slow").appendTo('#themanh');
            i++;
        });

        $('a.remove').click(function() {
	
            if(i > 2) {
                $('#anh'+i+':last').animate({opacity:"hide"}, "slow").remove();
                i--;
            }

        });
	
    });

</script>
<?php echo $this->Form->create(null, array( 'url' => DOMAINAD.'comment/edit','type' => 'post','enctype'=>'multipart/form-data','name'=>'adminForm')); ?>

<div id="khung">
    <div id="main">
        <div class="toolbar-list" id="toolbar">
            <ul>
                <li id="toolbar-new"> <a href="javascript:void(0);" onclick="javascript:document.adminForm.submit();" class="toolbar"> <span class="icon-32-save"></span> Lưu </a> </li>
                <li id="toolbar-refresh"> <a href="javascript:void(0);" class="toolbar" onclick="javascript:document.adminForm.reset();"> <span class="icon-32-refresh"> </span> Reset </a> </li>
                <li class="divider"></li>
                <li id="toolbar-help"> <a href="#messages" rel="modal" class="toolbar"> <span class="icon-32-help"></span> Trợ giúp </a> </li>
                <li id="toolbar-unpublish"> <a href="<?php echo DOMAINAD?>comment" class="toolbar"> <span class="icon-32-cancel"></span> Hủy </a> </li>
            </ul>
            <div class="clr"></div>
        </div>
        <div class="pagetitle icon-48-category-add">
            <h2>Cập nhật bình luận</h2>
        </div>
        <div class="clr"></div>
    </div>
</div>
<div class="content-box"><!-- Start Content Box -->
    <div class="content-box-header">
        <h3>Cập nhật bình luận</h3>
        <div class="clear"></div>
    </div>
    <!-- End .content-box-header -->
    <div class="content-box-content">
        <div class="tab-content default-tab" id="tab1">
            <table class="input">
                <tr>
                    <td width="120" class="label">Tên người bình luận:</td>
                    <td><?php echo $this->Form->input('Comment.name',array('label'=>'','class'=>'text-input medium-input','maxlength' => '60','onchange' => 'get_alias()','id' => 'idtitle'));?>
					
					<?php echo $this->Form->input('Comment.id',array('type'=>'hidden'));?></td>
                </tr>
            <tr>
                    <td width="120" class="label">Sô phút đã cmt:</td>
                    <td><?php echo $this->Form->input('Comment.time',array('label'=>'','class'=>'text-input medium-input','maxlength' => '255','id' => 'idtitle'));?></td>
                </tr>
				  
               <td><input type="checkbox" name="all" id="checkall" /> Chọn tất cả</td>
					
				<tr> <td colspan="2"> Chọn các danh mục
				  <div style="overflow:hidden; border:1px solid;">
                      <table width="20%" style="width:20% !important;" border="0">
					  <?php $cat=explode('|',$edit['Comment']['news_id']);
					 // pr($cat);die;
					  ?>
					  
					  <?php foreach($arrNews as $k=>$v) {?>
                          <tr>
                             <td><label style="color:red"><?php echo $v?></label></td>
                             <td><input type="checkbox" <?php for($i=0; $i<count($cat); $i++) if($cat[$i]==$k) { ?> checked="checked" <?php break; } ?> name="cat_<?php echo $k?>" /> </td>
                          </tr>
						  
                       <?php } ?>
                      </table>
                    
                    </div>
					</td>
				</tr>
             <tr>
                    <td class="label">&nbsp;</td>
                    <td>
                        <img src="<?php echo IMAGEAD; ?>comments/<?php echo $edit['Comment']['images']; ?>" height="100">
                        <?php echo $this->Form->input('id', array('type' => 'hidden')); ?>
                        <input name="oldimg" type="hidden" id="imgid" value="<?php echo $edit['Comment']['images']; ?>">
                    </td>
                </tr>
                <tr>
                    <td class="label">Hình ảnh đại diện:</td>
                    <td>&nbsp;
                        <input name="userfile" type="file" id="userfile" size="50"></td>
                </tr>
			  
			    <tr>
                    <td class="label">Nội dung bình luận :</td>
                    <td>
                        <?php echo $this->Form->input('Comment.content',array('label'=>'','class'=>'text-input medium-input'));?>
                        </td>
                </tr>
				
              
            </table>
            <div class="clear"></div>
        </div>
        <!-- End #tab1 -->
        <div class="tab-content" id="tab2">
            <div class="clear"></div>
            <!-- End .clear --> 
        </div>
        <!-- End #tab2 --> 
    </div>
    <!-- End .content-box-content --> 
</div>
<div id="khung">
    <div id="main">
        <div class="toolbar-list" id="toolbar">
            <ul>
                <li id="toolbar-new"> <a href="javascript:void(0);" onclick="javascript:document.adminForm.submit();" class="toolbar"> <span class="icon-32-save"></span> Lưu </a> </li>
                <li id="toolbar-refresh"> <a href="javascript:void(0);" class="toolbar" onclick="javascript:document.adminForm.reset();"> <span class="icon-32-refresh"> </span> Reset </a> </li>
                <li class="divider"></li>
                <li id="toolbar-help"> <a href="#messages" rel="modal" class="toolbar"> <span class="icon-32-help"></span> Trợ giúp </a> </li>
                <li id="toolbar-unpublish"> <a href="<?php echo DOMAINAD?>Comments" class="toolbar"> <span class="icon-32-cancel"></span> Hủy </a> </li>
            </ul>
            <div class="clr"></div>
        </div>
        <div class="pagetitle icon-48-category-add">
            <h2>Cập nhật sản phẩm</h2>
        </div>
        <div class="clr"></div>
    </div>
</div>
<?php echo $this->Form->end(); ?>