<script type="text/javascript">

    $(function() {

        var i = $('input').size() + 1;

        $('a.add').click(function() {

            $('<p style="padding-left: 9px;" id="anh'+i+'"><input name="userfileplus[]" id="userfile" type="file"  size="50"></p>').animate({ opacity: "show" }, "slow").appendTo('#themanh');
            i++;
        });

        $('a.remove').click(function() {
	
            if(i > 2) {
                $('#anh'+i+':last').animate({opacity:"hide"}, "slow").remove();
                i--;
            }

        });
	
    });

</script>
<?php echo $this->Form->create(null, array('url' => DOMAINAD . 'comment/add', 'type' => 'post', 'enctype' => 'multipart/form-data', 'name' => 'adminForm')); ?>

<div id="khung">
    <div id="main">
        <div class="toolbar-list" id="toolbar">
            <ul>
                <li id="toolbar-new"> <a href="javascript:void(0);" onclick="javascript:document.adminForm.submit();" class="toolbar"> <span class="icon-32-save"></span> Lưu </a> </li>
                <li id="toolbar-refresh"> <a href="javascript:void(0);" class="toolbar" onclick="javascript:document.adminForm.reset();"> <span class="icon-32-refresh"> </span> Reset </a> </li>
                <li class="divider"></li>
                <li id="toolbar-help"> <a href="#messages" rel="modal" class="toolbar"> <span class="icon-32-help"></span> Trợ giúp </a> </li>
                <li id="toolbar-unpublish"> <a href="<?php echo DOMAINAD; ?>comment" class="toolbar"> <span class="icon-32-cancel"></span> Hủy </a> </li>
            </ul>
            <div class="clr"></div>
        </div>
        <div class="pagetitle icon-48-category-add">
            <h2>Thêm mới bình luận</h2>
        </div>
        <div class="clr"></div>
    </div>
</div>
<div class="content-box"><!-- Start Content Box -->
    <div class="content-box-header">
        <h3>Thêm mới bình luận</h3>
        <div class="clear"></div>
    </div>
    <!-- End .content-box-header -->
    <div class="content-box-content">
        <div class="tab-content default-tab" id="tab1">
            <table class="input">
                <tr>
                    <td width="120" class="label">Tên người bình luận:</td>
                    <td><?php echo $this->Form->input('Comment.name', array('label' => '', 'class' => 'text-input medium-input', 'maxlength' => '60', 'onchange' => 'get_alias()', 'id' => 'idtitle')); ?></td>
				
					<?php  echo $this->Form->input('parent_id', array('type' => 'hidden', 'value' =>  $parent_id)); ?>
                </tr>

               <tr>
                    <td width="120" class="label">Số phút đã coment:</td>
                    <td><?php echo $this->Form->input('Comment.time', array('label' => '', 'class' => 'text-input medium-input', 'maxlength' => '255', 'id' => 'idtitle')); ?></td>
                </tr>
                    
				 		
			 
				
                <tr>
                    <td class="label">Hình ảnh đại diện:</td>
                    <td>&nbsp;
                        <input name="userfile" type="file" id="userfile" size="50"></td>
                </tr>
	  
              <tr>
                    <td class="label">Nội dung bình luận :</td>
                    <td>
                        <?php echo $this->Form->input('Comment.content',array('label'=>'','class'=>'text-input medium-input'));?>
						
                        </td>
                </tr>
				
               
            </table>
            <div class="clear"></div>
        </div>
        <!-- End #tab1 -->
        <div class="tab-content" id="tab2">
            <div class="clear"></div>
            <!-- End .clear --> 
        </div>
        <!-- End #tab2 --> 
    </div>
    <!-- End .content-box-content --> 
</div>
<div id="khung">
    <div id="main">
        <div class="toolbar-list" id="toolbar">
            <ul>
                <li id="toolbar-new"> <a href="javascript:void(0);" onclick="javascript:document.adminForm.submit();" class="toolbar"> <span class="icon-32-save"></span> Lưu </a> </li>
                <li id="toolbar-refresh"> <a href="javascript:void(0);" class="toolbar" onclick="javascript:document.adminForm.reset();"> <span class="icon-32-refresh"> </span> Reset </a> </li>
                <li class="divider"></li>
                <li id="toolbar-help"> <a href="#messages" rel="modal" class="toolbar"> <span class="icon-32-help"></span> Trợ giúp </a> </li>
                <li id="toolbar-unpublish"> <a href="<?php echo DOMAINAD; ?>Comments" class="toolbar"> <span class="icon-32-cancel"></span> Hủy </a> </li>
            </ul>
            <div class="clr"></div>
        </div>
        <div class="pagetitle icon-48-category-add">
            <h2>Thêm mới sản phẩm</h2>
        </div>
        <div class="clr"></div>
    </div>
</div>
<?php echo $this->Form->end(); ?>