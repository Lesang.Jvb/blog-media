<?php
$setActive = $this->request->url;
$setActive = explode("/", $setActive);
$setActive = $setActive[0];
$tabindex = "";

//if (in_array($setActive, array('catalog', 'recruitment'))) {
//    $tabindex = 1;
//}
$role=$this->Session->read('role');
$quyen=$this->Session->read('quyen');
?>
<script type="text/javascript">
    ddaccordion.init({
        headerclass: "submenuheader",
        contentclass: "submenu",
        revealtype: "click",
        mouseoverdelay: 200,
        collapseprev: true,
        defaultexpanded: [<?php echo $tabindex; ?>],
        onemustopen: false,
        animatedefault: false,
        persiststate: false,
        toggleclass: ["", ""],
        animatespeed: "fast",
        oninit:function(headers, expandedindices){
            //do nothing
        },
        onopenclose:function(header, index, state, isuseractivated){
            //do nothing
        }
    })
</script>

<div id="sidebar">
    <div id="sidebar-wrapper">
        <h1 id="sidebar-title"><a href="#"></a></h1>
        <a href="#"><img id="logo" src="<?php echo DOMAINAD ?>images/logo.png" alt="Design by Quảng cáo vip" /></a>
        <div id="profile-links"> Xin chào, <a href="#" title="Edit your profile"><?php echo $this->Session->read('name'); ?></a><br />
            <br />
            <a href="<?php echo DOMAIN; ?>" title="View the Site" target="_blank">Xem trang chủ</a> | <a href="<?php echo DOMAINAD ?>login/logout" title="Sign Out">Thoát</a> </div>
            <div id="list">
                <ul id="main-nav">
                    <li id="arrayorder_1"> <a href="<?php echo DOMAINAD ?>home" class="nav-top-item no-submenu"> Trang chủ </a> </li>
                    <li id="arrayorder_5"> <a href="<?php echo DOMAINAD ?>layout" class="nav-top-item">Quản lý layout</a> </li>
                    <li id="arrayorder_7"> <a href="<?php echo DOMAINAD ?>news" class="nav-top-item">Quản lý bài viết</a> </li>
                    <li id="arrayorder_7"> <a href="<?php echo DOMAINAD ?>comment" class="nav-top-item">Quản lý bình luận</a> </li>
                    <li id="arrayorder_7"> <a href="<?php echo DOMAINAD ?>code" class="nav-top-item">Quản lý mã code</a> </li>
                    <li id="arrayorder_7"> <a href="<?php echo DOMAINAD ?>order" class="nav-top-item">Quản lý đơn hàng</a> </li>
                    <li id="arrayorder_7"> <a href="<?php echo DOMAINAD ?>administrators" class="nav-top-item">Quản lý tài khoản</a> </li>
                </ul>
            </div>
        </div>
    </div>
