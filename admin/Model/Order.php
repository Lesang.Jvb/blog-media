<?php

class Order extends AppModel {

    var $name = 'Order';
    var $displayField = 'name';

    var $belongsTo = array(
        'News' => array(
            'className' => 'News',
            'foreignKey' => 'news_id'
        )
    );

}

?>
