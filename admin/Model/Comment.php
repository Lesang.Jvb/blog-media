<?php

class Comment extends AppModel {

    var $name = 'Comment';
    var $displayField = 'name';

    var $belongsTo = array(
        'News' => array(
            'className' => 'News',
            'foreignKey' => 'news_id'
        )
    );

}

?>
