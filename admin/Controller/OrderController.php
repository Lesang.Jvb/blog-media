<?php

date_default_timezone_set('Asia/Ho_Chi_Minh');

Class OrderController extends AppController{
    var $name = 'Order';
	var $uses=array('Order');
    
    public function beforeFilter() {
        parent::beforeFilter();
		$this->layout = 'admin';
        if (!$this->Session->read("id") || !$this->Session->read("name")) {
            $this->redirect('/');
        }
    }
    
    public function index() {
        $this->paginate = array(
		     'conditions' => array(
					'Order.delete_flag' => 0,
				),
            'order' => 'Order.id DESC',
            'limit' => '20'
        );

        $Order = $this->paginate('Order');

        $this->set('Order', $Order);



        // Luu du?ng d?n d? quay l?i n?u update, edit, dellete
        $urlpro = DOMAINAD . $this->request->url;

        $this->Session->write('urlpro', $urlpro);
        // Tang so thu tu * limit (example : 10)
        $urlTmp = DOMAINAD . $this->request->url;
        $urlTmp = explode(":", $urlTmp);
        if (isset($urlTmp[2])) {
            $startPage = ($urlTmp[2] - 1) * 10 + 1;
        } else {
            $startPage = 1;
        }
        $this->set('startPage', $startPage);
        // Xoa session thang search
        $this->Session->delete('catId');
        $this->Session->delete('keyword');
        $this->Session->delete('pageproduct');
		
		 // Load model
        $this->loadModel("News");
        $list_news = $this->News->find('list',array('conditions'=>array('News.status' => 1)));
        $this->set(compact('list_news'));
	}
    
   
    public function delete($id = null) {	
		if (empty($id)) {
			$this->Session->setFlash(__('Khôn t?n t?i danh m?c này', true));
			//$this->redirect(array('action'=>'index'));
		}
		$data['Order'] = $this->data['Order'];
		$data['Order']['delete_flag']=1;
		$data['Order']['id']=$id;
		if ($this->Order->save($data['Order'])) {
			$this->Session->setFlash(__('H́nh ?nh du?c hi?n th?', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Danh m?c không xóa du?c', true));
		$this->redirect(array('action' => 'index'));
	}
    
   
    
    public function active($id=null) {	
		//$this->account();	
		if (empty($id)) {
			$this->Session->setFlash(__('Khôn t?n t?i ', true));
			$this->redirect(array('action'=>'index'));
		}
		$data['Order'] = $this->data['Order'];
		$data['Order']['status']=1;
		$data['Order']['id']=$id;
		if ($this->Order->save($data['Order'])) {
			$this->Session->setFlash(__('H́nh ?nh du?c hi?n th?', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('H́nh ?nh không active du?c', true));
		$this->redirect(array('action' => 'index'));

	}
    
    public function deactive($id=null) {	
		//$this->account();	
		if (empty($id)) {
			$this->Session->setFlash(__('Khôn t?n t?i ', true));
			$this->redirect(array('action'=>'index'));
		}
		$data['Order'] = $this->data['Order'];
		$data['Order']['status']=2;
		$data['Order']['id']=$id;
		if ($this->Order->save($data['Order'])) {
			$this->Session->setFlash(__('H́nh ?nh du?c hi?n th?', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('H́nh ?nh không active du?c', true));
		$this->redirect(array('action' => 'index'));

	}
    
    public function close($id=null) {	
		//$this->account();	
		if (empty($id)) {
			$this->Session->setFlash(__('Khôn t?n t?i ', true));
			$this->redirect(array('action'=>'index'));
		}
		$data['Order'] = $this->data['Order'];
		
		$data['Order']['status']=0;
		$data['Order']['id']=$id;
		if ($this->Order->save($data['Order'])) {
			$this->Session->setFlash(__('H́nh ?nh không du?c hi?n th?', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('H́nh ?nh không close du?c', true));
		$this->redirect(array('action' => 'index'));

	}
    
    public function process() {
        $process = $_REQUEST['process'];
        $chon = $_REQUEST['chon'];
        if (count($chon) == 0 || $process == 5) {
			if($this->Session->check('pageproduct')) {
				$this->redirect($this->Session->read('pageproduct'));
				exit;
			} else {
				$this->redirect('/order');
				exit;
			}            
        }

        switch ($process) {
            case '1' :
                // Update active
                foreach ($chon as $k => $v) {
                    $this->Order->updateAll(
                        array(
                        'Order.status' => 1
                        ), array(
                        'Order.id' => $k)
                    );
                }
                break;
                
            case '0' :
                // Update deactive
                foreach ($chon as $k => $v) {
                    $this->Order->updateAll(
                        array(
                        'Order.status' => 0
                        ), array(
                        'Order.id' => $k)
                    );
                }
                break;

            case '2' :
                // Update deactive
                foreach ($chon as $k => $v) {
                    $this->Order->updateAll(
                        array(
                        'Order.status' => 2
                        ), array(
                        'Order.id' => $k)
                    );
                }
                break;
                
            case '3' :
                // delete many rows
                $groupId = "";
                foreach ($chon as $k => $v) {
                    $groupId .= "," . $k;
                }
                $groupId = substr($groupId, 1);
                $conditions = array(
                    'Order.id IN (' . $groupId . ')'
                );
                $this->Order->deleteAll($conditions);
                break;
        }
        if ($this->Session->check('pageproduct')) {
            $this->redirect($this->Session->read('pageproduct'));
        } else {
            $this->redirect(array('action' => 'index'));
        } 
    }
    
    public function search(){
        
        if ($this->request->is('get')){
            $listCat = $_REQUEST['listCat'];
            $this->Session->write('catId', $listCat);
        }
        
        $condition = array();
        if ($listCat > 0) {
            $condition[] = array(
                'Order.news_id' => $listCat
            );
        }
		
		$condition[] = array(
                'Order.delete_flag' => 0
            );
        $urlTmp = DOMAINAD . $this->request->url;
        $this->Session->write('pageproduct', $urlTmp);
        foreach ($condition as $values) {
            foreach ($values as $key => $cond) {
                $conditions[$key] = $cond;
            }
        }
		
		 // Tang so thu tu * limit (example : 10)
        $urlTmp = DOMAINAD . $this->request->url;
        $urlTmp = explode(":", $urlTmp);
        if (isset($urlTmp[2])) {
            $startPage = ($urlTmp[2] - 1) * 10 + 1;
        } else {
            $startPage = 1;
        }
        $this->set('startPage', $startPage);
		
		// Simple to call data
        $this->paginate = array(
            'conditions' => $condition,
            'order' => 'Order.id DESC',
            'limit' => '10'
        );
        $order = $this->paginate('Order');
        $this->set('order', $order);

        // Load model
        $this->loadModel("News");
        $list_news = $this->News->find('list',array('conditions'=>array('News.status' => 1)));
        $this->set(compact('list_news'));
		
    }
}
?>