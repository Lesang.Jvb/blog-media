<?php
App::import('Vendor', 'upload');
App::import('Vendor', 'ckeditor');
App::import('Vendor', 'ckfinder');
date_default_timezone_set('Asia/Ho_Chi_Minh');


class LayoutController extends AppController{
    var $name = 'Layout'; 
	var $uses=array('Layout','Product','Tintuc');
    public function beforeFilter() {

        parent::beforeFilter();

        $this->layout = 'admin';

        if (!$this->Session->read("id") || !$this->Session->read("name")) {
            $this->redirect('/');
        }
    }
     public function index() {

        $this->paginate = array(

            'order' => 'Layout.id DESC',

            'limit' => '20'

        );

        $Layout = $this->paginate('Layout');

        $this->set('Layout', $Layout);



        // Luu du?ng d?n d? quay l?i n?u update, edit, dellete
        $urlpro = DOMAINAD . $this->request->url;

        $this->Session->write('urlpro', $urlpro);
        // Tang so thu tu * limit (example : 10)
        $urlTmp = DOMAINAD . $this->request->url;
        $urlTmp = explode(":", $urlTmp);
        if (isset($urlTmp[2])) {
            $startPage = ($urlTmp[2] - 1) * 10 + 1;
        } else {
            $startPage = 1;
        }
        $this->set('startPage', $startPage);
        // Xoa session thang search
        $this->Session->delete('catId');
        $this->Session->delete('keyword');
        $this->Session->delete('pageproduct');
	}
    public function add() {
        if (!empty($this->request->data)) {
            $this->Layout->create();
            $data['Layout'] = $this->data['Layout'];
		   
            if ($this->Layout->save($data['Layout'])) {

                if ($this->Session->check('pageproduct')) {
                    $this->redirect($this->Session->read('pageproduct'));
                } else {
                    $this->redirect(array('action' => 'index'));
                }
            } else {

                $this->Session->setFlash(__('Thêm moi danh m?c th?t b?i. Vui long th? l?i', true));
            }
        }
    }
	
    public function delete($id = null) {	
		//$this->account();	
		if (empty($id)) {
			$this->Session->setFlash(__('Khôn t?n t?i danh m?c này', true));
			//$this->redirect(array('action'=>'index'));
		}
		if ($this->Layout->delete($id)) {
			$this->Session->setFlash(__('Xóa danh m?c thành công', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Danh m?c không xóa du?c', true));
		$this->redirect(array('action' => 'index'));
	}
	
	
    public function edit($id = null) {
        if (!$id && empty($this->request->data)) {
            $this->Session->setFlash(__('Không t?n t?i ', true));
            if ($this->Session->check('pageproduct')) {
                $this->redirect($this->Session->read('pageproduct'));
            } else {
                $this->redirect(array('action' => 'index'));
            }
        }

        if (!empty($this->request->data)) {
            $data['Layout'] = $this->data['Layout'];
            $data['Layout']['modified'] = date('Y-m-d H-i-s');
            if ($this->Layout->save($data['Layout'])) {
                if ($this->Session->check('pageproduct')) {
                    $this->redirect($this->Session->read('pageproduct'));
                } else {
                    $this->redirect(array('action' => 'index'));
                }
            } else {
                $this->Session->setFlash(__('Bài vi?t này không s?a du?c vui ḷng th? l?i.', true));
            }
        }

        if (empty($this->request->data)) {
            $this->data = $this->Layout->read(null, $id);
        }

        $this->set('edit', $this->Layout->findById($id));
    }
    
    
    public function active($id=null) {	
		//$this->account();	
		if (empty($id)) {
			$this->Session->setFlash(__('Khôn t?n t?i ', true));
			$this->redirect(array('action'=>'index'));
		}
		$data['Layout'] = $this->data['Layout'];
		$data['Layout']['status']=1;
		$data['Layout']['id']=$id;
		if ($this->Layout->save($data['Layout'])) {
			$this->Session->setFlash(__('H́nh ?nh du?c hi?n th?', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('H́nh ?nh không active du?c', true));
		$this->redirect(array('action' => 'index'));

	}
    
    
    public function close($id=null) {	
		//$this->account();	
		if (empty($id)) {
			$this->Session->setFlash(__('Khôn t?n t?i ', true));
			$this->redirect(array('action'=>'index'));
		}
		$data['Layout'] = $this->data['Layout'];
		
		$data['Layout']['status']=0;
		$data['Layout']['id']=$id;
		if ($this->Layout->save($data['Layout'])) {
			$this->Session->setFlash(__('H́nh ?nh không du?c hi?n th?', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('H́nh ?nh không close du?c', true));
		$this->redirect(array('action' => 'index'));

	}
    
    public function search(){
        
        if ($this->request->is('post')){
            $keyword = $_REQUEST['keyword'];
            $this->Session->write('keyword', $keyword);
        }else{
            $keyword = $this->Session->read('keyword');
        }
        
        $condition = array();
        if (!empty($keyword)) {
            $condition[] = array(
                'Contact.name LIKE' => '%' . $keyword . '%'
            );
        }
        $urlTmp = DOMAINAD . $this->request->url;
        $this->Session->write('pageproduct', $urlTmp);
        foreach ($condition as $values) {
            foreach ($values as $key => $cond) {
                $conditions[$key] = $cond;
            }
        }
    }
    
    public function process() {
        $process = $_REQUEST['process'];
        $chon = $_REQUEST['chon'];
        if (count($chon) == 0 || $process < 1) {
			if($this->Session->check('pageproduct')) {
				$this->redirect($this->Session->read('pageproduct'));
				exit;
			} else {
				$this->redirect('/layout');
				exit;
			}            
        }

        switch ($process) {
            case '1' :
                // Update active
                foreach ($chon as $k => $v) {
                    $this->Layout->updateAll(
                        array(
                        'Layout.status' => 1
                        ), array(
                        'Layout.id' => $k)
                    );
                }
                break;
                
            case '2' :
                // Update deactive
                foreach ($chon as $k => $v) {
                    $this->Layout->updateAll(
                        array(
                        'Layout.status' => 0
                        ), array(
                        'Layout.id' => $k)
                    );
                }
                break;

            case '3' :
                // delete many rows
                $groupId = "";
                foreach ($chon as $k => $v) {
                    $groupId .= "," . $k;
                }
                $groupId = substr($groupId, 1);
                $conditions = array(
                    'Layout.id IN (' . $groupId . ')'
                );
                $this->Layout->deleteAll($conditions);
                break;
        }
        if ($this->Session->check('pageproduct')) {
            $this->redirect($this->Session->read('pageproduct'));
        } else {
            $this->redirect(array('action' => 'index'));
        } 
    }
}
?>