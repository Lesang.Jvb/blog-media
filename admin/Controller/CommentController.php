<?php

App::import('Vendor', 'upload');

//App::import('Vendor', 'ckeditor');

//App::import('Vendor', 'ckfinder');


class CommentController extends AppController {

	var $name = 'Comment';
	var $uses=array('Comment','News');
	 public function beforeFilter() {

        parent::beforeFilter();

        $this->layout = 'admin';

        if (!$this->Session->read("id") || !$this->Session->read("name")) {

            $this->redirect('/');

        }

    }
	
		
	public function nameparent($id=null) { 
		$nameparent = $this->Comment->find('first', array(
            'conditions' => array(
                'Comment.id' => $id,		
            )
        ));
        return $nameparent;
	}
	


	 public function process() {

	

        $process = $_REQUEST['process'];

        $chon = $_REQUEST['chon'];

        if (count($chon) == 0 || $process < 1) {

			if($this->Session->check('pageproduct')) {

				$this->redirect($this->Session->read('pageproduct'));

				exit;

			} else {

				$this->redirect('/comment');

				exit;

			}            

        }

		

        switch ($process) {

            case '1' :

                // Update active

                foreach ($chon as $k => $v) {

                    $this->Comment->updateAll(

                        array(

                        'Comment.status' => 1

                        ), array(

                        'Comment.id' => $k)

                    );

                }

                break;



            case '2' :

                // Update deactive

                foreach ($chon as $k => $v) {

                    $this->Comment->updateAll(

                        array(

                        'Comment.status' => 0

                        ), array(

                        'Comment.id' => $k)

                    );

                }

                break;



            case '3' :

                // delete many rows

                $groupId = "";

                foreach ($chon as $k => $v) {

                    $groupId .= "," . $k;

                }

                $groupId = substr($groupId, 1);

                $conditions = array(

                    'Comment.id IN (' . $groupId . ')'

                );

                $this->Comment->deleteAll($conditions);

                break;

        }

        if ($this->Session->check('pageproduct')) {

            $this->redirect($this->Session->read('pageproduct'));

        } else {

            $this->redirect(array('action' => 'index'));

        } 
 
    }
 public function index() {

        $this->paginate = array(

            'order' => 'Comment.pos ASC, Comment.id DESC',

            'limit' => '20'

        );

        $Comment = $this->paginate('Comment');

        $this->set('Comment', $Comment);



        // Luu du?ng d?n d? quay l?i n?u update, edit, dellete

        $urlpro = DOMAINAD . $this->request->url;

        $this->Session->write('urlpro', $urlpro);



        // Tang so thu tu * limit (example : 10)

        $urlTmp = DOMAINAD . $this->request->url;

        $urlTmp = explode(":", $urlTmp);

        if (isset($urlTmp[2])) {

            $startPage = ($urlTmp[2] - 1) * 10 + 1;

        } else {

            $startPage = 1;

        }

        $this->set('startPage', $startPage);



        // Xoa session thang search

        $this->Session->delete('catId');

        $this->Session->delete('keyword');

        $this->Session->delete('pageproduct');
		
		 // Load model
        $this->loadModel("News");
        $list_news = $this->News->find('list',array('conditions'=>array('News.status' => 1)));
        $this->set(compact('list_news'));
 

	}

	
		public function addchild($parent_id) {
        if (!empty($this->request->data)) {
            $this->Comment->create();
            $data['Comment'] = $this->data['Comment'];
		  if($_FILES['userfile']['name']) {
                $handle = new upload($_FILES['userfile']);
                if ($handle->uploaded) {


                    $filename = date('YmdHis') . md5(rand(10000, 99999));
                    $handle->file_new_name_body = $filename;

                    $handle->Process(IMAGES_URL . 'comments');
                    if ($handle->processed) {
                        $img = $handle->file_dst_name;
                    }
                }
            } else {
                $img = null;
            }
            $data['Comment']['images'] = $img;
			
		    $arrNews = $this->News->find('list',array('conditions'=>array('News.status'=>1),'order'=>'News.id'));
			
			 $cat_id='|';
			 
			 foreach($arrNews as $k=>$v) {
				 
			  if(isset($_POST["cat_$k"])) $cat_id.= $k.'|';
			
			 }
			 
		   $data['Comment']['news_id'] = $cat_id;
		   
            if ($this->Comment->save($data['Comment'])) {

                if ($this->Session->check('pageproduct')) {
                    $this->redirect($this->Session->read('pageproduct'));
                } else {
                    $this->redirect(array('action' => 'index'));
                }
            } else {

                $this->Session->setFlash(__('Thêm moi danh m?c th?t b?i. Vui long th? l?i', true));
            }
        }
		$arrNews = $this->News->find('list',array('conditions'=>array('News.status'=>1),'order'=>'News.id'));
        $this->set(compact('arrNews'));
		
		$this->set('parent_id',$parent_id);

    }
	
	
	public function add() {
        if (!empty($this->request->data)) {
            $this->Comment->create();
            $data['Comment'] = $this->data['Comment'];
		  if($_FILES['userfile']['name']) {
                $handle = new upload($_FILES['userfile']);
                if ($handle->uploaded) {


                    $filename = date('YmdHis') . md5(rand(10000, 99999));
                    $handle->file_new_name_body = $filename;

                    $handle->Process(IMAGES_URL . 'comments');
                    if ($handle->processed) {
                        $img = $handle->file_dst_name;
                    }
                }
            } else {
                $img = null;
            }
            $data['Comment']['images'] = $img;
			
		    $arrNews = $this->News->find('list',array('conditions'=>array('News.status'=>1),'order'=>'News.id'));
			
			 $cat_id='|';
			 
			 foreach($arrNews as $k=>$v) {
				 
			  if(isset($_POST["cat_$k"])) $cat_id.= $k.'|';
			
			 }
			 
		   $data['Comment']['news_id'] = $cat_id;
		   
            if ($this->Comment->save($data['Comment'])) {

                if ($this->Session->check('pageproduct')) {
                    $this->redirect($this->Session->read('pageproduct'));
                } else {
                    $this->redirect(array('action' => 'index'));
                }
            } else {

                $this->Session->setFlash(__('Thêm moi danh m?c th?t b?i. Vui long th? l?i', true));
            }
        }
		$arrNews = $this->News->find('list',array('conditions'=>array('News.status'=>1),'order'=>'News.id'));
        $this->set(compact('arrNews'));
		


    }
	
	public function delete($id = null) {	
		if (empty($id)) {
			$this->Session->setFlash(__('Khôn t?n t?i danh m?c này', true));
			//$this->redirect(array('action'=>'index'));
		}
		if ($this->Comment->delete($id)) {
			$this->Session->setFlash(__('Xóa danh m?c thành công', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Danh m?c không xóa du?c', true));
		$this->redirect(array('action' => 'index'));
	}
	
 public function edit($id = null) {

        if (!$id && empty($this->request->data)) {

            $this->Session->setFlash(__('Không tồn tại ', true));

            if ($this->Session->check('pageproduct')) {

                $this->redirect($this->Session->read('pageproduct'));

            } else {

                $this->redirect(array('action' => 'index'));

            }

        }

        if (!empty($this->request->data)) {

            $data['Comment'] = $this->data['Comment'];
			if ($_FILES['userfile']['name'] != "") {
                // Upload anh
                $handle = new upload($_FILES['userfile']);
                if ($handle->uploaded) {
                    $filename = date('YmdHis') . md5(rand(10000, 99999));
                    $handle->file_new_name_body = $filename;

                    $handle->Process(IMAGES_URL . 'comments');
                    if ($handle->processed) {
                        $img = $handle->file_dst_name;
                    }
                }
            } else {
                $img = $_REQUEST['oldimg'];
            }

            $data['Comment']['images'] = $img;

            $data['Comment']['modified'] = date('Y-m-d');


			 $arrNews = $this->News->find('list',array('conditions'=>array('News.status'=>1),'order'=>'News.id'));
			 $cat_id='|';
			 
			 foreach($arrNews as $k=>$v) {
			 if(isset($_POST["cat_$k"])) $cat_id.= $k.'|';
			 }
			 
		   $data['Comment']['news_id'] = $cat_id;
			
			
			

            if ($this->Comment->save($data['Comment'])) {

                if ($this->Session->check('pageproduct')) {

                    $this->redirect($this->Session->read('pageproduct'));

                } else {

                    $this->redirect(array('action' => 'index'));

                }

            } else {

                $this->Session->setFlash(__('Bài vi?t này không s?a du?c vui lòng th? l?i.', true));

            }

        }

        if (empty($this->request->data)) {

            $this->data = $this->Comment->read(null, $id);

        }
		$arrNews = $this->News->find('list',array('conditions'=>array('News.status'=>1),'order'=>'News.id'));
        $this->set(compact('arrNews'));

        $this->set('edit', $this->Comment->findById($id));

		

    }
	public function close($id=null) {	
		//$this->account();	
		if (empty($id)) {
			$this->Session->setFlash(__('Khôn t?n t?i ', true));
			$this->redirect(array('action'=>'index'));
		}
		$data['Comment'] = $this->data['Comment'];
		
		$data['Comment']['status']=0;
		$data['Comment']['id']=$id;
		if ($this->Comment->save($data['Comment'])) {
			$this->Session->setFlash(__('Hình ?nh không du?c hi?n th?', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Hình ?nh không close du?c', true));
		$this->redirect(array('action' => 'index'));

	}
	
	public function active($id=null) {	
		//$this->account();	
		if (empty($id)) {
			$this->Session->setFlash(__('Khôn t?n t?i ', true));
			$this->redirect(array('action'=>'index'));
		}
		$data['Comment'] = $this->data['Comment'];
		$data['Comment']['status']=1;
		$data['Comment']['id']=$id;
		if ($this->Comment->save($data['Comment'])) {
			$this->Session->setFlash(__('Hình ?nh du?c hi?n th?', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Hình ?nh không active du?c', true));
		$this->redirect(array('action' => 'index'));

	}
	
	
   
   public function search(){
        
	   if($_POST['listCat']){
          $listCat = $_POST['listCat'];
		  $this->Session->write('catId', $listCat);
	   }else{
		  $listCat = 0; 
		   }
        $condition = array();
        if ($listCat > 0) {
            $condition[] = array(
                'Comment.news_id LIKE' => '%'.'|'.$listCat.'|'.'%',
            );
        }

        $urlTmp = DOMAINAD . $this->request->url;
        $this->Session->write('pageproduct', $urlTmp);
        foreach ($condition as $values) {
            foreach ($values as $key => $cond) {
                $conditions[$key] = $cond;
            }
        }
		
		 // Tang so thu tu * limit (example : 10)
        $urlTmp = DOMAINAD . $this->request->url;
        $urlTmp = explode(":", $urlTmp);
        if (isset($urlTmp[2])) {
            $startPage = ($urlTmp[2] - 1) * 10 + 1;
        } else {
            $startPage = 1;
        }
        $this->set('startPage', $startPage);
		
		// Simple to call data
        $this->paginate = array(
            'conditions' => $condition,
            'order' => 'Comment.id DESC',
            'limit' => '10'
        );
        $comment = $this->paginate('Comment');
        $this->set('comment', $comment);

        // Load model
        $this->loadModel("News");
        $list_news = $this->News->find('list',array('conditions'=>array('News.status' => 1)));
        $this->set(compact('list_news'));
		
    }
	
	public function processing() {
		if(isset($_POST['dropdown']))
			$select=$_POST['dropdown'];
			
		if(isset($_POST['checkall']))
				{
			
			switch ($select){
				case 'active':
				$Comments=($this->Comment->find('all'));
				foreach($Comments as $Comment) {
					$Comment['Comment']['status']=1;
					$this->Comment->save($Comment['Comment']);					
				}
				//vong lap active
				break;
				case 'notactive':	
				//vong lap huy
				$Comments=($this->Comment->find('all'));
				foreach($Comments as $Comment) {
					$new['Comment']['status']=0;
					$this->Comment->save($Comment['Comment']);					
				}
				break;
				case 'delete':
				$Comments=($this->Comment->find('all'));
				foreach($Comments as $Comment) {
					$this->Comment->delete($Comment['Comment']['id']);					
				}
				if($this->Comment->find('count')<1)
					$this->redirect(array('action' => 'index'));	
				 else
				 {
					$this->Session->setFlash(__('Danh m?c không close du?c', true));
					$this->redirect(array('action' => 'index'));
				 }
				//vong lap xoa
				break;
				
			}
		}
		else{
			
			switch ($select){
				case 'active':
				$Comments=($this->Comment->find('all'));
				foreach($Comments as $Comment) {
					if(isset($_POST[$Comment['Comment']['id']]))
					{
						$Comment['Comment']['status']=1;
						$this->Comment->save($Comment['Comment']);
					}
				}
				//vong lap active
				break;
				case 'notactive':	
				//vong lap huy
				$Comments=($this->Comment->find('all'));
				foreach($Comments as $Comment) {
					if(isset($_POST[$Comment['Comment']['id']]))
					{
						$new['Comment']['status']=0;
						$this->Comment->save($Comment['Comment']);
					}
				}
				break;
				case 'delete':
				$Comments=($this->Comment->find('all'));
				foreach($Comments as $Comment) {
					if(isset($_POST[$Comment['Comment']['id']]))
					{
					    $this->Comment->delete($Comment['Comment']['id']);
						$this->redirect(array('action'=>'index'));
					}
										
				}
				
				die;	
				//vong lap xoa
				break;
				
			}
			
		}
		$this->redirect(array('action' => 'index'));
		
	}
	
}
?>