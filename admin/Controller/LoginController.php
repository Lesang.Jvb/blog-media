<?php

class LoginController extends AppController {

    public $name = 'Login';
    public  $helpers = array('Session', 'Form', 'Html');
    public $uses = array('Administrator');

    public function beforeFilter() {
        parent::beforeFilter();
        $this->layout = 'login';
    }

    public function index() {
        
    }

    /*
     * Check Login
     * @create : 09-10-2012
     */
    function login() {
        $data['Administrator'] = $this->data['Administrator'];
        if (empty($data['Administrator']['name'])) {
            $this->Session->setFlash(__('Xin vui lòng nhập tên đăng nhập', true));
            $this->redirect(array('action' => 'index'));
        } elseif (empty($data['Administrator']['password'])) {
            $this->Session->setFlash(__('Xin vui lòng nhập mật khẩu', true));
            $this->redirect(array('action' => 'index'));
        } else {
            $chek = $this->Administrator->findByName($data['Administrator']['name']);
            if ($chek) {
                if ($chek['Administrator']['password'] == md5($data['Administrator']['password'])) {
                    $this->Session->write('id', $chek['Administrator']['id']);
                    $this->Session->write('name', $chek['Administrator']['name']);
                    $this->Session->write('role', $chek['Administrator']['role']);
					if($chek['Administrator']['role']==0){
						$quyen=explode('|',$chek['Administrator']['quyen']);
						$mang=array();
						for($i=0;$i<count($quyen)-1; $i++){
						
							$bien=explode('_',$quyen[$i]);
							$sub=explode('-',$bien[1]);
							$mang[$bien[0]]['active']=$sub[0];
							$mang[$bien[0]]['close']=$sub[0];
							$mang[$bien[0]]['delete']=$sub[1];
							$mang[$bien[0]]['edit']=$sub[2];
							$mang[$bien[0]]['add']=$sub[3];
							$mang[$bien[0]]['access']=$sub[4];
						}
						
					 $this->Session->write('quyen', $mang);
					}else $this->Session->write('quyen','FULL');
					
                    $this->redirect('/home');
                } else {
                    $this->Session->setFlash(__('Mật khẩu không đúng !', true));
                    $this->redirect('/');
                }
            } else {
                $this->Session->setFlash(__('Xin vui lòng đăng nhập lại', true));
                    $this->redirect('/');
            }
        }
    }

    //lay lai password
    function password() {
        $this->layout = 'password';
    }

    function check_pass() {
        
    }

    //logout ra khoi he thong
    function logout() {
        $this->Session->delete('id');
        $this->Session->delete('name');
        $this->Session->delete('role');
        $this->Session->delete('quyen');
        $this->redirect(array('action' => 'index'));
    }

}
