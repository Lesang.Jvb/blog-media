<?php

App::import('Vendor', 'upload');

App::import('Vendor', 'ckeditor');

App::import('Vendor', 'ckfinder');


class ContactsController extends AppController {



    public $name = 'Contact';

    public $uses = array('Contact','Email','Product');



    public function beforeFilter() {

        parent::beforeFilter();

        $this->layout = 'admin';

        if (!$this->Session->read("id") || !$this->Session->read("name")) {

            $this->redirect('/');

        }

    }

	public function excel($id = null)

    {

        $this->autoLayout = false;



        $this->Email->recursive = 1;

        

        $data = $this->Email->find('all', array('conditions'=>array(),'order'=>'Email.id DESC'));



                

        $this->set('rows',$data);

    }



        public function exportToExcel() {

        $contacts = $this->Contact->find('all');

        $this->set(compact('contacts'));

    }

   function index() {

		 

		    
		$list_product = $this->Product->find('list',array('conditions'=>array('Product.status'=>1)));
        $this->set(compact('list_product'));

		  $this->paginate = array('limit' => '15','order' => 'Contact.id DESC');

	      $this->set('Contact', $this->paginate('Contact',array()));

		

		  

	}

	

	//view mot tin 

	function view($id = null) {

	  
	 if (!$id && empty($this->request->data)) {
            $this->Session->setFlash(__('Không tồn tại ', true));
            if ($this->Session->check('pagenew')) {
                $this->redirect($this->Session->read('pagenew'));
            } else {
                $this->redirect(array('action' => 'index'));
            }
        }
        if (!empty($this->request->data)) {
            $data = $this->request->data;
        

           
            if ($this->Contact->save($data['Contact'])) {
                if ($this->Session->check('pagenew')) {
                    $this->redirect($this->Session->read('pagenew'));
                } else {
                    $this->redirect(array('action' => 'index'));
                }
            }
        }
        if (empty($this->request->data)) {
            $this->data = $this->Contact->read(null, $id);
        }
 
        $views = $this->Contact->findById($id);
        $this->set('views', $views);
		

		  $this->paginate = array(
		  	'conditions' => array(
                           'Email.name LIKE' => $views['Contact']['code'],
                         ),

            'order' => 'Email.id ASC, Email.modified DESC',

            'limit' => '10',

        );

        $listcode = $this->paginate('Email');
        $this->set('listcode', $listcode);
        // pr($listcode);die;
		 // $listcode = $this->Email->find('all', array(
   //                      'conditions' => array(
   //                              'Email.status' => 1,
   //                              'Email.content LIKE' => $views['Contact']['code']
   //                          ),
   //                        'order' => 'Email.id ASC, Email.modified DESC',

   //                    ));

   //                    $this->set('listcode', $listcode);

		  

		  

	}

	


    public function search() {
		
		 

        if ($this->request->is('post')) {

            // Lay du lieu tu form

            $listCat = $_REQUEST['listCat'];

            $this->Session->write('catId', $listCat);



            // Get keyword

            $keyword = $_REQUEST['keyword'];

            $this->Session->write('keyword', $keyword);

        } else {

            $listCat = $this->Session->read('catId');

            $keyword = $this->Session->read('keyword');

        }



        // setup condition to search

        $condition = array();

        if (!empty($keyword)) {

            $condition[] = array(

                'Contact.name LIKE' => '%' . $keyword . '%'

            );

        }



        if ($listCat > 0) {

            $condition[] = array(

                'Contact.product_id' => $listCat

            );

        }



        // Lưu đường dẫn để quay lại nếu update, edit, dellete

        $urlTmp = DOMAINAD . $this->request->url;

        $this->Session->write('pageproduct', $urlTmp);



        // Sau khi lay het dieu kien sap xep vao 1 array

        $conditions = array();

        foreach ($condition as $values) {

            foreach ($values as $key => $cond) {

                $conditions[$key] = $cond;

            }

        }



        // Tang so thu tu * limit (example : 10)

        $urlTmp = DOMAINAD . $this->request->url;

        $urlTmp = explode(":", $urlTmp);

        if (isset($urlTmp[2])) {

            $startPage = ($urlTmp[2] - 1) * 10 + 1;

        } else {

            $startPage = 1;

        }

        $this->set('startPage', $startPage);



        // Simple to call data

        $this->paginate = array(

            'conditions' => $condition,

            'order' => 'Contact.id DESC',

            'limit' => '10'

        );

        $contact = $this->paginate('Contact');

        $this->set('contact', $contact);
		  
		 	
	   

        // Load model

       $list_product = $this->Product->find('list',array('conditions'=>array('Product.status'=>1)));
       $this->set(compact('list_product'));
        
    }


	function search11() {

		

	  

		$keyword=isset($_POST['name'])? $_POST['name'] : '';

		

	

		$this->paginate = array('conditions'=>array('Contact.name like'=>'%'.$keyword.'%'),'limit' => '12','order' => 'Contact.id DESC');

		

		

		$this->set('Contact', $this->paginate('Contact',array()));	

		$this->render('index');

		

       

		

	}

	function processing() {

		

		if(isset($_POST['dropdown']))

			$select=$_POST['dropdown'];

			

	

			

			switch ($select){

				case 'active':

				$Contact=($this->Contact->find('all'));

				foreach($Contact as $new) {

					if(isset($_POST[$new['Contact']['id']]))

					{

						$new['Contact']['status']=1;

						$this->Contact->save($new['Contact']);

					}

				}

				//vong lap active

				break;

				case 'notactive':	

				//vong lap huy

				$Contact=($this->Contact->find('all'));

				foreach($Contact as $new) {

					if(isset($_POST[$new['Contact']['id']]))

					{

						$new['Contact']['status']=0;

						$this->Contact->save($new['Contact']);

					}

				}

				break;

				case 'delete':

				$Contact=($this->Contact->find('all'));

				foreach($Contact as $new) {

					if(isset($_POST[$new['Contact']['id']]))

					{

					    $this->Contact->delete($new['Contact']['id']);

					

					}

										

				}

				

					

				//vong lap xoa

				break;

				

			

			

		}

		$this->redirect(array('action' => 'index'));

		

	}

	

	//close tin tuc

	function close($id=null) {

		

		if (empty($id)) {

			$this->Session->setFlash(__('Khôn tồn tại bài viết này', true));

			$this->redirect(array('action'=>'index'));

		}

		$data['Email'] = $this->data['Email'];

		$data['Email']['status']=0;

		if ($this->Email->save($data['Email'])) {

			$this->Session->setFlash(__('Bài viết không được hiển thị', true));

			$this->redirect(array('action'=>'index'));

		}

		$this->Session->setFlash(__('Bài viết không close được', true));

		$this->redirect(array('action' => 'index'));



	}

	// active tin bai viêt

	function active($id=null) {

		

		if (empty($id)) {

			$this->Session->setFlash(__('Khôn tồn tại bài viết này', true));

			$this->redirect(array('action'=>'index'));

		}

		$data['Email'] = $this->data['Email'];

		$data['Email']['status']=1;

		if ($this->Email->save($data['Email'])) {

			$this->Session->setFlash(__('Bài viết được hiển thị', true));

			$this->redirect(array('action'=>'index'));

		}

		$this->Session->setFlash(__('Bài viết không hiển được bài viết', true));

		$this->redirect(array('action' => 'index'));

	}

	

	

	

	// Xoa cac dang

	function delete($id = null) {

				

		if (empty($id)) {

			$this->Session->setFlash(__('Khôn tồn tại bài viết này', true));

			//$this->redirect(array('action'=>'index'));

		}

		if ($this->Contact->delete($id)) {

			$this->Session->setFlash(__('Xóa bài viết thành công', true));

			$this->redirect(array('action'=>'index'));

		}

		$this->Session->setFlash(__('Bài viết không xóa được', true));

		$this->redirect(array('action' => 'index'));

	}

	

	



}

