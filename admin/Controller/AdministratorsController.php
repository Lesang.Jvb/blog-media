<?php

class AdministratorsController extends AppController {

    public $name = 'Administrators';
    public $uses = array('Administrator');

    public function beforeFilter() {
        parent::beforeFilter();
        $this->layout = 'admin';
        if (!$this->Session->read("id") || !$this->Session->read("name")) {
            $this->redirect('/');
        }
    }

    public function index() {
        $this->set('users', $this->Administrator->find('all'));
		if($this->Session->read('role')==0) {
		 echo "<script>alert('" . json_encode('Bạn không được phép truy cập vào địa chỉ này!') . "');</script>";
                echo "<script>history.back();</script>"; die;
		}
    }

    public function edit_pass($id = null) {
        if (!$id && empty($this->data)) {
            $this->Session->setFlash(__('Không tồn tại ', true));
            $this->redirect(array('action' => 'index'));
        }
        if (empty($this->data)) {
            $this->data = $this->Administrator->read(null, $id);
        }
    }

    public function check_pass() {
        if (!empty($this->data)) {
            $data['Administrator'] = $this->data['Administrator'];
            if ($data['Administrator']['password'] == '' || $data['Administrator']['pass2'] == '') {
                echo "<script>alert('" . json_encode('Bạn vui lòng nhập đầy đủ mật khẩu cũ và mật khẩu mới!') . "');</script>";
                echo "<script>history.back();</script>";
            } else {
                $check = $this->Administrator->findById($data['Administrator']['id']);
                if ($check['Administrator']['password'] != md5($data['Administrator']['password'])) {
                    echo "<script>alert('" . json_encode('Mật khẩu cũ không đúng! Vui lòng thực hiện lại!') . "');</script>";
                    echo "<script>history.back();</script>";
                } else {
                    $data['Administrator']['password'] = md5($data['Administrator']['pass2']);
                    if ($this->Administrator->save($data['Administrator'])) {
                        echo "<script>alert('" . json_encode('Tài khoản của bạn đã thay đổi thành công!') . "');</script>";
                        echo "<script>location.href='" . DOMAINAD . "administrators'</script>";
                    }
                }
            }
        }
    }
    public function add() {
        if (!empty($this->data)) {
            $data = $this->data;
			$bien='';
			for($i=1;$i<17;$i++){
				$modul=explode('-',$_POST["modul$i"]);
				$a=isset($_POST["sub1$i"])?$_POST["sub1$i"]:'0';
				$b=isset($_POST["sub2$i"])?$_POST["sub2$i"]:'0';
				$c=isset($_POST["sub3$i"])?$_POST["sub3$i"]:'0';
				$d=isset($_POST["sub4$i"])?$_POST["sub4$i"]:'0';
				$e=isset($_POST["sub5$i"])?$_POST["sub5$i"]:'0';
								
				$bien=$bien.$modul[0].'_'.$a.'-'.$b.'-'.$c.'-'.$d.'-'.$e.'|';
				//if(isset($modul[1])) $bien=$bien.$modul[1].'_'.$a.'-'.$b.'-'.$c.'-'.$d.'-'.$e.'|';
			}
			$data['Administrator']['quyen']=$bien;
				if($data['Administrator']['password']=='' || $data['Administrator']['name']=='') {
					echo "<script>alert('" . json_encode('Bạn phải nhập đầy đủ thông tin!') . "');</script>";
					echo "<script>history.back(-1);</script>";die;
				}
				
					if($data['Administrator']['password']=='' || $data['Administrator']['pass2']=='') {
					echo "<script>alert('" . json_encode('Bạn phải nhập đầy đủ thông tin!') . "');</script>";
					echo "<script>history.back(-1);</script>";die;
				}
				if($data['Administrator']['password']!=$data['Administrator']['pass2']) {
					echo "<script>alert('" . json_encode('Hai password không giống nhau!') . "');</script>";
					echo "<script>history.back(-1);</script>";die;
				}
				
				$user=$this->Administrator->findByName($data['Administrator']['name']);
				if(isset($user['Administrator'])) {
					echo "<script>alert('" . json_encode('Tên đăng nhập đã tồn tại!') . "');</script>";
					echo "<script>history.back(-1);</script>";die;
				}
				
		    $data['Administrator']['password'] = md5(trim($this->data['Administrator']['password']));
            $this->Administrator->create();
            if ($this->Administrator->save($data['Administrator']))
                $this->redirect(array('action' => 'index'));
            
        }
    }

	
	 public function edit($id=null) {
        if (!empty($this->data)) {
            $data = $this->data;
			$bien='';
			for($i=1;$i<17;$i++){
				$modul=explode('-',$_POST["modul$i"]);
				$a=isset($_POST["sub1$i"])?$_POST["sub1$i"]:'0';
				$b=isset($_POST["sub2$i"])?$_POST["sub2$i"]:'0';
				$c=isset($_POST["sub3$i"])?$_POST["sub3$i"]:'0';
				$d=isset($_POST["sub4$i"])?$_POST["sub4$i"]:'0';
				$e=isset($_POST["sub5$i"])?$_POST["sub5$i"]:'0';
							
				$bien=$bien.$modul[0].'_'.$a.'-'.$b.'-'.$c.'-'.$d.'-'.$e.'|';
				//if(isset($modul[1])) $bien=$bien.$modul[1].'_'.$a.'-'.$b.'-'.$c.'-'.$d.'-'.$e.'|';
			}
			$data['Administrator']['quyen']=$bien;
				if($data['Administrator']['password']=='' || $data['Administrator']['name']=='') {
					echo "<script>alert('" . json_encode('Bạn phải nhập đầy đủ thông tin!') . "');</script>";
					echo "<script>history.back(-1);</script>";die;
				}
				$user=$this->Administrator->findByName($data['Administrator']['name']);
				if(isset($user['Administrator']) && $user['Administrator']['id']!=$data['Administrator']['id']) {
					echo "<script>alert('" . json_encode('Tên đăng nhập đã tồn tại!') . "');</script>";
					echo "<script>history.back(-1);</script>";die;
				}
				
				if($data['Administrator']['password']!=$data['Administrator']['pass2']) {
					echo "<script>alert('" . json_encode('Hai password không giống nhau!') . "');</script>";
					echo "<script>history.back(-1);</script>";die;
				}
				
				if($user['Administrator']['password'] != $data['Administrator']['password'])
				$data['Administrator']['password'] = md5(trim($this->data['Administrator']['password']));
        
            if ($this->Administrator->save($data['Administrator']))
                $this->redirect(array('action' => 'index'));
            
        }
		else {
			$edit=$this->Administrator->findById($id);
			$this->data=$edit;
			$this->set('edit',$edit);
		}
    }
	
    public function delete($id = null) {
        if (!empty($id))
            $this->Administrator->delete($id);
        $this->redirect(array('action' => 'index'));
    }

}
