<?php


class CodeController extends AppController {

	var $name = 'Code';
	var $uses=array('Code','News');
	 public function beforeFilter() {

        parent::beforeFilter();

        $this->layout = 'admin';

        if (!$this->Session->read("id") || !$this->Session->read("name")) {

            $this->redirect('/');

        }

    }

	public function search() {
		
        if (isset($_GET['keyword'])) {
            $keyword = $_GET['keyword'];
        }else{
			$keyword = 0;
			}
        $this->paginate = array(
            'conditions' => array(
                'Code.name LIKE' => '%' . $keyword . '%',
            ),
            'order' => 'Code.id DESC',
            'limit' => '9'
        );

        $listCode = $this->paginate('Code');

        $this->set('listCode', $listCode);
		}
	
 public function index() {

        $this->paginate = array(

            'order' => 'Code.id DESC',

            'limit' => '20'

        );

        $code = $this->paginate('Code');

        $this->set('code', $code);



        // Luu du?ng d?n d? quay l?i n?u update, edit, dellete

        $urlpro = DOMAINAD . $this->request->url;

        $this->Session->write('urlpro', $urlpro);



        // Tang so thu tu * limit (example : 10)

        $urlTmp = DOMAINAD . $this->request->url;

        $urlTmp = explode(":", $urlTmp);

        if (isset($urlTmp[2])) {

            $startPage = ($urlTmp[2] - 1) * 10 + 1;

        } else {

            $startPage = 1;

        }

        $this->set('startPage', $startPage);


 

	}

	public function add() {
        if (!empty($this->request->data)) {
            $this->Code->create();
            $data['Code'] = $this->data['Code'];
		  
				 
		   
            if ($this->Code->save($data['Code'])) {

                if ($this->Session->check('pageproduct')) {
                    $this->redirect($this->Session->read('pageproduct'));
                } else {
                    $this->redirect(array('action' => 'index'));
                }
            } else {

                $this->Session->setFlash(__('Thêm moi danh m?c th?t b?i. Vui long th? l?i', true));
            }
        }
		    $product=$this->News->find('list',array('conditions'=>array('News.status'=>1,'News.delete_flag'=>0),'order'=>'News.id'));
        $this->set(compact('product'));
		
        // Load model
      //  $this->loadModel("Catproduct");
       // $list_cat = $this->Catproduct->generateTreeList(null, null, null, '-- ');
       // $this->set(compact('list_cat'));

    }
	
	public function delete($id = null) {	
		$this->account();	
		if (empty($id)) {
			$this->Session->setFlash(__('Khôn t?n t?i danh m?c này', true));
			//$this->redirect(array('action'=>'index'));
		}
		if ($this->Code->delete($id)) {
			$this->Session->setFlash(__('Xóa danh m?c thành công', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Danh m?c không xóa du?c', true));
		$this->redirect(array('action' => 'index'));
	}
	
 public function edit($id = null) {

        if (!$id && empty($this->request->data)) {

            $this->Session->setFlash(__('Không t?n t?i ', true));

            if ($this->Session->check('pageproduct')) {

                $this->redirect($this->Session->read('pageproduct'));

            } else {

                $this->redirect(array('action' => 'index'));

            }

        }

        if (!empty($this->request->data)) {

            $data['Code'] = $this->data['Code'];

            if ($this->Code->save($data['Code'])) {

                if ($this->Session->check('pageproduct')) {

                    $this->redirect($this->Session->read('pageproduct'));

                } else {

                    $this->redirect(array('action' => 'index'));

                }

            } else {

                $this->Session->setFlash(__('Bài vi?t này không s?a du?c vui lòng th? l?i.', true));

            }

        }

        if (empty($this->request->data)) {

            $this->data = $this->Code->read(null, $id);

        }
		$product=$this->News->find('list',array('conditions'=>array('News.status'=>1,'News.delete_flag'=>0),'order'=>'News.id'));
        $this->set(compact('product'));

        $this->set('edit', $this->Code->findById($id));

		

    }
	public function close($id=null) {	
		//$this->account();	
		if (empty($id)) {
			$this->Session->setFlash(__('Khôn t?n t?i ', true));
			$this->redirect(array('action'=>'index'));
		}
		$data['Code'] = $this->data['Code'];
		
		$data['Code']['status']=0;
		$data['Code']['id']=$id;
		if ($this->Code->save($data['Code'])) {
			$this->Session->setFlash(__('Hình ?nh không du?c hi?n th?', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Hình ?nh không close du?c', true));
		$this->redirect(array('action' => 'index'));

	}
	
	public function active($id=null) {	
		//$this->account();	
		if (empty($id)) {
			$this->Session->setFlash(__('Khôn t?n t?i ', true));
			$this->redirect(array('action'=>'index'));
		}
		$data['Code'] = $this->data['Code'];
		$data['Code']['status']=1;
		$data['Code']['id']=$id;
		if ($this->Code->save($data['Code'])) {
			$this->Session->setFlash(__('Hình ?nh du?c hi?n th?', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Hình ?nh không active du?c', true));
		$this->redirect(array('action' => 'index'));

	}
	
		public function _find_list() {
		return $this->Portfolio->generatetreelist(null, null, null, '__');
	}
	//check ton tai tai khoan
	public function account(){
		if(!$this->Session->read("id") || !$this->Session->read("name")){
			$this->redirect('/');
		}
	}
 
	
	public function processing() {
		$this->account();
		if(isset($_POST['dropdown']))
			$select=$_POST['dropdown'];
			
		if(isset($_POST['checkall']))
				{
			
			switch ($select){
				case 'active':
				$Codes=($this->Code->find('all'));
				foreach($Codes as $Code) {
					$Code['Code']['status']=1;
					$this->Code->save($Code['Code']);					
				}
				//vong lap active
				break;
				case 'notactive':	
				//vong lap huy
				$Codes=($this->Code->find('all'));
				foreach($Codes as $Code) {
					$new['Code']['status']=0;
					$this->Code->save($Code['Code']);					
				}
				break;
				case 'delete':
				$Codes=($this->Code->find('all'));
				foreach($Codes as $Code) {
					$this->Code->delete($Code['Code']['id']);					
				}
				if($this->Code->find('count')<1)
					$this->redirect(array('action' => 'index'));	
				 else
				 {
					$this->Session->setFlash(__('Danh m?c không close du?c', true));
					$this->redirect(array('action' => 'index'));
				 }
				//vong lap xoa
				break;
				
			}
		}
		else{
			
			switch ($select){
				case 'active':
				$Codes=($this->Code->find('all'));
				foreach($Codes as $Code) {
					if(isset($_POST[$Code['Code']['id']]))
					{
						$Code['Code']['status']=1;
						$this->Code->save($Code['Code']);
					}
				}
				//vong lap active
				break;
				case 'notactive':	
				//vong lap huy
				$Codes=($this->Code->find('all'));
				foreach($Codes as $Code) {
					if(isset($_POST[$Code['Code']['id']]))
					{
						$new['Code']['status']=0;
						$this->Code->save($Code['Code']);
					}
				}
				break;
				case 'delete':
				$Codes=($this->Code->find('all'));
				foreach($Codes as $Code) {
					if(isset($_POST[$Code['Code']['id']]))
					{
					    $this->Code->delete($Code['Code']['id']);
						$this->redirect(array('action'=>'index'));
					}
										
				}
				
				die;	
				//vong lap xoa
				break;
				
			}
			
		}
		$this->redirect(array('action' => 'index'));
		
	}
	
}
?>