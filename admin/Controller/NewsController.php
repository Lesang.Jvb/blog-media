<?php

App::import('Vendor', 'upload');
App::import('Vendor', 'ckeditor');
App::import('Vendor', 'ckfinder');

/**
 * Description of NewsController
 * @author : 
 * @since 09-10-2012
 */
 


class NewsController extends AppController {

    public $name = 'News';
    public $uses = array();

    public function beforeFilter() {
        parent::beforeFilter();
        $this->layout = 'admin';
        if (!$this->Session->read("id") || !$this->Session->read("name")) {
            $this->redirect('/');
        }
    }

    /**
     * Danh sách sản phẩm
     * @author 
     */
    function index() {
        $this->paginate = array(
		    'conditions'=>array(
			  		'News.delete_flag'=>0,
				),
            'order' => 'News.id DESC',
            'limit' => '10'
        );
        $listNews = $this->paginate('News');
        $this->set('listNewss', $listNews);

        // Tang so thu tu * limit (example : 10)
        $urlTmp = DOMAINAD . $this->request->url;
        $urlTmp = explode(":", $urlTmp);
        if (isset($urlTmp[2])) {
            $startPage = ($urlTmp[2] - 1) * 10 + 1;
        } else {
            $startPage = 1;
        }
        $this->set('startPage', $startPage);

        // Lưu đường dẫn để quay lại nếu update, edit, dellete
        $urlnew = DOMAINAD . $this->request->url;
        $this->Session->write('urlnew', $urlnew);

        // Xoa session thang search
        $this->Session->delete('catId');
        $this->Session->delete('keyword');
        $this->Session->delete('pagenew');


    }

    /**
     * Change position
     * @author Trung -Tong
     */
    function changepos() {
        $vitri = $_REQUEST['order'];
        $sphot = $_REQUEST['sphot'];

        // Update order
        foreach ($vitri as $k => $v) {
            if ($v == "") {
                $v = null;
            }
            $this->News->updateAll(
                array(
                'News.pos' => $v,
                'News.hot' => $sphot[$k]
                ), array(
                'News.id' => $k)
            );
        }
        if ($this->Session->check('pagenew')) {
            $this->redirect($this->Session->read('pagenew'));
        } else {
            $this->redirect(array('action' => 'index'));
        }
    }

    /**
     * Xu ly cac chuc nang lua chon theo so nhieu
     * @author Trung -Tong
     */
    function process() {
        $process = $_REQUEST['process'];
        $chon = $_REQUEST['chon'];
        if (count($chon) == 0 || $process < 1) {
            if ($this->Session->check('pagenew')) {
                $this->redirect($this->Session->read('pagenew'));
                exit;
            } else {
                $this->redirect('/news');
                exit;
            }
        }
        switch ($process) {
            case '1' :
                // Update active
                foreach ($chon as $k => $v) {
                    $this->News->updateAll(
                        array(
                        'News.status' => 1
                        ), array(
                        'News.id' => $k)
                    );
                }
                break;

            case '2' :
                // Update deactive
                foreach ($chon as $k => $v) {
                    $this->News->updateAll(
                        array(
                        'News.status' => 0
                        ), array(
                        'News.id' => $k)
                    );
                }
                break;

            case '3' :
                // delete many rows
                $groupId = "";
                foreach ($chon as $k => $v) {
                    $groupId .= "," . $k;
                }
                $groupId = substr($groupId, 1);
                $conditions = array(
                    'News.id IN (' . $groupId . ')'
                );
                $this->News->deleteAll($conditions);
                break;
        }
        if ($this->Session->check('pagenew')) {
            $this->redirect($this->Session->read('pagenew'));
        } else {
            $this->redirect(array('action' => 'index'));
        }
    }

    /**
     * Thêm sản phẩm
     * @author 
     */
    function add() {
        if (!empty($this->data)) {
            $this->News->create();
            $data = $this->request->data;

            /**
             * Upload file tuy bien
             * @author : 
             */
            if($_FILES['userfile']['name']) {
                $handle = new upload($_FILES['userfile']);
                if ($handle->uploaded) {

                    // Neu resize
//                $handle->image_resize          = true;
//                $handle->image_ratio_y        = true;
//                $handle->image_x                 = 790;

                    $filename = date('YmdHis') . md5(rand(10000, 99999));
                    $handle->file_new_name_body = $filename;

                    $handle->Process(IMAGES_URL . 'news');
                    if ($handle->processed) {
                        $img = $handle->file_dst_name;
                    }
                }
            } else {
                $img = null;
            }
            $data['News']['images'] = $img;
            if ($this->News->save($data['News'])) {
                if ($this->Session->check('pagenew')) {
                    $this->redirect($this->Session->read('pagenew'));
                } else {
                    $this->redirect(array('action' => 'index'));
                }
            }
        }

        // Load model
		 $this->loadModel("Layout");
        $list_layout = $this->Layout->find('list',array('conditions'=>array()));
        $this->set(compact('list_layout'));
		
		
    }



    //close bai viet
    function close($id = null) {
        $this->News->id = $id;
        $this->News->saveField('status', 0);
        $this->redirect($this->Session->read('urlnew'));
    }

    // active bai viet
    function active($id = null) {
        $this->News->id = $id;
        $this->News->saveField('status', 1);
        $this->redirect($this->Session->read('urlnew'));
    }

    /**
     * Tim kiem bai viet
     */
    function search() {
        if ($this->request->is('post')) {
            // Lay du lieu tu form
            $listCat = $_REQUEST['listCat'];
            $this->Session->write('catId', $listCat);

            // Get keyword
            $keyword = $_REQUEST['keyword'];
            $this->Session->write('keyword', $keyword);
        } else {
            $listCat = $this->Session->read('catId');
            $keyword = $this->Session->read('keyword');
        }

        // setup condition to search
        $condition = array();
        if (!empty($keyword)) {
            $condition[] = array(
                'News.name LIKE' => '%' . $keyword . '%'
            );
        }

        if ($listCat > 0) {
            $condition[] = array(
                'News.cat_id' => $listCat
            );
        }

        // Lưu đường dẫn để quay lại nếu update, edit, dellete
        $urlTmp = DOMAINAD . $this->request->url;
        $this->Session->write('pagenew', $urlTmp);

        // Sau khi lay het dieu kien sap xep vao 1 array
        $conditions = array();
        foreach ($condition as $values) {
            foreach ($values as $key => $cond) {
                $conditions[$key] = $cond;
            }
        }

        // Tang so thu tu * limit (example : 10)
        $urlTmp = DOMAINAD . $this->request->url;
        $urlTmp = explode(":", $urlTmp);
        if (isset($urlTmp[2])) {
            $startPage = ($urlTmp[2] - 1) * 10 + 1;
        } else {
            $startPage = 1;
        }
        $this->set('startPage', $startPage);

        // Simple to call data
        $this->paginate = array(
            'conditions' => $condition,
            'order' => 'News.pos DESC',
            'limit' => '10'
        );
        $product = $this->paginate('News');
        $this->set('product', $product);

      
    }

    // sua tin da dang
    function edit($id = null) {
        if (!$id && empty($this->request->data)) {
            $this->Session->setFlash(__('Không tồn tại ', true));
            if ($this->Session->check('pagenew')) {
                $this->redirect($this->Session->read('pagenew'));
            } else {
                $this->redirect(array('action' => 'index'));
            }
        }
        if (!empty($this->request->data)) {
            $data = $this->request->data;
			
            if ($_FILES['userfile']['name'] != "") {
                // Upload anh
                $handle = new upload($_FILES['userfile']);
                if ($handle->uploaded) {
                    $filename = date('YmdHis') . md5(rand(10000, 99999));
                    $handle->file_new_name_body = $filename;

                    $handle->Process(IMAGES_URL . 'news');
                    if ($handle->processed) {
                        $img = $handle->file_dst_name;
                    }
                }
            } else {
                $img = $_REQUEST['oldimg'];
            }

            $data['News']['images'] = $img;
            if ($this->News->save($data['News'])) {
                if ($this->Session->check('pagenew')) {
                    $this->redirect($this->Session->read('pagenew'));
                } else {
                    $this->redirect(array('action' => 'index'));
                }
            }
        }
        if (empty($this->request->data)) {
            $this->data = $this->News->read(null, $id);
        }


        // Edit tieng viet
        $edit_vie = $this->News->findById($id);
        $this->set('edit_vie', $edit_vie);
		
		 // Load model
		 $this->loadModel("Layout");
        $list_layout = $this->Layout->find('list',array('conditions'=>array()));
        $this->set(compact('list_layout'));

    }

    // Xoa cac dang
    function delete($id = null) {
        if (empty($id)) {
            $this->Session->setFlash(__('Không tồn tại bài viết này', true));
            //$this->redirect(array('action'=>'index'));
        }
        $this->News->id = $id;
        $this->News->saveField('delete_flag', 1);
        $this->redirect($this->Session->read('urlnew'));
		
        $this->Session->setFlash(__('Bài viết không xóa được', true));
        if ($this->Session->check('pagenew')) {
            $this->redirect($this->Session->read('pagenew'));
        } else {
            $this->redirect(array('action' => 'index'));
        }
    }

}