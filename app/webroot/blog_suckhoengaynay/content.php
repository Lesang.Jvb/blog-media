
                        <div class="sub-detail">
                            <span class="clock-subdetail">Đăng ngày 19/10/2017, 7:51 | Tác giả: Yến Nguyễn</span>
                        </div>
                        <div class="main-content">
       <p>
            Chào cả nhà,
        </p>


        <p>
            Là một bệnh nhân đã từng mắc bệnh <b style="color: red;">tiểu đường</b> nên tôi mới thấu hiểu được đây là một căn bệnh tưởng chừng có
            những biểu hiện khá đơn giản, nhưng hậu quả của nó thì vô cùng nghiêm trọng. Sau đây tôi xin
            chia sẻ với các bạn câu chuyện mà tôi chống lại “căn bệnh” này.
        </p>

        <p style="font: 26px/30px Arial, sans-serif;font-weight: bold;">
            Quá trình gửi gắm niềm tin khắp nơi tìm bài thuốc chữa tiểu đường:
        </p>
        <p style='text-align: center'>
            <img src='resource/image1.jpg' style='width: 96%'>
        </p>
        <p>
            Năm 2014, tôi phát hiện mình bị bệnh tiểu đường. Ban đầu chỉ số đường huyết của tôi cũng chỉ ở mức trung bình. Nên tôi không đi khám mà tìm những bài thuốc dân gian điều trị bệnh tiểu đường. Nghe nói mướp đắng có công dụng vì vậy tôi nhờ người mua mướp đắng khô ở phố Lãn Ông về pha với nước nóng để uống. Nhưng sau vài tháng ròng rã, đường huyết vẫn không có sự thay đổi tích cực nào.
        </p>
        <p>
           Tháng 10 – 2015, tôi chuyển sang đặt niềm tin vào Tây Y. Sau khi được thăm thăm khám và tư vấn, bác sĩ kê một số loại thuốc tây. Phải nói rằng thuốc Tây khá hiệu quả, song không biết do cơ địa hay quá lạm dụng thuốc mà tôi cảm thấy hệ tiêu hóa ảnh hưởng nghiêm trọng: tiêu chảy, đau bụng thậm chí nôn mửa rất khó chịu. Thật may cho tôi đã dừng uống thuốc tây trước khi bị loét dạ dày. 
        </p>
        <p style="font: 26px/30px Arial, sans-serif;font-weight: bold;">
            Và cuối cùng, sự cố gắng được đền đáp
        </p>
        <p>
            Càng sử dụng nhiều phương pháp thì tôi càng ngộ ra nhiều điều, cũng như thêm trân trọng sức khỏe của mình hơn. 
            Tết năm 2016, tôi được đứa cháu biếu hộp <a class="" href="#muahang" style="font-weight: bold;text-decoration: none">Tỏi đen Kochi NB</a> 
            và được giới thiệu loại tỏi này có công dụng hỗ trợ điều trị tiểu đường. 
            Tôi bán tín bán nghi cho nên đã dày công tìm hiểu trên internet, đồng thời tra cứu thông tin bệnh tật và những người cùng cảnh ngộ. 
            Khá bất ngờ vì rất nhiều người đã chia sẻ về tác dụng của <a class="" href="#muahang" style="font-weight:bold">Tỏi Đen</a>.
        </p>
        <p style='text-align: center'>
            <a href="#muahang"><img src='resource/image2.png' style='width: 96%'></a>
        <!--<h5 style="text-align:center;font-style: italic;font-weight: normal!important">( Tỏi Đen phòng ngừa và điều trị
            bệnh tiểu đường rất hiệu quả)</h5>-->
        </p>
        <p>
            Chính vì vậy, cuối cùng, tôi đã đặt niềm tin vào thứ tỏi này. Tôi đã tìm hiểu rất kỹ nên biết rằng 
            Tỏi Đen ngoài tác dụng với mỡ máu còn có thể hỗ trợ loại bỏ bệnh tiểu đường thông qua cơ chế điều hòa đường huyết.
        </p>
        <p style='text-align: center'>
            <a href="#muahang"><img src='resource/image3.jpg' style='width: 96%'></a>
        </p>
        <p>
            Trong các loại tỏi đen, <a
                    class="" href="#muahang" style="font-weight:bold">Tỏi đen một nhánh</a> có đặc điểm đặc biệt đó là
            chỉ có một tép tỏi
            nên toàn bộ
            hoạt chất đều tập trung vào một tép nên có giá trị chữa bệnh cao hơn tỏi thông thường nhiều lần.
            Nếu dùng tỏi một nhánh để lên men thành tỏi đen sẽ mang lại tác dụng tốt đối với sức khỏe.
        </p>
        <p>
            Và đây là kết quả đi khám của tôi:
        </p>
        <p style='text-align: center'>
            <img src='resource/anh004.jpg' style='width: 96%'/>
        </p>
        <p>
            Cầm kết quả trên tay mà tôi thực sự bất ngờ. Mừng quá, sau khi dùng hết hộp Tỏi đen được
            biếu, tôi hỏi địa chỉ đứa cháu để tiếp
            tục mua về dùng. Hóa ra, cháu tôi nó bảo tỏi này của là của Công ty TNHH MTV Xuất nhập khẩu và Phát triển
            công nghệ Nhật Bản. Lúc đầu tôi thấy đây là một công ty tư nhân bình thường nên cũng ko có gì ấn tượng. Tuy
            nhiên sau khi hỏi kỹ ra mới biết đây là một trong số ít những công ty áp dụng công nghệ của Nhật Bản trong
            việc sản xuất tỏi đen một nhánh. Như mọi người đã biết, Nhật Bản là quê hương của Tỏi đen, nên sản phẩm tỏi
            đen ủ và lên men bằng công nghệ hiện đại của đất nước mặt trời mọc sẽ có chất lượng tốt. Điều này đã
            khiến tôi tin tưởng vào chất lượng loại <a
                    class="" href="#muahang" style="font-weight:bold">Tỏi Đen Kochi – công nghệ Nhật Bản</a> này.
        </p>

        <p style='text-align: center'>
            <img src='resource/image5.png' style='width: 96%'>
        <h5 style="text-align:center;font-style: italic;font-weight: normal!important">(Giấy chứng nhận An toàn vệ sinh
            thực phẩm)</h5>
        </p>
        <p>
            Để mua được <a
                    class="" href="#muahang" style="font-weight:bold">Tỏi Đen Kochi</a> tôi được đứa cháu hướng dẫn chỉ cần đăng ký trên internet thôi, không cần phải đi đến tận trụ sở công ty, nên việc mua hàng rất tiện lợi. Đây là thông tin trên website của nhà cung cấp:
        </p>
		 <img src='resource/anhsua1.png' style='width: 100%'>

        <p>
            Hy vọng rằng, bằng những chia sẻ trên đây của tôi sẽ giúp ích cho bạn cũng như người thân trong việc
            <strong><em>phòng
                    ngừa và điều trị bệnh tiểu đường bằng Tỏi Đen</em></strong>.
        </p>

        <p style="text-align: center">
            Thân!!!
        </p>


        <h1 id="muahang" style="color: #4268ff;text-align: center">ĐĂNG KÝ MUA HÀNG</h1>
        <div class="row" style="width: 100%;margin-bottom: 20px;height: 275px">
            <div class="image" style="width: 50%;float: left">
                <img src="resource/toiden.jpg" style="width: 75%" class="img-responsive"/>
                <p style="color: #4268ff;text-align: center;font-size: 20px">
                    680.000 vnd/0.5 kg
                </p>
            </div>
            <div class="form" style="width: 50%;float: right">
                <form method="POST"
                      action="add.php/?aff_sub1=<?php if (isset($_GET['aff_sub1'])) echo $_GET['aff_sub1']; else echo 1 ?>"
                      id="dathang-form" onsubmit="return submitForm()">
                    <input type="text" class="form-control input-border" id="name" name="name" placeholder="Họ tên"
                           required/>
                    <input type="tel" class="form-control" name="phone" id="phone" placeholder="Số điện thoại"
                           required/>
                    <textarea class="form-control input-border" id="address" name="address" placeholder="Địa chỉ" required></textarea>
                    <input type="hidden" name="pid"
                           value="<?php if (isset($_GET['aff_sub1'])) echo $_GET['aff_sub1']; else echo 1 ?>">
                    <div class="text-center" style="position: relative;">
                        <img src="resource/dathang.png" style="z-index: 1"/>
                        <button type="submit" name="save"></button>
                    </div>
                </form>
            </div>
        </div>
        
        <!--h2 id="muahang" style="color: #4268ff;text-align: center; margin-bottom: 25px; font-weight: 600;">ĐĂNG KÝ MUA HÀNG</h2>
        <div class="formdk" style="width: 60%; margin: 0 auto;">
            <form method="POST" action="add.php/?aff_sub1=<?php if (isset($_GET['aff_sub1'])) echo $_GET['aff_sub1']; else echo 1 ?>" onsubmit="return submitForm()">
                <input type="text" class="form-control input-border" id="name" name="name" placeholder="Nhập tên" required/>
                <input type="text" class="form-control" id="phone" name="phone" placeholder="Số điện thoại" required/>
                <input type="text" class="form-control input-border" id="address" name="address" placeholder="Nhập địa chỉ" required/>
                <div class="text-center"><button class="button" style="width: 100%" type="submit" name="save">Đặt Hàng</button></div>
            </form>
        </div-->

        <div id="comment-title">Bình luận</div>
        <div class="comment">
            <img align="left" alt="" class="ava" height="50" src="./resource/lankhue.jpg" width="50">
            <span class="comment__name"><span style="color:#990000" class="name">Lan Khuê</span><br>
                <span style="color:black">Đặt hàng được mấy hôm rồi mà chưa thấy chuyển đến là sao?</span> 
            <p class="comment__info">
                11 phút trước |
                <span class="blue-text">Trả lời</span>
            </p>
        </div>
       <div class="comment" style="margin-left: 50px;">
            <img align="left" alt="" class="ava" height="50" src="./resource/tam.jpg" width="50">
            <span class="comment__name"><span style="color:#990000" class="name">Tâm</span><br>
                        <span style="color:black">Độ hôm sau  là nhận được thôi bạn.</a> </span>
            <p class="comment__info">
                13 phút trước |
                <span class="blue-text">Trả lời</span>
            </p>
        </div>
        <div class="comment">
            <img align="left" alt="" class="ava" height="50" src="./resource/lankhue.jpg" width="50">
            <span class="comment__name"><span style="color:#990000" class="name">Lan Khuê</span><br>
                        <span style="color:black">Đặt hàng được mấy hôm rồi mà chưa thấy chuyển đến là sao?</span>
                    <p class="comment__info">
                        13/8/2017 viết:
                    </p>
                </span></div>
        <div class="comment" style="margin-left: 50px;">
            <img align="left" alt="" class="ava" height="50" src="./resource/tam.jpg" width="50">
            <span class="comment__name"><span style="color:#990000" class="name">Tâm</span><br>
                        <span style="color:black">Độ hôm sau  là nhận được thôi bạn.</span>

                </span></div>
        <div class="comment">
            <img align="left" alt="" class="ava" height="50" src="./resource/nguyenduy.jpg" width="50">
            <span class="comment__name"><span style="color:#990000" class="name">Nguyễn Duy</span><br>
                        <span style="color:black">Vừa mua cho ông già ở nhà, không biết ăn thua gì ko?.</span>
                    <p class="comment__info">
                        12/8/2017 viết:
                    </p>
                </span></div>

        <div class="comment">
            <img align="left" alt="" class="ava" height="50" src="./resource/thuylinh.jpg" width="50">
            <span class="comment__name"><span style="color:#990000" class="name">Thùy Linh</span><br>
                        <span style="color:black">Mẹ em bị mỡ máu và tiểu đường thì có dùng đc ko ạ?</span>
                    <p class="comment__info">
                        11/8/2017 viết:

                    </p>
                </span></div>
        <div class="comment" style="margin-left: 50px;">
            <img align="left" alt="" class="ava" height="50" src="./resource/tam.jpg" width="50">
            <span class="comment__name"><span style="color:#990000" class="name">Tâm</span><br>
                        <span style="color:black">Đúng bệnh rồi đấy em.</span>

                </span></div>
        <div class="comment">
            <img align="left" alt="" class="ava" height="50" src="./resource/nguyenloan.jpg" width="50">
            <span class="comment__name"><span style="color:#990000" class="name">Nguyễn Loan</span><br>
                        <span style="color:black">Giao hàng hơi lâu, mấy hôm mới tới nơi.</span>
                    <p class="comment__info">
                        1/8/2017 viết:

                    </p>
                </span></div>
        <div class="comment" style="margin-left: 50px;">
            <img align="left" alt="" class="ava" height="50" src="./resource/tam.jpg" width="50">
            <span class="comment__name"><span style="color:#990000" class="name">Tâm</span><br>
                        <span style="color:black">Chắc do khách hàng đông quá nên họ ship hơi lâu.</span>

                </span></div>
        <div class="comment">
            <img align="left" alt="" class="ava" height="50" src="./resource/thuytrang.jpg" width="50">
            <span class="comment__name"><span style="color:#990000" class="name">Thùy Trang</span><br>
                        <span style="color:black">Tỏi ăn dẻo dỏe, thơm thơm như ô mai :D.</span>
                    <p class="comment__info">
                        9/8/2017 viết:
                    </p>
                </span></div>
        <div class="comment">
            <img align="left" alt="" class="ava" height="50" src="./resource/lankhue.png" width="50">
            <span class="comment__name"><span style="color:#990000" class="name">Lan Khuê</span><br>
                        <span style="color:black">Bố mình dùng đc 1 thời gian cũng thấy mỡ máu giảm đi đó. Nhưng văn phải ăn kiêng.</span>
                    <p class="comment__info">
                        8/8/2017 viết:
                    </p>
                </span></div>
        <div class="comment">
            <img align="left" alt="" class="ava" height="50" src="./resource/phamtuyen.jpg" width="50">
            <span class="comment__name"><span style="color:#990000" class="name">Phạm Tuyên</span><br>
                        <span style="color:black">Một thực phẩm nên dùng.</span>
                    <p class="comment__info">
                        8/8/2017 viết:
                    </p>
                </span></div>
        <div class="comment">
            <img align="left" alt="" class="ava" height="50" src="./resource/dieulinh.jpg" width="50">
            <span class="comment__name"><span style="color:#990000" class="name">Diệu Linh</span><br>
                        <span style="color:black">Chủ thớt nói đúng, chỉ có ai mắc bệnh mới thấu hiểu được nỗi khổ mà nó gây ra. </span>
                    <p class="comment__info">
                        7/8/2017 viết:
                    </p>
                </span></div>
        <div class="comment">
            <img align="left" alt="" class="ava" height="50" src="./resource/hoangquan.jpg" width="50">
            <span class="comment__name"><span style="color:#990000" class="name">Hoàng Quân </span><br>
                  <span style='color:black'> Từ 2015 mình vẫn duy trì thói quen ăn Tỏi đen đều đặn mỗi ngày.</span>
                    <p class="comment__info">
                        7/8/2017 viết:
                    </p>
        </div>
        <div class="comment">
            <img align="left" alt="" class="ava" height="50" src="./resource/thuydung.jpg" width="50">
            <span class="comment__name"><span style="color:#990000" class="name">Thúy Dung</span><br>
                        <span style="color:black">Mình hay đặt mua sản phẩm trên trang trực tuyến này, và duy trì mỗi ngày từ vài tép tỏi để có một sức khỏe tốt và ngăn ngừa các bệnh tuổi già.</span>
                    <p class="comment__info">
                        5/8/2017 viết:
                    </p>
                </span></div>
        <div class="comment">
            <img align="left" alt="" class="ava" height="50" src="./resource/quanghuan.jpg" width="50">
            <span class="comment__name"><span style="color:#990000" class="name">Quang Huân</span><br>
                        <span style="color:black">Tỏi Đen một nhánh quả thực rất tốt các bạn ạ, cả mình và bà xã đều dùng.  </span>
                    <p class="comment__info">
                        5/8/2017 viết:
                    </p>
                </span></div>
        <div class="comment">
            <img align="left" alt="" class="ava" height="50" src="./resource/danlinh.jpg" width="50">
            <span class="comment__name"><span style="color:#990000" class="name">Đan Linh</span><br>
                        <span style="color:black">Bệnh của mình cũng được thuyên giảm nhờ thói quen sử dụng tỏi đen. Một thực phẩm chức năng tốt, mọi người nên dùng.</span>
                    <p class="comment__info">
                        5/8/2017 viết:
                    </p>
                </span></div>
        <div class="comment">
            <img align="left" alt="" class="ava" height="50" src="./resource/hongnhung.jpg" width="50">
            <span class="comment__name"><span style="color:#990000" class="name">Hồng Nhung</span><br>
                        <span style="color:black">Do tuổi tác mà sức khỏe của chồng tôi không mấy được tốt, tôi đã cho chồng mình sử dụng rất nhiều loại thực phẩm chức năng mà cũng không thấy cải thiệnTôi đã được một người bạn mách cho bài thuốc về Tỏi Đen. Và thực sự tôi rất hài lòng.</span>
                    <p class="comment__info">
                        5/8/2017 viết:
                    </p>
                </span></div>
        <div class="comment">
            <img align="left" alt="" class="ava" height="50" src="./resource/nhatcuong.jpg" width="50">
            <span class="comment__name"><span style="color:#990000" class="name">Nhật Cường</span><br>
                        <span style="color:black">Nghe mấy người bạn cùng cơ quan giới thiệu về tỏi đen tôi đã mua về dùng thử và tôi thật sự hài lòng vì mùi vị của nó: Thơm và ngọt, giống mứt trái cây, rất thú vị . Giờ thì tôi đã yên tâm lựa chọn tỏi đen làm thực phẩm hỗ trợ để bảo vệ sức khỏe cho mình và gia đình.</span>
                    <p class="comment__info">
                        5/8/2017 viết:
                    </p>
                </span></div>

</div>
