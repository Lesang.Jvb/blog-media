<div class="col-lg-4 col-md-4 col-sm-12">
            <div class="article-wing">
                <div class="wing-title">
                    <h3>Tìm kiếm</h3>
                </div>
            </div>
            <form method="get">
                <div class="rbox-search">
                    <div class="box-searchthuoc">
                        <input class="form-control" name="q" placeholder="Nhập tên thuốc cần tìm..." type="text" />
                    </div>
                </div>
            </form>
            <div class="bg-lighter">
                <div class="article-chucmung">
                    <div class="img-chucmung">
                        <div class="responsive-image responsive-image--16by9">
                            <a href="">
                                <img class="lazy" src="<?php echo DOMAIN ?>/blog_suckhoengaynay/images/toi-den-cong-dung.jpg" />
                            </a>
                        </div>
                    </div>
                    <div class="txt-chucmung">
                        <h3>
                            <a>Công dụng bất ngờ của tỏi đen</a>
                        </h3>
                    </div>
                </div>
            </div>
            <div class="litle-new">
                <div class="litle-img">
                    <a href="http://www.hachishop.net/products/toi-den-blaga-hachishop-net/" target="_blank">
                        <img src="<?php echo DOMAIN ?>/blog_suckhoengaynay/images/phat-minh-vi.jpg" />
                    </a>
                </div>
                <div class="litle-title">
                    <a href="http://www.hachishop.net/products/toi-den-blaga-hachishop-net/" target="_blank">
                    Phát minh vĩ đại của Y học giúp các bệnh nhân tiểu đường
                    </a>
                </div>
                <div style="clear: both;"></div>
            </div>
            <div class="article-wing">
                <div class="wing-title">
                    <h3>Đọc nhiều nhất</h3>
                </div>
            </div>
            <div class="litle-new">
                <div class="litle-img">
                    <a href="http://www.hachishop.net/products/toi-den-kochi-hachishop-net/" target="_blank">
                        <img src="<?php echo DOMAIN ?>/blog_suckhoengaynay/images/toi-den-nhat.jpg" />
                    </a>
                </div>
                <div class="litle-title">
                    <a href="http://www.hachishop.net/products/toi-den-kochi-hachishop-net/" target="_blank">
                    Thảo dược quý đến từ Nhật Bản giúp đỡ người bị tiểu đường</a>
                </div>
                <div style="clear: both;"></div>
            </div>
            <div class="litle-new">
                <div class="litle-img">
                    <a href="http://www.hachishop.net/products/kem-flekosteel-dieu-tri-dau-nhuc-xuong-khop" target="_blank">
                        <img src="<?php echo DOMAIN ?>/blog_suckhoengaynay/images/het-dau.jpg" />
                    </a>
                </div>
                <div class="litle-title">
                    <a href="http://www.hachishop.net/products/kem-flekosteel-dieu-tri-dau-nhuc-xuong-khop" target="_blank">
                    Hết đau nhức xương khớp là do thứ này
                    </a>
                </div>
                <div style="clear: both;"></div>
            </div>
            <div class="litle-new">
                <div class="litle-img">
                    <a href="http://www.hachishop.net/products/toi-den-kochi-hachishop-net/" target="_blank">
                        <img src="<?php echo DOMAIN ?>/blog_suckhoengaynay/images/sai-qua-sai.jpg" />
                    </a>
                </div>
                <div class="litle-title">
                    <a href="http://www.hachishop.net/products/toi-den-kochi-hachishop-net/" target="_blank">
                    Không đọc bài viết này là một sai lầm quá lớn!
                    </a>
                </div>
                <div style="clear: both;"></div>
            </div>
            <div class="litle-new">
                <div class="litle-img">
                    <a href="http://www.hachishop.net/products/tri-minh-duong" target="_blank">
                        <img src="<?php echo DOMAIN ?>/blog_suckhoengaynay/images/ko-con-lam-phien.jpg" />
                    </a>
                </div>
                <div class="litle-title">
                    <a href="http://www.hachishop.net/products/tri-minh-duong" target="_blank">
                    Trĩ sẽ không còn làm phiền được nữa nếu làm theo cách này
                    </a>
                </div>
                <div style="clear: both;"></div>
            </div>
            <div class="litle-new">
                <div class="litle-img">
                    <a href="http://www.hachishop.net/products/toi-den-kochi-hachishop-net" target="_blank">
                        <img src="<?php echo DOMAIN ?>/blog_suckhoengaynay/images/toi-tuoi-toi-den.jpg" />
                    </a>
                </div>
                <div class="litle-title">
                    <a href="http://www.hachishop.net/products/toi-den-kochi-hachishop-net" target="_blank">
                    Tỏi tươi tốt hay tỏi đen tốt hơn?
                    </a>
                </div>
                <div style="clear: both;"></div>
            </div>
        </div>
        <div style="clear: both;"></div>