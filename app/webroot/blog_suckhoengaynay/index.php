<!DOCTYPE HTML>
<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf8" />
	<meta name="author" content="GallerySoft.info" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
	<title>Điều trị tiểu đường bằng tỏi đen</title>
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <link href="css/bootstrap-theme.css" rel="stylesheet" />
    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <link href="css/style.css" rel="stylesheet" />
    <?php
    if (isset($_GET['aff_sub1'])) {
        $pid = $_GET['aff_sub1'];
        require_once "PixelManager.php";
        $pixelmg = new PixelManager();
        echo $pixelmg->getPixelFile("aff_sub1", $pid);
    } else {
        $pid = 1;
        require_once "PixelManager.php";
        $pixelmg = new PixelManager();
        echo $pixelmg->getPixelFile("pid", $pid);
    }
    ?>
</head>

<body>
<div id="wrapper">
    <?php include "./layout/banner.php"; ?>
    <?php include "./layout/menu.php"; ?>
    <?php include "./layout/container.php"; ?>
    <?php include "./layout/menu-footer.php"; ?>
</div>



<script>
    function submitForm(){
        var name = document.getElementById("name").value;
        var phone = document.getElementById("phone").value;
        var address = document.getElementById("address").value;
        
        if(/   +/.test(name)){
            alert("Vui lòng kiểm tra lại phần nhập họ tên!");
        }else if(!/[0-9]+/.test(phone)){
            alert("Bạn chỉ được nhập số, không được nhập ký tự!");
        }else if(phone.slice(0,1) != 0){
            alert("Đầu số điện thoại không phải là '"+phone.slice(0,1)+"'");
        }else if(/   +/.test(address)){
            alert("Vui lòng kiểm tra lại phần nhập địa chỉ!");
        }else{
            return true;
        }
            return false;
        }
</script>
</body>
</html>