<?php
	class PixelManager{
		
		public function run(){
			if(isset($_GET['aff_sub1'])){
				$pixelID = $_GET['aff_sub1'];
				$Name = "aff_sub1";
				$Number = $pixelID;
				
				$dataContent = $this->getPixelFile($Name, $Number);
				if($dataContent != false)
				{
					echo $dataContent;
				}
				else{
					echo "<font color='red'>No Pixel</font>";
				}
			}
		}
		
		public function getPixelFile($Name, $Number){
			$path = $Name.$Number.".txt";
			if(file_exists($path))
			{
				$fileContent = file_get_contents($path);
				return $fileContent;
			}
			else{
				return false;
			}
		}
		
		private function readFile($path=""){
		  $result = "";
		  $file = fopen($path, "r") or die("Unable to open file!");
		  if(filesize($path)>0)
			  $result = fread($file, filesize($path));
		  fclose($file);
		  $result = trim($result);
		  return $result;
		}
	}
?>