<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8" />
	<meta http-equiv="content-type" content="text/html" />
	<meta name="author" content="GallerySoft.info" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<title>Cảm ơn bạn đã đặt hàng</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!-- Latest compiled and minified CSS --> 
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
    <!-- Optional theme -->
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css">
    <!-- Latest compiled and minified JavaScript -->
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
    <link href="style.css" rel="stylesheet" />
    <?php 
    if(isset($_GET['aff_sub1']))
    	{
    		$pid = $_GET['aff_sub1'];
    		require_once "PixelManager.php";
    		$pixelmg = new PixelManager();
    		echo $pixelmg->getPixelFile("aff_sub1", $pid); 
    	}else{
    		$pid = 1;
    		require_once "PixelManager.php";
    		$pixelmg = new PixelManager();
    		echo $pixelmg->getPixelFile("pid", $pid);
    	}
    ?>
    <script>
        fbq('track', 'Purchase');
    </script>
</head>

<body>
    <div class="container">
        <h1 class="success-page__title">
            XIN CHÚC MỪNG! ĐƠN HÀNG CỦA BẠN ĐÃ ĐƯỢC CHẤP NHẬN!
        </h1>
        <p>
            Tiếp theo, nhân viên trung tâm dịch vụ khách hàng sẽ liên hệ với bạn để xác nhận đơn hàng.Hãy đảm bảo điện thoại của bạn luôn bật.
        </p>
        
        <h5>Click <a href="../<?php if(isset($_GET['aff_sub1'])){echo '?aff_sub1='.$_GET['aff_sub1'];}else{echo '?aff_sub1=1';}?>">vào đây</a> để quay trở lại</h5>
    </div>
</body>
</html>