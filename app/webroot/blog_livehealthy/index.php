<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8" />
	<meta http-equiv="content-type" content="text/html" />
	<meta name="author" content="GallerySoft.info" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<title>Live Healthy</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!-- Latest compiled and minified CSS --> 
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css"/>
    <!-- Optional theme -->
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css"/>
    <!-- Latest compiled and minified JavaScript -->
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
    <link href="css/style.css" rel="stylesheet" />
    <?php 
    if(isset($_GET['aff_sub1']))
    	{
    		$pid = $_GET['aff_sub1'];
    		require_once "PixelManager.php";
    		$pixelmg = new PixelManager();
    		echo $pixelmg->getPixelFile("aff_sub1", $pid); 
    	}else{
    		$pid = 1;
    		require_once "PixelManager.php";
    		$pixelmg = new PixelManager();
    		echo $pixelmg->getPixelFile("pid", $pid);
    	}
    ?>
</head>

<body>
<div id="wrapper">
    <?php include_once './layout/menu.php'; ?>
    <?php include_once './layout/container.php'; ?>
</div>
<script>
        function submitForm(){
        var name = $('#name').val();
        var phone = $('#phone').val();
        var address = $('#address').val();
        var quantity = $('#quantity').val();
        var note = $('#note').val();
        
        if(/   +/.test(name)){
            $(".js_errorMessage_name").show();
            $(".js_errorMessage_name").html('Bạn vui lòng kiểm tra lại họ tên.');
            $(".js_errorMessage_address").hide();
            $(".js_errorMessage_phone").hide();
            $(".js_errorMessage_quantity").hide();
            $("#name").focus();
        }else if(phone.slice(0,1) != 0){
            $(".js_errorMessage_phone").show();
            $(".js_errorMessage_phone").html('Bạn vui lòng kiểm tra lại số điện thoại.');
            $(".js_errorMessage_name").hide();
            $(".js_errorMessage_address").hide();
            $(".js_errorMessage_quantity").hide();
            $("#phone").focus();
        }else if(/   +/.test(address)){
            $(".js_errorMessage_address").show();
            $(".js_errorMessage_address").html('Bạn vui lòng kiểm tra lại địa chỉ.');
            $(".js_errorMessage_name").hide();
            $(".js_errorMessage_phone").hide();
            $(".js_errorMessage_quantity").hide();
            $("#address").focus();
        }else if(/   +/.test(note)){
            $(".js_errorMessage_note").show();
            $(".js_errorMessage_note").html('Bạn vui lòng kiểm tra lại mã sản phẩm.');
            $(".js_errorMessage_name").hide();
            $(".js_errorMessage_phone").hide();
            $(".js_errorMessage_quantity").hide();
            $("#note").focus();
        }else if(quantity < 1){
            $(".js_errorMessage_quantity").show();
            $(".js_errorMessage_quantity").html('Bạn vui lòng nhập số lượng lớn hơn 0.');
            $(".js_errorMessage_name").hide();
            $(".js_errorMessage_address").hide();
            $(".js_errorMessage_phone").hide();
            $("#quantity").focus();
        }else{
            $(".js_errorMessage_name").hide();
            $(".js_errorMessage_address").hide();
            $(".js_errorMessage_phone").hide();
            $(".js_errorMessage_quantity").hide();
            return true;
        }
        return false;
    }
</script>  
</body>
</html>