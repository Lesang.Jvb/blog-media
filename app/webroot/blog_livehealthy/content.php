<p style="font-size: 22px; font-family: time new roman; font-weight: bold;"> 
        Nàng điệu đi biển, khăn tắm cũng phải điệu
    </p>
    <div class="sub-detail">
        <span class="clock-subdetail">Đăng ngày 20/03/2018, 7:51 | Tác giả: Huyền Nguyễn</span>
    </div>
    <div class="main-content">
    
    <p>
            Chào cả nhà,
        </p>
        <p>
             Trong hành trang đi biển của chị em, để bắt cặp cùng món đồ bơi xinh xắn không thể thiếu những chiếc 
             <a href="#dathang-form" style="text-decoration: none; font-weight: bold; color: #1979ca;">khăn tắm</a>. 
             Chính vì vậy mà “khăn tắm đi biển và những tiêu chí không được bỏ qua” là từ khóa các bạn nữ tìm kiếm rất nhiều 
             trong thời gian gần đây, đặc biệt là dịp lễ 30/4-1/5 đang đến gần. Đây là những mẫu khăn tròn đi biển dưới đây 
             đang được chị em săn lùng trong hè 2018.
        </p>
        <p style="text-align: center; cursor: pointer;">
            <a href="#dathang-form"><img src="resource/anh1.jpg" style="width: 63%" /></a>
        </p>
        <p>
            Mỗi chiếc khăn tắm như một bức tranh mùa hè sinh động và nhiều màu sắc. Bạn sẽ trở nên thật tuyệt vời khi tự kết hợp 
            chiếc khăn với bộ bikini gợi cảm của mình, đôi khi chỉ cần một chút tinh ý trong việc chọn lựa chiếc khăn tắm thì bạn 
            đã trở thành tâm điểm của bãi biển mùa hè năm nay. 
        </p>
        <p style="text-align: center; cursor: pointer;">
            <a href="#dathang-form"><img src="resource/anh2.jpg" style="width: 63%" /></a>
        </p>
        <p>
            Với công dụng chung của khăn là giúp lau khô như những chiếc khăn tắm thông thường thì bạn sẽ trở nên sành điệu và 
            khuyến rũ hơn khi biến hóa chiếc khăn trải dùng ngả lưng khi bạn muốn thư giãn mà không muốn cát dính lên cơ thể mình.
        </p>
        <p style="text-align: center; cursor: pointer;">
            <a href="#dathang-form"><img src="resource/anh10.jpg" style="width: 63%" /></a>
        </p>
        <p>
            Trong khoảng thời gian ngâm mình dưới biển để nô đùa, tắm biển và khi rời khỏi mặt nước thì chiếc khăn tắm hầu như là vật 
            dụng không thể thiếu trong lúc này. Giúp phái đẹp chúng ta trở lên gợi cảm, duyên dáng hơn bao giờ hết khi đi bơi, dạo 
            quanh bờ biển.
        </p>
        <p style="text-align: center; cursor: pointer;">
            <a href="#dathang-form"><img src="resource/anh4.jpg" style="width: 63%" /></a>
        </p>
        <p>
            Tưởng như khăn tắm chưa từng được quan tâm nhiều, nhưng chúng ta đều biết, những chiếc khăn tắm chính là phông nền để tạo 
            nên những bức ảnh hoàn hảo, khi mà mỗi cô gái đều muốn tự biến mình thành một siêu mẫu trên bãi biển. Nếu như coi bờ cát 
            trắng là sàn catwalk, thì những chiếc khăn tắm choàng bên vai chính là đôi cánh thiên thần của mỗi nàng thơ, dưới làn gió 
            biển bay phấp phơ như khiến những thân hình nóng bỏng dưới hai mảnh bikini kia bay xa hơn, bồng bềnh hơn, che chở, vỗ về 
            và tôn vinh nữ tính.
        </p>
        <p style="text-align: center; cursor: pointer;">
            <a href="#dathang-form"><img src="resource/anh5.jpg" style="width: 63%" /></a>
        </p>
        <p>
            Đắm chìm vào thế giới khăn tắm siêu điệu mới thấy rõ sự muôn hình vạn trạng của các loại khăn tắm. Nếu ai đó nghĩ rằng 
            khăn tắm chỉ có mỗi hình chữ nhật thì đã nhầm to, các nàng điệu bây giờ cũng có đủ kiểu khăn tắm vô cùng sáng tạo. 
            Nổi bật ngay từ kiểu dáng mà chưa cần tính đến màu sắc hay họa tiết, thế là các nàng đã thắng thế ngay khi bước chân 
            lên bãi biển với ánh nhìn tò mò của những người xung quanh. Cũng đúng thôi, vì một lần nữa phải nhắc lại rằng, cái khăn 
            tắm nó chẳng phải có công dụng duy nhất để lau người. Nó còn là cái thảm trải để nằm phơi nắng, để nằm thả dáng, để bày 
            tiệc trà, nói như vậy, khăn tắm còn là đại diện của phòng ngủ, phòng khách, phòng ăn của các nàng. Đừng hỏi tại sao nó 
            lại phức tạp như vậy!
        </p>
        <p>
            Bộ sưu tập Khăn tắm tròn mới nhất năm 2018
        </p>
        <p style="text-align: center; cursor: pointer;">
            <a href="#dathang-form"><img src="resource/anh9.jpg" style="width: 63%" /></a>
            <p style="text-align: center;"><i>(Mẫu số 01)</i></p>
        </p>
        <br />
        <p style="text-align: center; cursor: pointer;">
            <a href="#dathang-form"><img src="resource/anh10.jpg" style="width: 63%" /></a>
            <p style="text-align: center;"><i>(Mẫu số 02)</i></p>
        </p>
        <br />
        <p style="text-align: center; cursor: pointer;">
            <a href="#dathang-form"><img src="resource/anh11.jpg" style="width: 63%" /></a>
            <p style="text-align: center;"><i>(Mẫu số 03)</i></p>
        </p>
        <br />
        <p style="text-align: center; cursor: pointer;">
            <a href="#dathang-form"><img src="resource/anh12.jpg" style="width: 63%" /></a>
            <p style="text-align: center;"><i>(Mẫu số 04)</i></p>
        </p>
        <br />
        <p style="text-align: center; cursor: pointer;">
            <a href="#dathang-form"><img src="resource/anh13.jpg" style="width: 63%" /></a>
            <p style="text-align: center;"><i>(Mẫu số 05)</i></p>
        </p>
        <br />
        <p style="text-align: center; cursor: pointer;">
            <a href="#dathang-form"><img src="resource/anh14.jpg" style="width: 63%" /></a>
            <p style="text-align: center;"><i>(Mẫu số 06)</i></p>
        </p>
        <br />
        <p style="text-align: center; cursor: pointer;">
            <a href="#dathang-form"><img src="resource/anh15.jpg" style="width: 63%" /></a>
            <p style="text-align: center;"><i>(Mẫu số 07)</i></p>
        </p>
        
        <br />
        
        <h1 id="dathang-form" style="color: #4268ff;text-align: center; margin: 50px 0 50px 0;">Combo 2 sản phẩm giá 549.000 vnđ</h1>
        <div class="containers">
            <div class="col-lg-6 col-md-6 col-sm-12 anhsp">
                <img src="resource/anh6.jpg" class="img-responsive" style="width: 78%;" />
                <h4>299.000 Vnđ/1 sản phẩm</h4>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 form-dangky">
                <form method="post" action="add.php?aff_sub1=<?php if(isset($_GET['aff_sub1'])){echo $_GET['aff_sub1'];}else{echo 1;} ?>"
                    onsubmit="return submitForm()">
                        <div class="js_errorMessage_name" style="color: #F00;font-family: Arial;font-size: 14px; display:none"></div>
                    <input class="form-control" type="text" name="name" id="name" placeholder="Họ tên" required="required" />
                        <div class="js_errorMessage_phone" style="color: #F00;font-family: Arial;font-size: 14px; display:none"></div>
                    <input class="form-control" type="number" id="phone" name="phone"  placeholder="Số điện thoại" required="required" />
                        <div class="js_errorMessage_quantity" style="color: #F00;font-family: Arial;font-size: 14px; display:none"></div>
                    <input class="form-control" type="number" name="quantity" id="quantity" placeholder="Số lượng" required="required" />
                        <div class="js_errorMessage_note" style="color: #F00;font-family: Arial;font-size: 14px; display:none"></div>
                    <input class="form-control" type="text" name="note" id="note" placeholder="Mã sản phẩm" required="required" />
                        <div class="js_errorMessage_address" style="color: #F00;font-family: Arial;font-size: 14px; display:none"></div>
                    <textarea class="form-control" name="address" id="address" placeholder="Địa chỉ" required="required" ></textarea>
                    <div class="text-center"><button class="button" style="width: 100%" type="submit" name="submit">Đặt Hàng</button></div>
                </form>
            </div>
        </div>
        
        
        <div style="clear: both;"></div>
        <div id="comment-title">Bình luận</div>
        
        <div class="comment">
            <img align="left" alt="" class="ava" height="50" src="./resource/nguyenthitam01.jpg" width="50">
            <span class="comment__name"><span style="color:#990000" class="name">Nguyễn Thị Tâm</span><br>
                <span style="color:black">Chị lấy 2 cái, số 1 và số 5. Địa chỉ p3107, tòa 34T, Hoàng Đạo Thúy. Số đt 0982459188 nhé.</span>
            <p class="comment__info">
                16 phút trước |
                <span class="blue-text">Trả lời</span>

            </p>
        </div>
        <div class="comment">
            <img align="left" alt="" class="ava" height="50" src="./resource/ngocnguyen01.jpg" width="50">
            <span class="comment__name"><span style="color:#990000" class="name">Ngoc Nguyen</span><br>
                <span style="color:black">Hihi, có người làm quen với em nhờ tấm khăn này đấy, bắt đền shop, 
                    giờ chuẩn em bị cưới r.</span>
            <p class="comment__info">
                20 phút trước |
                <span class="blue-text">Trả lời</span>

            </p>
        </div>

        <div class="comment">
            <img align="left" alt="" class="ava" height="50" src="./resource/trangpham01.jpg" width="50">
            <span class="comment__name"><span style="color:#990000" class="name">Trang Phạm</span><br>
                <span style="color:black">Mua về để đi biển, thằng con đòi quấn nằm đi ngủ luôn.</span>
            <p class="comment__info">
                36 phút trước |
                <span class="blue-text">Trả lời</span>
            </p>
        </div>
        <div class="comment">
            <img align="left" alt="" class="ava" height="50" src="./resource/phamtuyen01.jpg" width="50">
            <span class="comment__name"><span style="color:#990000" class="name">Phạm Tuyên</span><br>
                <span style="color:black">Thuận tiện khi đi biển.</span>
            <p class="comment__info">
                39 phút trước |
                <span class="blue-text">Trả lời</span>
            </p>
        </div>
        <div class="comment">
            <img align="left" alt="" class="ava" height="50" src="./resource/phamhuong01.jpg" width="50">
            <span class="comment__name"><span style="color:#990000" class="name">Phạm Hương</span><br>
                <span style="color:black">-	Con em đi biển rất thích, shop nhập thêm vài mẫu hoạt hình thì 
                    nhắn mình nhé. 0982180292.
            </span>
            <p class="comment__info">
                52 phút trước |
                <span class="blue-text">Trả lời</span>
            </p>
        </div>
    
    </div>