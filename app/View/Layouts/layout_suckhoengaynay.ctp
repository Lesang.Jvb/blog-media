<!DOCTYPE HTML>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="author" content="GallerySoft.info"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <?php
        if (isset($keywords_for_layout)) {
            echo "<meta name='keywords' content='" . $keywords_for_layout . "' /> ";
        }
        if (isset($description_for_layout)) {
            echo "<meta name='description' content='" . $description_for_layout . "' />";
        }
        ?>
        <title>
            <?php
            if (isset($title_for_layout)) {
                echo $title_for_layout;
            }
            $domain = DOMAIN;
            ?>
        </title>
    <link href="<?php echo DOMAIN ?>/blog_suckhoengaynay/css/bootstrap.min.css" rel="stylesheet"/>
    <link href="<?php echo DOMAIN ?>/blog_suckhoengaynay/css/bootstrap-theme.css" rel="stylesheet"/>
    <script src="<?php echo DOMAIN ?>/blog_suckhoengaynay/js/jquery-3.2.1.min.js"></script>
    <script src="<?php echo DOMAIN ?>/blog_suckhoengaynay/js/bootstrap.min.js"></script>
    <link href="<?php echo DOMAIN ?>/blog_suckhoengaynay/css/style.css" rel="stylesheet"/>
<?php 	
	$code_home = $this->Session->read('code_home');
	
	if(isset($code_home)){
	?>
	<!-- Facebook Pixel Code -->
	<script>
	  !function(f,b,e,v,n,t,s)
	  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
	  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
	  n.queue=[];t=b.createElement(e);t.async=!0;
	  t.src=v;s=b.getElementsByTagName(e)[0];
	  s.parentNode.insertBefore(t,s)}(window, document,'script',
	  'https://connect.facebook.net/en_US/fbevents.js');
	  fbq('init', '<?php echo $code_home;?>');
	  fbq('track', 'PageView');
	</script>
	<noscript><img height="1" width="1" style="display:none"
	  src="https://www.facebook.com/tr?id=<?php echo $code_home;?>&ev=PageView&noscript=1"
	/></noscript>
	<!-- End Facebook Pixel Code -->
	
<?php }?>
</head>

<body>
<div id="wrapper">
    <div class="container" style="max-width: 1100px;">
        <div class="banner">
            <div class="logo">
                <img src="<?php echo DOMAIN ?>/blog_suckhoengaynay/images/logo.png" class="img-responsive" />
            </div>
            <div class="bannerqc">
                <img src="<?php echo DOMAIN ?>/blog_suckhoengaynay/images/bannerqc.gif" class="img-responsive" />
            </div>
        </div>
    </div>
    <div class="menu">
        <ul class="container">
            <li><a>
                <img alt="Trang chủ" src="<?php echo DOMAIN ?>/blog_suckhoengaynay/images/home.png" />
            </a></li>
            <li><a href="">Thời sự</a></li>
            <li><a href="">Khám bệnh online</a></li>
            <li><a href="">Dịch vụ y</a></li>
            <li><a href="">Chat với bác sĩ</a></li>
            <li><a href="">Video</a></li>
            <li><a href="">Sức khỏe a-z</a></li>
            <li><a href="">Tim mạch</a></li>
            <li><a href="">Tiểu đường</a></li>
            <li><a href="">Ung thư</a></li>
            <li><a href="">Đột quỵ</a></li>
        </ul>
    </div>
    <div style="clear: both;"></div>
    <div class="container khuvuc01">
        <div class="col-lg-8 col-md-8 col-sm-12">
            <div class="article">
                <div class="detail-article">
                    <!-- content -->
                    <div class="main-content">
                        <!-- content -->
                        <?php echo $content_for_layout; ?>
                    </div>
                </div>
                <div style="clear: both;"></div>
                <div class="sub-detail" style="height: 20px;"></div>
                    <div class="cothebanqt">
                        <h4>Bản Tin Sức Khỏe</h4>
                        <div class="slider-nong247">
                            <div class="litle-nong247">
                                <div class="litle-nong247-img">
                                    <a href="http://www.hachishop.net/products/toi-den-kochi-hachishop-net" target="_blank">
                                        <img src="<?php echo DOMAIN ?>/blog_suckhoengaynay/images/an-toi-den-hang-ngay.jpg" class="img-responsive" />
                                    </a>
                                </div>
                                <div class="litle-nong247-title">
                                    <h3><a href="http://www.hachishop.net/products/toi-den-kochi-hachishop-net" target="_blank">
                                    Ăn tỏi đen hàng ngày rất tốt cho tim mạch</a></h3>
                                    <span class="comment__info">07/01/2018</span> 
                                </div>
                            </div>
                            <div class="litle-nong247">
                                <div class="litle-nong247-img">
                                    <a href="http://www.hachishop.net/products/kem-flekosteel-dieu-tri-dau-nhuc-xuong-khop" target="_blank">
                                        <img src="<?php echo DOMAIN ?>/blog_suckhoengaynay/images/mach-nho.jpg" class="img-responsive" />
                                    </a>
                                </div>
                                <div class="litle-nong247-title">
                                    <h3><a href="http://www.hachishop.net/products/kem-flekosteel-dieu-tri-dau-nhuc-xuong-khop" target="_blank">
                                    Mách nhỏ cho người đang bị đau xương khớp</a></h3>
                                    <span class="comment__info">08/01/2018</span> 
                                </div>
                            </div>
                            <div class="litle-nong247" style="margin-right: 0;">
                                <div class="litle-nong247-img">
                                    <a href="http://www.hachishop.net/products/wakasatogenky-dac-tri-tieu-duong" target="_blank">
                                        <img src="<?php echo DOMAIN ?>/blog_suckhoengaynay/images/bi-tieu-duong-thi.jpg" class="img-responsive" />
                                    </a>
                                </div>
                                <div class="litle-nong247-title">
                                    <h3><a href="http://www.hachishop.net/products/wakasatogenky-dac-tri-tieu-duong" target="_blank">
                                    Bị tiểu đường thì nên ăn gì?</a></h3>
                                    <span class="comment__info">07/01/2018</span> 
                                </div>
                            </div>
                        </div>
                    </div>
                <div style="clear: both;"></div>
                <div class="tinmoinhat">
                    <div class="title-new01">
                        <h2><a class="title24">Tin cùng chuyên mục</a></h2>
                    </div>
                    <div class="box-new01" style="background: white;">
                        <div class="litle-new01">
                            <div class="litle-new01-img">
                                <a href="http://www.hachishop.net/products/kem-flekosteel-dieu-tri-dau-nhuc-xuong-khop" target="_blank">
                                    <img src="<?php echo DOMAIN ?>/blog_suckhoengaynay/images/dau-nhuc-xuong.jpg" class="img-responsive" />
                                </a>
                            </div>
                            <a href="http://www.hachishop.net/products/kem-flekosteel-dieu-tri-dau-nhuc-xuong-khop" target="_blank">
                                <p style="color: #555555;">Đau nhức xương khớp sẽ không còn hành hạ nếu làm theo cách này</p>
                            </a>
                        </div>
                        <div class="litle-new01">
                            <div class="litle-new01-img">
                                <a href="http://www.hachishop.net/products/tri-minh-duong/" target="_blank">
                                    <img src="<?php echo DOMAIN ?>/blog_suckhoengaynay/images/bai-thuoc-dong-y.jpg" class="img-responsive" />
                                </a>
                            </div>
                            <a href="http://www.hachishop.net/products/tri-minh-duong/" target="_blank">
                                <p style="color: #555555;">Bài thuốc Đông Y giúp ngăn ngừa các tác nhân của bệnh trĩ</p>
                            </a>
                        </div>
                        <div class="litle-new01" style="margin-left: 0;">
                            <div class="litle-new01-img">
                                <a href="http://www.hachishop.net/products/toi-den-blaga-hachishop-net/" target="_blank">
                                    <img src="<?php echo DOMAIN ?>/blog_suckhoengaynay/images/thuc-pham-an-hang-ngay.jpg" class="img-responsive" />
                                </a>
                            </div>
                            <a href="http://www.hachishop.net/products/toi-den-blaga-hachishop-net/" target="_blank">
                                <p style="color: #555555;">Loại thực phẩm ăn hàng ngày tốt cho sức khỏe</p>
                            </a>
                        </div>
                        <div style="clear: both;"></div>
                    </div>
                    <div class="box-new01" style="background: white;">
                        <div class="litle-new01">
                            <div class="litle-new01-img">
                                <a href="http://www.hachishop.net/products/wakasatogenky-dac-tri-tieu-duong" target="_blank">
                                    <img src="<?php echo DOMAIN ?>/blog_suckhoengaynay/images/vi-sao-nhan-sam.jpg" class="img-responsive" />
                                </a>
                            </div>
                            <a href="http://www.hachishop.net/products/wakasatogenky-dac-tri-tieu-duong" target="_blank">
                                <p style="color: #555555;">Vì sao nhân sâm tốt cho sức khỏe?</p>
                            </a>
                        </div>
                        <div class="litle-new01">
                            <div class="litle-new01-img">
                                <a href="http://www.hachishop.net/products/toi-den-kochi-hachishop-net/" target="_blank">
                                    <img src="<?php echo DOMAIN ?>/blog_suckhoengaynay/images/thao-duoc.jpg" class="img-responsive" />
                                </a>
                            </div>
                            <a href="http://www.hachishop.net/products/toi-den-kochi-hachishop-net/" target="_blank">
                                <p style="color: #555555;">Thảo dược tốt cho sức khỏe đến từ Nhật Bản</p>
                            </a>
                        </div>
                        <div class="litle-new01" style="margin-left: 0;">
                            <div class="litle-new01-img">
                                <a href="http://www.hachishop.net/products/toi-den-blaga-hachishop-net/" target="_blank">
                                    <img src="<?php echo DOMAIN ?>/blog_suckhoengaynay/images/phuong-phap-don-gian.jpg" class="img-responsive" />
                                </a>
                            </div>
                            <a href="http://www.hachishop.net/products/toi-den-blaga-hachishop-net/" target="_blank">
                                <p style="color: #555555;">Phương pháp đơn giản loại bỏ bệnh tiểu đường</p>
                            </a>
                        </div>
                        <div style="clear: both;"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-12">
            <div class="article-wing">
                <div class="wing-title">
                    <h3>Tìm kiếm</h3>
                </div>
            </div>
            <form method="get">
                <div class="rbox-search">
                    <div class="box-searchthuoc">
                        <input class="form-control" name="q" placeholder="Nhập tên thuốc cần tìm..." type="text" />
                    </div>
                </div>
            </form>
            <div class="bg-lighter">
                <div class="article-chucmung">
                    <div class="img-chucmung">
                        <div class="responsive-image responsive-image--16by9">
                            <a href="">
                                <img class="lazy" src="<?php echo DOMAIN ?>/blog_suckhoengaynay/images/toi-den-cong-dung.jpg" />
                            </a>
                        </div>
                    </div>
                    <div class="txt-chucmung">
                        <h3>
                            <a>Công dụng bất ngờ của tỏi đen</a>
                        </h3>
                    </div>
                </div>
            </div>
            <div class="litle-new">
                <div class="litle-img">
                    <a href="http://www.hachishop.net/products/toi-den-blaga-hachishop-net/" target="_blank">
                        <img src="<?php echo DOMAIN ?>/blog_suckhoengaynay/images/phat-minh-vi.jpg" />
                    </a>
                </div>
                <div class="litle-title">
                    <a href="http://www.hachishop.net/products/toi-den-blaga-hachishop-net/" target="_blank">
                    Phát minh vĩ đại của Y học giúp các bệnh nhân tiểu đường
                    </a>
                </div>
                <div style="clear: both;"></div>
            </div>
            <div class="article-wing">
                <div class="wing-title">
                    <h3>Đọc nhiều nhất</h3>
                </div>
            </div>
            <div class="litle-new">
                <div class="litle-img">
                    <a href="http://www.hachishop.net/products/toi-den-kochi-hachishop-net/" target="_blank">
                        <img src="<?php echo DOMAIN ?>/blog_suckhoengaynay/images/toi-den-nhat.jpg" />
                    </a>
                </div>
                <div class="litle-title">
                    <a href="http://www.hachishop.net/products/toi-den-kochi-hachishop-net/" target="_blank">
                    Thảo dược quý đến từ Nhật Bản giúp đỡ người bị tiểu đường</a>
                </div>
                <div style="clear: both;"></div>
            </div>
            <div class="litle-new">
                <div class="litle-img">
                    <a href="http://www.hachishop.net/products/kem-flekosteel-dieu-tri-dau-nhuc-xuong-khop" target="_blank">
                        <img src="<?php echo DOMAIN ?>/blog_suckhoengaynay/images/het-dau.jpg" />
                    </a>
                </div>
                <div class="litle-title">
                    <a href="http://www.hachishop.net/products/kem-flekosteel-dieu-tri-dau-nhuc-xuong-khop" target="_blank">
                    Hết đau nhức xương khớp là do thứ này
                    </a>
                </div>
                <div style="clear: both;"></div>
            </div>
            <div class="litle-new">
                <div class="litle-img">
                    <a href="http://www.hachishop.net/products/toi-den-kochi-hachishop-net/" target="_blank">
                        <img src="<?php echo DOMAIN ?>/blog_suckhoengaynay/images/sai-qua-sai.jpg" />
                    </a>
                </div>
                <div class="litle-title">
                    <a href="http://www.hachishop.net/products/toi-den-kochi-hachishop-net/" target="_blank">
                    Không đọc bài viết này là một sai lầm quá lớn!
                    </a>
                </div>
                <div style="clear: both;"></div>
            </div>
            <div class="litle-new">
                <div class="litle-img">
                    <a href="http://www.hachishop.net/products/tri-minh-duong" target="_blank">
                        <img src="<?php echo DOMAIN ?>/blog_suckhoengaynay/images/ko-con-lam-phien.jpg" />
                    </a>
                </div>
                <div class="litle-title">
                    <a href="http://www.hachishop.net/products/tri-minh-duong" target="_blank">
                    Trĩ sẽ không còn làm phiền được nữa nếu làm theo cách này
                    </a>
                </div>
                <div style="clear: both;"></div>
            </div>
            <div class="litle-new">
                <div class="litle-img">
                    <a href="http://www.hachishop.net/products/toi-den-kochi-hachishop-net" target="_blank">
                        <img src="<?php echo DOMAIN ?>/blog_suckhoengaynay/images/toi-tuoi-toi-den.jpg" />
                    </a>
                </div>
                <div class="litle-title">
                    <a href="http://www.hachishop.net/products/toi-den-kochi-hachishop-net" target="_blank">
                    Tỏi tươi tốt hay tỏi đen tốt hơn?
                    </a>
                </div>
                <div style="clear: both;"></div>
            </div>
        </div>
        <div style="clear: both;"></div>
    </div>
    <div class="menu menu-footer">
        <ul class="container" style="max-width: 1100px;">
            <li>
                <a href=""><img src="<?php echo DOMAIN ?>/blog_suckhoengaynay/images/home.png" alt="Home" /></a>
            </li>
            <li>
                <a href="">Khám bệnh online</a>
            </li>
            <li>
                <a href="">Hỏi đáp dịch vụ y tế</a>
            </li>
            <li>
                <a href="">Tư vấn trực tiếp</a>
            </li>
            <li>
                <a href="">Ưu đãi - Khuyến mãi</a>
            </li>
            <li>
                <a href="">Thuốc</a>
            </li>
            <li>
                <a href="">Sức khỏe a-z</a>
            </li>
            <li>
                <a href="">Video</a>
            </li>
        </ul>
    </div>
</div>



<script>
    function submitForm(){
        var name = document.getElementById("name").value;
        var phone = document.getElementById("phone").value;
        var address = document.getElementById("address").value;
        
        if(/   +/.test(name)){
            alert("Vui lòng kiểm tra lại phần nhập họ tên!");
        }else if(!/[0-9]+/.test(phone)){
            alert("Bạn chỉ được nhập số, không được nhập ký tự!");
        }else if(phone.slice(0,1) != 0){
            alert("Đầu số điện thoại không phải là '"+phone.slice(0,1)+"'");
        }else if(/   +/.test(address)){
            alert("Vui lòng kiểm tra lại phần nhập địa chỉ!");
        }else{
            return true;
        }
            return false;
        }
</script>
</body>
</html>