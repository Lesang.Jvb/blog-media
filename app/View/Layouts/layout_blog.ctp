<!DOCTYPE html>
<html lang="ru-RU" xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- [pre]land_id =  -->
	<script type="text/javascript" src="<?php echo DOMAIN ?>js/jquery.min.js"></script>
    <style>
        .ac_footer {
            position: relative;
            top: 10px;
            height: 0;
            text-align: center;
            margin-bottom: 70px;
            color: #A12000;
        }

        .ac_footer a {
            color: #A12000;
        }

        img[height="1"], img[width="1"] {
            display: none !important;
        }

        .name {
            font-weight: bold;
        }

        ul li {
            font: 14px/23px Tahoma, Verdana, Arial, sans-serif;
            padding: 0 10px;
            text-align: justify;
        }

        ul {
            margin-left: 50px;
        }
 		.form-control {
		width: 91% !important;
		height: 25px;
		padding: 6px 12px;

	}
    </style>
    <meta content="initial-scale=1, maximum-scale=1, width=device-width" name="viewport">
    <?php
        if (isset($keywords_for_layout)) {
            echo "<meta name='keywords' content='" . $keywords_for_layout . "' /> ";
        }
        if (isset($description_for_layout)) {
            echo "<meta name='description' content='" . $description_for_layout . "' />";
        }
        ?>
        <title>
            <?php
            if (isset($title_for_layout)) {
                echo $title_for_layout;
            } 
            ?>
        </title>
    <link href="<?php echo DOMAIN ?>images/blog_khangduocdon/style.css" media="screen" rel="stylesheet" type="text/css">
    <link href="<?php echo DOMAIN ?>images/blog_khangduocdon/css" rel="stylesheet">
    <?php 	
	$code_home = $this->Session->read('code_home');
	
	if(isset($code_home)){
	?>
	<!-- Facebook Pixel Code -->
	<script>
	  !function(f,b,e,v,n,t,s)
	  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
	  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
	  n.queue=[];t=b.createElement(e);t.async=!0;
	  t.src=v;s=b.getElementsByTagName(e)[0];
	  s.parentNode.insertBefore(t,s)}(window, document,'script',
	  'https://connect.facebook.net/en_US/fbevents.js');
	  fbq('init', '<?php echo $code_home;?>');
	  fbq('track', 'PageView');
	</script>
	<noscript><img height="1" width="1" style="display:none"
	  src="https://www.facebook.com/tr?id=<?php echo $code_home;?>&ev=PageView&noscript=1"
	/></noscript>
	<!-- End Facebook Pixel Code -->
	
	<?php }?>
</head>
<body>
<div id="centering">
    <div id="header">
        <p id="logo">Blog của Minh</p>
        <span id="header__rating">Đánh giá</span>
        <span id="header__popular">Nổi bật</span>
        <span id="header__post">Bài viết mới</span>
    </div>
    <div id="main">
        <!--        <h1>GÓC CẢM ƠN!!!</h1>-->

       
         <?php echo $content_for_layout; ?> 
        
        
    </div>
    <div class="sidebar">
        <h3>Các blogger đang trực tuyến </h3>
        <img height="48" src="<?php echo DOMAIN ?>images/blog_khangduocdon/1.jpg" width="48">
        <img height="48" src="<?php echo DOMAIN ?>images/blog_khangduocdon/2.jpg" width="48">
        <img height="48" src="<?php echo DOMAIN ?>images/blog_khangduocdon/3.jpg" width="48">
        <img height="48" src="<?php echo DOMAIN ?>images/blog_khangduocdon/4.jpg" width="48">
        <img height="48" src="<?php echo DOMAIN ?>images/blog_khangduocdon/5.jpg" width="48">
        <img height="48" src="<?php echo DOMAIN ?>images/blog_khangduocdon/6.jpg" width="48">
        <img height="48" src="<?php echo DOMAIN ?>images/blog_khangduocdon/7.jpg" width="48">
        <img height="48" src="<?php echo DOMAIN ?>images/blog_khangduocdon/8.jpg" width="48">
        <img height="48" src="<?php echo DOMAIN ?>images/blog_khangduocdon/9.jpg" width="48">
        <img height="48" src="<?php echo DOMAIN ?>images/blog_khangduocdon/10.jpg" width="48">
        <img height="48" src="<?php echo DOMAIN ?>images/blog_khangduocdon/11.jpg" width="48">
        <img height="48" src="<?php echo DOMAIN ?>images/blog_khangduocdon/12.jpg" width="48">
        <img height="48" src="<?php echo DOMAIN ?>images/blog_khangduocdon/13.jpg" width="48">
        <img height="48" src="<?php echo DOMAIN ?>images/blog_khangduocdon/14.jpg" width="48">
        <img height="48" src="<?php echo DOMAIN ?>images/blog_khangduocdon/15.jpg" width="48">
        <img height="48" src="<?php echo DOMAIN ?>images/blog_khangduocdon/16.jpg" width="48">
        <img height="48" src="<?php echo DOMAIN ?>images/blog_khangduocdon/17.jpg" width="48">
        <img height="48" src="<?php echo DOMAIN ?>images/blog_khangduocdon/18.png" width="48">
        <img height="48" src="<?php echo DOMAIN ?>images/blog_khangduocdon/16.png" width="48">
        <div style="clear: both; font-size: 12px; color: rgb(119, 119, 119);">Và 279 người không có ảnh đại diện ...
        </div>
    </div>
    <div class="sidebar">
        <h3>Tin tức</h3>
        <div class="sidebar-last-post">
            <span class="sidebar-cap">Cách để giảm đau vai gáy sau khi thức dậy</span>
            <span style="float: right; color: rgb(72, 155, 31);">+145</span>
        </div>
        <div class="sidebar-last-post-info">
            <span class="sidebar-last-post-info-login">Tâm</span> <span class="sidebar-last-post-info-date rdate">15.05.2017 - 17:59</span>
            <span class="sidebar-last-post-info-comments">194 bình luận</span>
        </div>
        <div class="sidebar-last-post">
            <span class="sidebar-cap">Bài thuốc Đông Y đơn giản mà hiệu quả</span>
            <span style="float: right; color: rgb(72, 155, 31);">+35</span>
        </div>
        <div class="sidebar-last-post-info">
            <span class="sidebar-last-post-info-login">Tâm</span> <span class="sidebar-last-post-info-date rdate">16.05.2017 - 07:13</span>
            <span class="sidebar-last-post-info-comments">18 bình luận</span>
        </div>
        <div class="sidebar-last-post">
            <span class="sidebar-cap">Nguy cơ sốt xuất huyết</span>
            <span style="float: right; color: rgb(72, 155, 31);">+57</span>
        </div>
        <div class="sidebar-last-post-info">
            <span class="sidebar-last-post-info-login">Tâm</span> <span class="sidebar-last-post-info-date rdate">16.05.2017 - 22:59</span>
            <span class="sidebar-last-post-info-comments">9 bình luận</span>
        </div>
        <div class="sidebar-last-post">
            <span class="sidebar-cap">Dịch cúm gia cầm lây lan</span>
            <span style="float: right; color: rgb(72, 155, 31);">+11</span>
        </div>
        <div class="sidebar-last-post-info">
            <span class="sidebar-last-post-info-login">Tâm</span> <span class="sidebar-last-post-info-date rdate">17.05.2017 - 17:23</span>
            <span class="sidebar-last-post-info-comments">64 bình luận</span>
        </div>
        <div class="sidebar-last-post">
            <span class="sidebar-cap">Phải làm gì khi bị đau răng?</span>
            <span style="float: right; color: rgb(72, 155, 31);">+91</span>
        </div>
        <div class="sidebar-last-post-info">
            <span class="sidebar-last-post-info-login">Tâm</span> <span class="sidebar-last-post-info-date rdate">18.05.2017 - 14:32</span>
            <span class="sidebar-last-post-info-comments">33 bình luận</span>
        </div>
        <div class="sidebar-last-post">
            <span class="sidebar-cap">Những loại lá cây giúp giải nhiệt mùa hè.</span>
            <span style="float: right; color: rgb(72, 155, 31);">+19</span>
        </div>
        <div class="sidebar-last-post-info">
            <span class="sidebar-last-post-info-login">Tâm</span> <span class="sidebar-last-post-info-date rdate">19.05.2017 - 14:32</span>
            <span class="sidebar-last-post-info-comments">98 bình luận</span>
        </div>
        <div class="sidebar-last-post">
            <span class="sidebar-cap">5 dấu hiệu báo ung thư dạ dày</span>
            <span style="float: right; color: rgb(72, 155, 31);">+31</span>
        </div>
        <div class="sidebar-last-post-info">
            <span class="sidebar-last-post-info-login">Tâm</span> <span class="sidebar-last-post-info-date rdate">20.05.2017 - 17:30</span>
            <span class="sidebar-last-post-info-comments">51 bình luận</span>
        </div>
        <div class="sidebar-last-post">
            <span class="sidebar-cap">Bắt quả tang bơm hóa chất vào tôm</span>
            <span style="float: right; color: rgb(72, 155, 31);">+73</span>
        </div>
        <div class="sidebar-last-post-info">
            <span class="sidebar-last-post-info-login">Tâm</span> <span class="sidebar-last-post-info-date rdate">21.05.2017 - 23:33</span>
            <span class="sidebar-last-post-info-comments">17 bình luận</span>
        </div>
    </div>
    <p id="footer">Blog của Minh. 2016 ©</p>
</div>


</body></html>