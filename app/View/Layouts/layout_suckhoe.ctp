<!DOCTYPE html>
<!-- saved from url=(0055)#muahang -->
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="author" content="GallerySoft.info">
<meta name="viewport" content="width=device-width, initial-scale=1">
<?php
        if (isset($keywords_for_layout)) {
            echo "<meta name='keywords' content='" . $keywords_for_layout . "' /> ";
        }
        if (isset($description_for_layout)) {
            echo "<meta name='description' content='" . $description_for_layout . "' />";
        }
        ?>
        <title>
            <?php
            if (isset($title_for_layout)) {
                echo $title_for_layout;
            } 
            ?>
        </title>
<link href="<?php echo DOMAIN ?>images/blog_suckhoe/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo DOMAIN ?>images/blog_suckhoe/bootstrap-theme.css" rel="stylesheet">
<script src="<?php echo DOMAIN ?>images/blog_suckhoe/jquery-3.2.1.min.js.tải xuống"></script>
<script src="<?php echo DOMAIN ?>images/blog_suckhoe/bootstrap.min.js.tải xuống"></script>
<link href="<?php echo DOMAIN ?>images/blog_suckhoe/style.css" rel="stylesheet">
<?php 	
	$code_home = $this->Session->read('code_home');
	
	if(isset($code_home)){
	?>
	<!-- Facebook Pixel Code -->
	<script>
	  !function(f,b,e,v,n,t,s)
	  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
	  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
	  n.queue=[];t=b.createElement(e);t.async=!0;
	  t.src=v;s=b.getElementsByTagName(e)[0];
	  s.parentNode.insertBefore(t,s)}(window, document,'script',
	  'https://connect.facebook.net/en_US/fbevents.js');
	  fbq('init', '<?php echo $code_home;?>');
	  fbq('track', 'PageView');
	</script>
	<noscript><img height="1" width="1" style="display:none"
	  src="https://www.facebook.com/tr?id=<?php echo $code_home;?>&ev=PageView&noscript=1"
	/></noscript>
	<!-- End Facebook Pixel Code -->
	
	<?php }?>
</head>

<body>
<div id="wrapper">
  <div class="container" style="max-width: 1100px;">
    <div class="banner">
      <div class="logo"> <img src="<?php echo DOMAIN ?>images/blog_suckhoe/logo.png" class="img-responsive"> </div>
      <div class="bannerqc"> <img src="<?php echo DOMAIN ?>images/blog_suckhoe/bannerqc.gif" class="img-responsive"> </div>
    </div>
  </div>
  <div class="menu">
    <ul class="container">
      <li><a> <img alt="Trang chủ" src="<?php echo DOMAIN ?>images/blog_suckhoe/home.png"> </a></li>
      <li><a href="#muahang">Thời sự</a></li>
      <li><a href="#muahang">Khám bệnh online</a></li>
      <li><a href="#muahang">Dịch vụ y</a></li>
      <li><a href="#muahang">Chat với bác sĩ</a></li>
      <li><a href="#muahang">Video</a></li>
      <li><a href="#muahang">Sức khỏe a-z</a></li>
      <li><a href="#muahang">Tim mạch</a></li>
      <li><a href="#muahang">Tiểu đường</a></li>
      <li><a href="#muahang">Ung thư</a></li>
      <li><a href="#muahang">Đột quỵ</a></li>
    </ul>
  </div>
  <div style="clear: both;"></div>
  <div class="container khuvuc01">
    <div class="col-lg-8 col-md-8 col-sm-12">
      <div class="article">
        <div class="top-detail">
          <h2><a>Thuốc</a></h2>
        </div>
        <div class="detail-article">
          <div class="main-content"> 
		      <?php echo $content_for_layout; ?>
          </div>
        </div>
        <div style="clear: both;"></div>
        <div class="sub-detail" style="height: 20px;"></div>
        <div class="cothebanqt">
          <h4>Có thể bạn quan tâm</h4>
          <div class="slider-nong247">
            <div class="litle-nong247">
              <div class="litle-nong247-img"> <a href="#muahang"><img src="<?php echo DOMAIN ?>images/blog_suckhoe/11aXOPV_002.jpg"></a> </div>
              <div class="litle-nong247-title">
                <h3><a href="#muahang">Hôm nay, xét xử ông Đinh La Thăng và đồng phạm</a></h3>
                <span class="comment__info">08/01/2018</span> </div>
            </div>
            <div class="litle-nong247">
              <div class="litle-nong247-img"> <a href="#muahang"><img src="<?php echo DOMAIN ?>images/blog_suckhoe/tram-be.jpg"></a> </div>
              <div class="litle-nong247-title">
                <h3><a href="#muahang">Luật sư: Ông Trầm Bê giảm sút sức khoẻ trước phiên xử</a></h3>
                <span class="comment__info">07/01/2018</span> </div>
            </div>
            <div class="litle-nong247" style="margin-right: 0;">
              <div class="litle-nong247-img"> <a href="#muahang"><img src="<?php echo DOMAIN ?>images/blog_suckhoe/74dbuc-thu-a.jpg"></a> </div>
              <div class="litle-nong247-title">
                <h3><a href="#muahang">Bức thư của bà cụ 83 tuổi là điều bạn nên đọc mỗi ngày</a></h3>
                <span class="comment__info">07/01/2018</span> </div>
            </div>
          </div>
        </div>
        <div style="clear: both;"></div>
        <div class="tinmoinhat">
          <div class="title-new01">
            <h2><a class="title24">Tin mới nhất</a></h2>
          </div>
          <div class="box-new01">
            <div class="litle-new01">
              <div class="litle-new01-img"> <a href="#muahang"><img src="<?php echo DOMAIN ?>images/blog_suckhoe/Canxi-va-vitamin-D-khong-phai-la-giai-phap-phong-ngua-lo_003.jpg"></a> </div>
              <p>Canxi và vitamin D không phải là giải pháp phòng ngừa loãng xương? </p>
            </div>
            <div class="litle-new01">
              <div class="litle-new01-img"> <a href="#muahang"><img src="<?php echo DOMAIN ?>images/blog_suckhoe/6e714Corticoid.jpg"></a> </div>
              <p>Thuốc Corticoid - dao hai lưỡi nguy hiểm </p>
            </div>
            <div class="litle-new01" style="margin-left: 0;">
              <div class="litle-new01-img"> <a href="#muahang"><img src="<?php echo DOMAIN ?>images/blog_suckhoe/Buon-non-do-thuoc-tri-dau-rang-1.jpg"></a> </div>
              <p>Buồn nôn do thuốc trị đau răng? </p>
            </div>
            <div style="clear: both;"></div>
          </div>
        </div>
        <div class="tinmoinhat">
          <div class="title-new01">
            <h2><a class="title24">Tin cùng chuyên mục</a></h2>
          </div>
          <div class="box-new01" style="background: white;">
            <div class="litle-new01">
              <div class="litle-new01-img"> <a href="#muahang"><img src="<?php echo DOMAIN ?>images/blog_suckhoe/di-ung3_002.jpg"></a> </div>
              <p style="color: #555555;">Dùng thuốc chống dị ứng thường xuyên, hại gì?</p>
            </div>
            <div class="litle-new01">
              <div class="litle-new01-img"> <a href="#muahang"><img src="<?php echo DOMAIN ?>images/blog_suckhoe/Co-the-rut-ngan-thoi-gian-dieu-tri-lao-phoi-nho-vitamin_002.jpg"></a> </div>
              <p style="color: #555555;">Có thể rút ngắn thời gian điều trị lao phổi nhờ vitamin C</p>
            </div>
            <div class="litle-new01" style="margin-left: 0;">
              <div class="litle-new01-img"> <a href="#muahang"><img src="<?php echo DOMAIN ?>images/blog_suckhoe/Canxi-va-vitamin-D-khong-phai-la-giai-phap-phong-ngua-lo_002.jpg"></a> </div>
              <p style="color: #555555;">Canxi và vitamin D không phải là giải pháp phòng ngừa loãng xương?</p>
            </div>
            <div style="clear: both;"></div>
          </div>
          <div class="box-new01" style="background: white;">
            <div class="litle-new01">
              <div class="litle-new01-img"> <a href="#muahang"><img src="<?php echo DOMAIN ?>images/blog_suckhoe/thuoc-viem-bang-quang.jpg"></a> </div>
              <p style="color: #555555;">Bất lợi khi dùng thuốc chữa viêm bàng quang</p>
            </div>
            <div class="litle-new01">
              <div class="litle-new01-img"> <a href="#muahang"><img src="<?php echo DOMAIN ?>images/blog_suckhoe/Buon-non-do-thuoc-tri-dau-rang-1.jpg"></a> </div>
              <p style="color: #555555;">Buồn nôn do thuốc trị đau răng? </p>
            </div>
            <div class="litle-new01" style="margin-left: 0;">
              <div class="litle-new01-img"> <a href="#muahang"><img src="<?php echo DOMAIN ?>images/blog_suckhoe/thuoc13.jpg"></a> </div>
              <p style="color: #555555;">Có nên dùng đồng thời thuốc hen và thuốc huyết áp?</p>
            </div>
            <div style="clear: both;"></div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-12">
      <div class="article-wing">
        <div class="wing-title">
          <h3>Thuốc</h3>
        </div>
      </div>
      <form method="get">
        <div class="rbox-search">
          <div class="box-searchthuoc">
            <input class="form-control" name="q" placeholder="Nhập tên thuốc cần tìm..." type="text">
          </div>
        </div>
      </form>
      <div class="bg-lighter">
        <div class="article-chucmung">
          <div class="img-chucmung">
            <div class="responsive-image responsive-image--16by9"> <a href="#muahang"> <img class="lazy" src="<?php echo DOMAIN ?>images/blog_suckhoe/Su-dung-thuoc-tang-co-co-the-mac-ung-thu-tinh-hoan-1.jpg"> </a> </div>
          </div>
          <div class="txt-chucmung">
            <h3> <a>Sử dụng thuốc tăng cơ có thể mắc ung thư tinh hoàn</a> </h3>
            <p> Việc dùng thuốc tăng cơ có thể làm tăng nguy cơ mắc bệnh ung thư tinh hoàn, theo một nghiên cứu ở Mỹ. </p>
          </div>
        </div>
      </div>
      <div class="litle-new">
        <div class="litle-img"> <img src="<?php echo DOMAIN ?>images/blog_suckhoe/Canxi-va-vitamin-D-khong-phai-la-giai-phap-phong-ngua-loang-.jpg"> </div>
        <div class="litle-title"> <a href="#muahang">Canxi và vitamin D không phải là giải pháp phòng ngừa loãng xương?</a> </div>
        <div style="clear: both;"></div>
      </div>
      <div class="article-wing">
        <div class="wing-title">
          <h3>Đọc nhiều nhất</h3>
        </div>
      </div>
      <div class="litle-new">
        <div class="litle-img"> <img src="<?php echo DOMAIN ?>images/blog_suckhoe/Day-la-ly-do-vi-sao-khong-nen-cho-tre-con-xem-youtube-1.jpg"> </div>
        <div class="litle-title"> <a href="#muahang">Vì sao không nên cho trẻ con xem youtube?</a> </div>
        <div style="clear: both;"></div>
      </div>
      <div class="litle-new">
        <div class="litle-img"> <img src="<?php echo DOMAIN ?>images/blog_suckhoe/200ca8.jpg"> </div>
        <div class="litle-title"> <a href="#muahang">Cá hồi - Ăn bao nhiêu một tuần để tốt cho sức khỏe?</a> </div>
        <div style="clear: both;"></div>
      </div>
      <div class="litle-new">
        <div class="litle-img"> <img src="<?php echo DOMAIN ?>images/blog_suckhoe/Nhung-thuc-pham-tot-cho-nguoi-thieu-mau-1.jpg"> </div>
        <div class="litle-title"> <a href="#muahang">Những thực phẩm tốt cho người thiếu máu</a> </div>
        <div style="clear: both;"></div>
      </div>
      <div class="litle-new">
        <div class="litle-img"> <img src="<?php echo DOMAIN ?>images/blog_suckhoe/Hoi-phuc-suc-khoe-sau-nhung-tran-nhau-say-tuy-luy-1.jpg"> </div>
        <div class="litle-title"> <a href="#muahang">Hồi phục sức khỏe sau những trận nhậu say túy lúy</a> </div>
        <div style="clear: both;"></div>
      </div>
      <div class="litle-new">
        <div class="litle-img"> <img src="<?php echo DOMAIN ?>images/blog_suckhoe/Ngu-khong-du-giac-co-the-dan-toi-nhung-benh-ly-gi-1.jpg"> </div>
        <div class="litle-title"> <a href="#muahang">Ngủ không đủ giấc có thể dẫn tới những bệnh lý gì?</a> </div>
        <div style="clear: both;"></div>
      </div>
    </div>
    <div style="clear: both;"></div>
  </div>
  <div class="menu menu-footer">
    <ul class="container" style="max-width: 1100px;">
      <li> <a href="#muahang"><img src="<?php echo DOMAIN ?>images/blog_suckhoe/home.png" alt="Home"/></a> </li>
      <li> <a href="#muahang">Khám bệnh online</a> </li>
      <li> <a href="#muahang">Hỏi đáp dịch vụ y tế</a> </li>
      <li> <a href="#muahang">Tư vấn trực tiếp</a> </li>
      <li> <a href="#muahang">Ưu đãi - Khuyến mãi</a> </li>
      <li> <a href="#muahang">Thuốc</a> </li>
      <li> <a href="#muahang">Sức khỏe a-z</a> </li>
      <li> <a href="#muahang">Video</a> </li>
    </ul>
  </div>
</div>

</body>
</html>