<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style class="vjs-styles-defaults">
.video-js {
	width: 300px;
	height: 150px;
}
.vjs-fluid {
	padding-top: 56.25%
}
</style>
<style>
   .button-support{
	   position:fixed;
	   right:5px;
	   bottom:1px;
	   padding-top:10px;
	   width:300px;
	   background:#F00;
	   }
	.form-button-support{
		display:none;
		}
		
	.button-title-support{
		background:#F00;
		color:#FFF;
		font-weight:bold;
		font-size:20px;
		text-align:center;
		    padding-bottom: 10px;
		cursor:pointer;
		}
	.form-control {
    display: block;
    width: 78%;
    height: 34px;
    padding: 6px 12px;
    font-size: 14px;
    line-height: 1.42857143;
    color: #555;
    background-color: #fff;
    background-image: none;
    border: 1px solid #ccc;
    border-radius: 4px;
    -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
    box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
    -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
    -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
    transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
}

.form-button-support{
	width:100%;
	 padding:20px;
	 padding-top:5px;
	 padding-bottom:5px;
	}
	
.btn-danger {
    color: #000;
    background-color: #ffe000;
    border-color: #d43f3a;
}

.btn {
    display: inline-block;
    padding: 6px 27px;
    margin-bottom: 0;
    font-size: 18px;
    font-weight: 400;
    line-height: 1.42857143;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    -ms-touch-action: manipulation;
    touch-action: manipulation;
    cursor: pointer;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    background-image: none;
    border: 1px solid transparent;
    border-radius: 4px;
}
</style>
<meta http-equiv="REFRESH" content="1800">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

<!---->
<?php
        if (isset($keywords_for_layout)) {
            echo "<meta name='keywords' content='" . $keywords_for_layout . "' /> ";
        }
        if (isset($description_for_layout)) {
            echo "<meta name='description' content='" . $description_for_layout . "' />";
        }
        ?>
        <title>
            <?php
            if (isset($title_for_layout)) {
                echo $title_for_layout;
            } 
            ?>
            </title>
<script type="text/javascript" src="<?php echo DOMAIN;?>js/jquery-3.2.1.min.js"></script>
<link href="<?php echo DOMAIN;?>blog_tamsu/reset.css" rel="stylesheet">
<link href="<?php echo DOMAIN;?>blog_tamsu/detail.css" rel="stylesheet">
<link href="<?php echo DOMAIN;?>blog_tamsu/style.css" rel="stylesheet">
<link href="<?php echo DOMAIN;?>blog_tamsu/response.css" rel="stylesheet">
 <style>
      
 		.form-control {
		width: 91% !important;
		height: 25px;
		padding: 6px 12px;

	}
    </style>
    <?php 	
	$code_home = $this->Session->read('code_home');
	
	if(isset($code_home)){
	?>
	<!-- Facebook Pixel Code -->
	<script>
	  !function(f,b,e,v,n,t,s)
	  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
	  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
	  n.queue=[];t=b.createElement(e);t.async=!0;
	  t.src=v;s=b.getElementsByTagName(e)[0];
	  s.parentNode.insertBefore(t,s)}(window, document,'script',
	  'https://connect.facebook.net/en_US/fbevents.js');
	  fbq('init', '<?php echo $code_home;?>');
	  fbq('track', 'PageView');
	</script>
	<noscript><img height="1" width="1" style="display:none"
	  src="https://www.facebook.com/tr?id=<?php echo $code_home;?>&ev=PageView&noscript=1"
	/></noscript>
	<!-- End Facebook Pixel Code -->
	
	<?php }?>
</head>
<body>
<div class="blogtamsu-wrapper">
  <div class="btsw-content">
    <header class="btswc-header">
      <p class="logomobile"><img src="<?php echo DOMAIN;?>blog_tamsu/iconlogo.png"></p>
      <div class="btswch-topmenu-wrapper">
        <div class="w1000">
          <ul class="btmw-list clearfix">
            <li class="btmwli"> <a href="#muahang" class="home"></a> </li>
            <li class="btmwli"> <a href="#muahang" class="link"> TV Show </a> </li>
            <li class="btmwli"> <a href="#muahang" class="link">Thời trang</a> </li>
            <li class="btmwli"> <a href="#muahang" class="link"> Về chúng tôi </a> </li>
            <li class="btmwli last"> <a href="#muahang" class="btmwli-icon instagram"></a> <a href="#muahang" class="btmwli-icon facebook"></a> <a href="#muahang" class="btmwli-icon youtube"></a> </li>
          </ul>
        </div>
      </div>
      <div class="btswch-middle-wrapper">
        <div class="w1000"> <a href="#muahang" class="bmw-logo-bts"></a>
          <div class="bmw-content clearfix">
            <div class="bmwc-banner fr">
              <div class="banner-ads" style="width:100%;height:65px;  float: left;margin:0 auto;padding:0px 0px 0"> 
                <script type="text/javascript">Poly_Top();</script> 
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="btswch-bottommenu-wrapper">
        <div class="w1000">
          <div class="bbtmw-content">
            <ul class="bbmw-list">
              <li class="bbmwli  "> <a href="">Trang
                chủ</a> </li>
              <li class="bbmwli"> <a href="">Sao</a>
                <div class="btmwli-submenu">
                  <ul class="btmwli-list">
                    <li class="btmwlism"> <a href="#muahang">Sao Việt</a> </li>
                    <li class="btmwlism"> <a href="#muahang">Sao Ngoại</a> </li>
                    <li class="btmwlism"> <a href="#muahang">Nhan Sắc</a> </li>
                    <li class="btmwlism"> <a href="#muahang">Sao &amp; Fan</a> </li>
                  </ul>
                </div>
              </li>
              <li class="bbmwli"> <a href="#muahang">Đẹp</a>
                <div class="btmwli-submenu">
                  <ul class="btmwli-list">
                    <li class="btmwlism"><a href="#muahang">Đẹp như Sao</a></li>
                    <li class="btmwlism"><a href="#muahang">Tips</a></li>
                    <li class="btmwlism"><a href="#muahang">Đẹp công sở</a></li>
                  </ul>
                </div>
              </li>
              <li class="bbmwli"> <a href="#muahang">Nhạc</a> </li>
              <li class="bbmwli"> <a href="#muahang">Phim</a> </li>
              <li class="bbmwli"> <a href="#muahang">TV Show</a>
                <div class="btmwli-submenu">
                  <ul class="btmwli-list">
                    <li class="btmwlism"><a href="#muahang">Trong nước</a></li>
                    <li class="btmwlism"><a href="#muahang">Nước ngoài</a> </li>
                  </ul>
                </div>
              </li>
              <li class="bbmwli"> <a href="#muahang">Sức khỏe</a> </li>
              <li class="bbmwli"> <a href="#muahang">Ngẫm</a> </li>
            </ul>
          </div>
        </div>
      </div>
    </header>
    <section class="btswc-body">
      <div class="w1000">
        
        <div class="btswc-special-wrapper clearfix">
          <div class="bsw-content">
            <div class="btswc-newlayout-wrapper clearfix">
              <div class="bnlw-left fl"> 
                
                <!-- box content -->
                <div class="clearfix"></div>
                <div class="box-middle-detail">
                  <ul class="sub-breakcrumb">
                    <li><a href="#muahang">Sức khỏe</a></li>
                    <li>ThuyLF</li>
                    <li>05-12-2017</li>
                  </ul>
                  <div class="clearfix"></div>
                  <h2 class="sapo_detai">(Blogtamsu) -  Chào cả nhà, </h2>
                  <div class="main-content-detail">
                    <div id="main"> 
                     <?php echo $content_for_layout; ?> 
                    </div>
                  </div>
                  <div class="clearfix"></div>
                  <div class="clear-float"></div>
                  <div class="clear-float"></div>
                  <div class="clear-float"></div>
                </div>
                <!-- end box contecnt --> 
                <!-- box bottom content   -->
                <div class="clearfix"></div>
                <div class="box-botton-detail">
                  <div class="bnlwl-same-wrapper border-bottom clearfix box-same-detail">
                    <div class="bsw-left music fl"> <a href="#muahang" class="bnlwl-title">Âm nhạc</a>
                      <ul class="bswl-list">
                        <li class="bswlli">
                          <div class="bswlli-content clearfix"> <a href="#muahang" class="bswllic-avatar"> <img src="<?php echo DOMAIN;?>blog_tamsu/phalebisotmv-21-of-25-e1512446577456-niepenw5ey0cb2yexwgxfkfr5vj49e1pwc9hpexswo.jpg"> </a>
                            <div class="bswllic-info">
                              <h3 class="bswllic-title"> <a href="#muahang"> Bất chấp đang sốt, Pha Lê thức trắng đêm đợi tạnh mưa để quay MV </a> </h3>
                            </div>
                          </div>
                        </li>
                      </ul>
                    </div>
                    <div class="bsw-right cine fr"> <a href="#muahang" class="bnlwl-title">Phim ảnh</a>
                      <ul class="bswl-list">
                        <li class="bswlli">
                          <div class="bswlli-content clearfix"> <a href="#muahang" class="bswllic-avatar"> <img src="<?php echo DOMAIN;?>blog_tamsu/24726397_848568755312915_791912694_o-niekwez1qfwka6mocrp81idftj89m7xo9ae9jzi6xk.jpg"> </a>
                            <div class="bswllic-info">
                              <h3 class="bswllic-title"> <a href="#muahang"> Đi tìm nguồn cảm hứng đã tạo ra tuyệt phẩm "Coco" </a> </h3>
                            </div>
                          </div>
                        </li>
                      </ul>
                    </div>
                    <div class="box_foot_detail">
                      <div class="left_foot_detail"> <a href="javascript:void(0)" class="bnlwl-title">Có thể bạn quan tâm</a>
                        <ul>
                          <li>
                            <div class="img_left"><a href="#muahang"> <img src="<?php echo DOMAIN;?>blog_tamsu/1210-nieml69ep1vi8y1jkqlo8tkjasw6oht29aexczbseu.jpg"> </a></div>
                            <a href="#muahang">Đẻ con ra sạch tinh, trắng như trứng bóc, mũi cao chuẩn đẹp chỉ cần mẹ bầu ăn cật lực 5 món này suốt thai kì</a> </li>
                          <li>
                            <div class="img_left"><a href="#muahang"> <img src="<?php echo DOMAIN;?>blog_tamsu/dau1-blogtamsuvn1-nicxsx1m70bfpmlugcpt0iq5p0j23hkz2fe7ptvjx2.jpg"> </a></div>
                            <a href="#muahang">Đau dạ dày kinh niên cũng khỏi hẳn sau 3 ngày chỉ với 1 củ riềng, ung thư không bao giờ có cửa tìm tới</a> </li>
                          <li>
                            <div class="img_left"><a href="#muahang"> <img src="<?php echo DOMAIN;?>blog_tamsu/haiblogtamsuvn033-nieo8jqeeb5jm7sqvejaqu6q508uw7iof5x2o7ajdi.jpg"> </a></div>
                            <a href="#muahang">Cứ nấu ăn theo kiểu này, phổi bị TÀN PHÁ nặng nề hơn cả hút thuốc lá, chưa đến 40 đã bị ung thư ác tính, các chị em nên đọc kỹ để đề phòng</a> </li>
                          <li>
                            <div class="img_left"><a href="#muahang"> <img src="<?php echo DOMAIN;?>blog_tamsu/page679-nieox1f0gwoi4g81uzncpnp3cfj5gaqwgduitqz992.jpg"> </a></div>

                            <a href="#muahang">Cô gái 28 năm giữ cái ngàn vàng cuối cùng mắc ung thư âm đạo chỉ vì đã bỏ qua những dấu hiệu ĐOẠT MẠNG mà chị em nào cũng thường gặp</a> </li>
                        </ul>
                      </div>
                      <div class="right_foot_detail"> <a href="javascript:void(0)" class="bnlwl-title">Tin mới cập nhật</a>
                        <ul>
                          <li> <a href="#muahang">Lý do bạn nên sắm ngay chiếc smartphone tràn màn hình thời thượng tại FPT Shop</a>
                            <p><a href="#muahang">Đẹp</a> <span>/&nbsp; 05-12-2017</span></p>
                          </li>
                          <li> <a href="#muahang">Đau dạ dày kinh niên cũng khỏi hẳn sau 3 ngày chỉ với 1 củ riềng, ung thư không bao giờ có cửa tìm tới</a>
                            <p><a href="#muahang">Sức khỏe</a> <span>/&nbsp; 05-12-2017</span></p>
                          </li>
                          <li> <a href="#muahang">Tử vi mới nhất Thứ Tư ngày 6/12/2017 của 12 con giáp cực chuẩn, cực hấp dẫn</a>
                            <p><a href="#muahang">12 Chòm sao</a> <span>/&nbsp; 05-12-2017</span></p>
                          </li>
                          <li> <a href="#muahang">Lâm Tâm Như đụng hàng Phạm Băng Băng vẫn đẹp xuất sắc, tới khi chạm trán Triệu Lệ Dĩnh thì 'tắt điện'</a>
                            <p><a href="#muahang">Thời Trang</a> <span>/&nbsp; 05-12-2017</span></p>
                          </li>
                          <li> <a href="#muahang">Tuyển tập trang phục khoe vòng 1 gợi cảm của Trương Quỳnh Anh</a>
                            <p><a href="#muahang">Thời Trang</a> <span>/&nbsp; 05-12-2017</span></p>
                          </li>
                        </ul>
                      </div>
                    </div>
                    <!-- clip moi-->
                    <div class="list-video-news">
                      <div class="bar_left"> <a href="javascript:void(0)" class="main_folder">clip mới<img class="icondown" src="<?php echo DOMAIN;?>blog_tamsu/icondown.gif"></a> </div>
                      <div class="content-video-news">
                        <div class="first-video-news">
                          <div class="box-video-play"> <a href="#muahang" > <img src="<?php echo DOMAIN;?>blog_tamsu/0.jpg">
                            <div class="button-play-video"></div>
                            </a> </div>
                          <div class="tit-first-video">
                            <p> <a href="#muahang" > Đã tìm ra danh tính chú bộ đội giới thiệu "tour quân sự" gây sốt MXH, không chỉ đẹp trai mà gia thế </a> </p>
                          </div>
                        </div>
                        <div class="first-video-news">
                          <div class="box-video-play"> <a href="#muahang" > <img src="<?php echo DOMAIN;?>blog_tamsu/0(1).jpg">
                            <div class="button-play-video"></div>
                            </a> </div>
                          <div class="tit-first-video">
                            <p> <a href="#muahang" > Hé lộ sự thật trần trụi về nữ thần y mệnh danh 'Công chúa thuốc lào' chữa bách bệnh bằng nắnn bóp </a> </p>
                          </div>
                        </div>
                        <div class="first-video-news">
                          <div class="box-video-play"> <a href="#muahang" > <img src="<?php echo DOMAIN;?>blog_tamsu/0(2).jpg">
                            <div class="button-play-video"></div>
                            </a> </div>
                          <div class="tit-first-video">
                            <p> <a href="#muahang" > 'Chết lặng' khởi tố vì chém trộm, người vợ đau đớn kêu oan: Chồng tôi làm vậy để bảo vệ 6 mạng người </a> </p>
                          </div>
                        </div>
                        <div style="clear:both;"></div>
                        <div class="first-video-news">
                          <div class="box-video-play"> <a href="#muahang" > <img src="<?php echo DOMAIN;?>blog_tamsu/0(3).jpg">
                            <div class="button-play-video"></div>
                            </a> </div>
                          <div class="tit-first-video">
                            <p> <a href="#muahang" > Hướng dẫn cách làm món 4 MÓN THỊT VỊT NGON BẤT NGỜ | Feedy TV </a> </p>
                          </div>
                        </div>
                        <div class="first-video-news">
                          <div class="box-video-play"> <a href="#muahang" > <img src="<?php echo DOMAIN;?>blog_tamsu/0(4).jpg">
                            <div class="button-play-video"></div>
                            </a> </div>
                          <div class="tit-first-video">
                            <p> <a href="#muahang" > [Review] Bánh mì chảo của quán Hạt dẻ cười </a> </p>
                          </div>
                        </div>
                        <div class="first-video-news">
                          <div class="box-video-play"> <a href="#muahang" > <img src="<?php echo DOMAIN;?>blog_tamsu/0(5).jpg">
                            <div class="button-play-video"></div>
                            </a> </div>
                          <div class="tit-first-video">
                            <p> <a href="#muahang" > Tổng hợp các món ăn được yêu thích phần 49 I Feedy TV </a> </p>
                          </div>
                        </div>
                        <div style="clear:both;"></div>
                      </div>
                    </div>
                    <div style="clear:both;"></div>
                    <!--  end box clip moi  --> 
                    <!--  mon ngon moi nngay --> 
                    <!-- end box mon ngon moi ngay     --> 
                  </div>
                </div>
                <!-- box bottom content  --> 
              </div>
              <div class="bnlw-right">
                <div class="bnlwr-news-wrapper newUpdate">
                  <div class="bqhsw-title"> <a href="javascript:void(0)" class="bnlwl-title">Tin mới cập nhật</a> </div>
                  <ul class="bqhsw-list">
                    <li class="bqhswli ">
                      <div class="bqhswli-content ">
                        <h3 class="bqhswli-title"> <a href="#muahang"> Đẻ con ra sạch tinh, trắng như trứng bóc, mũi cao chuẩn đẹp chỉ cần mẹ bầu ăn cật lực 5 món này suốt thai kì </a> </h3>
                        <p class="bthwlici-description"> <a href="#muahang" class="bthwlicid-category">Sức khỏe</a> / <span class="bthwlicid-time">05-12-2017</span> </p>
                      </div>
                    </li>
                    <li class="bqhswli ">
                      <div class="bqhswli-content ">
                        <h3 class="bqhswli-title"> <a href="#muahang"> Lý do bạn nên sắm ngay chiếc smartphone tràn màn hình thời thượng tại FPT Shop </a> </h3>
                        <p class="bthwlici-description"> <a href="#muahang" class="bthwlicid-category">Đẹp</a> / <span class="bthwlicid-time">05-12-2017</span> </p>
                      </div>
                    </li>
                    <li class="bqhswli ">
                      <div class="bqhswli-content ">
                        <h3 class="bqhswli-title"> <a href="#muahang"> Đau dạ dày kinh niên cũng khỏi hẳn sau 3 ngày chỉ với 1 củ riềng, ung thư không bao giờ có cửa tìm tới </a> </h3>
                        <p class="bthwlici-description"> <a href="#muahang" class="bthwlicid-category">Sức khỏe</a> / <span class="bthwlicid-time">05-12-2017</span> </p>
                      </div>
                    </li>
                    <li class="bqhswli ">
                      <div class="bqhswli-content ">
                        <h3 class="bqhswli-title"> <a href="#muahang"> Lâm Tâm Như đụng hàng Phạm Băng Băng vẫn đẹp xuất sắc, tới khi chạm trán Triệu Lệ Dĩnh thì 'tắt điện' </a> </h3>
                        <p class="bthwlici-description"> <a href="#muahang" class="bthwlicid-category">Thời Trang</a> / <span class="bthwlicid-time">05-12-2017</span> </p>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="btswc-body slide-hoptactin">
      <div class="w1000"> </div>
    </section>
    <footer class="btswc-footer">
      <div class="btswcf-info">
        <div class="w1000">
          <div class="btswcf-content">
            <div class="btswcfc-left fl"> <a href="javascript:void(0)" class="btwl-logo-bts"></a> <span class="btwl-gp"> GP TTTĐTH: 5051/GP-TTĐT do Sở TT&amp;TT
              Hà Nội cấp. </span>
              <p class="btwl-des"> Blog Tâm sự chia sẻ tâm sự tình yêu, giới tính, hôn nhân, gia đình của giới trẻ. Nơi mà
                ai
                cũng có thể chia sẻ Tâm sự của mình. </p>
            </div>
            <div class="btswcfc-right">
              <div class="btswcfcr-wrapper">
                <div class="btswcfcrw-category clearfix">
                  <div class="btswcfcrw-left fl">
                    <ul class="blc-category-list">
                      <li class="blccli">
                        <div class="blccli-content clearfix">
                          <ul class="blcclic-list">
                            <li class="blcclicli title"> <a href="#muahang">Sao</a> </li>
                            <li class="blcclicli"> <a href="#muahang">Sao việt</a> </li>
                            <li class="blcclicli"> <a href="#muahang">Sao ngoại</a> </li>
                            <li class="blcclicli"> <a href="#muahang">Nhan sắc</a> </li>
                            <li class="blcclicli"> <a href="#muahang"> Sao &amp; Fan</a> </li>
                          </ul>
                        </div>
                      </li>
                      <li class="blccli">
                        <div class="blccli-content clearfix">
                          <ul class="blcclic-list">
                            <li class="blcclicli title"> <a href="">Giải trí</a> </li>
                            <li class="blcclicli"> <a href="#muahang">Âm nhạc</a> </li>
                            <li class="blcclicli"> <a href="#muahang">Phim Ảnh</a> </li>
                          </ul>
                        </div>
                      </li>
                      <li class="blccli">
                        <div class="blccli-content clearfix">
                          <ul class="blcclic-list">
                            <li class="blcclicli title"> <a href="#muahang">TV Show</a> </li>
                            <li class="blcclicli"> <a href="#muahang">Trong nước</a> </li>
                            <li class="blcclicli"> <a href="#muahang">Nước ngoài</a> </li>
                          </ul>
                        </div>
                      </li>
                    </ul>
                  </div>
                  <div class="btswcfcrw-right fr">
                    <ul class="blc-category-list">
                      <li class="blccli">
                        <div class="blccli-content clearfix">
                          <ul class="blcclic-list">
                            <li class="blcclicli title"> <a href="#muahang">Đẹp</a> </li>
                            <li class="blcclicli"> <a href="#muahang">Thời Trang</a> </li>
                            <li class="blcclicli"> <a href="#muahang">Đẹp như Sao</a> </li>
                            <li class="blcclicli"> <a href="#muahang">Tips</a> </li>
                          </ul>
                        </div>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <div class="btswc-footer_boot">
              <ul class="btwl-list">
              </ul>
            </div>
            <div class="btswc-footer_boot" style="font-size: 12px;line-height: 15px;">
              <p>- Cơ quan chủ quản: Công ty Blog tam su;</p>
              <p>- Địa chỉ: Tầng 5, toà nhà Ngôi Sao, phố Dương Đình Nghệ, KĐT mới Cầu Giấy, phường Yên Hoà, quận Cầu Giấy, thành phố Hà Nội;</p>
              <p>- Người chịu trách nhiệm nội dung: Nguyen Van Nam.</p>
            </div>
          </div>
        </div>
      </div>
      <div class="btswcf-copyright">
        <div class="w1000">
          <div class="bcr-content clearfix">
            <p class="bcrc-text fl"> © Copyright 2014 <a href="#muahang">Blogtamsu</a>, All rights reserved </p>
            <a href="javascript:void(0)" class="bcrc-ads">Liên hệ quảng cáo</a> </div>
        </div>
      </div>
    </footer>
  </div>
</div>


</body>
</html>