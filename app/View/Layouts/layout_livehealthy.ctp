<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8" />
	<meta http-equiv="content-type" content="text/html" />
	<meta name="author" content="GallerySoft.info" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<?php
        if (isset($keywords_for_layout)) {
            echo "<meta name='keywords' content='" . $keywords_for_layout . "' /> ";
        }
        if (isset($description_for_layout)) {
            echo "<meta name='description' content='" . $description_for_layout . "' />";
        }
        ?>
        <title>
            <?php
            if (isset($title_for_layout)) {
                echo $title_for_layout;
            }
            $domain = DOMAIN;
            ?>
        </title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!-- Latest compiled and minified CSS --> 
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css"/>
    <!-- Optional theme -->
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css"/>
    <!-- Latest compiled and minified JavaScript -->
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
    <link href="<?php echo DOMAIN ?>/blog_livehealthy/css/style.css" rel="stylesheet"/>
    <?php 	
	$code_home = $this->Session->read('code_home');
	
	if(isset($code_home)){
	?>
	<!-- Facebook Pixel Code -->
	<script>
	  !function(f,b,e,v,n,t,s)
	  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
	  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
	  n.queue=[];t=b.createElement(e);t.async=!0;
	  t.src=v;s=b.getElementsByTagName(e)[0];
	  s.parentNode.insertBefore(t,s)}(window, document,'script',
	  'https://connect.facebook.net/en_US/fbevents.js');
	  fbq('init', '<?php echo $code_home;?>');
	  fbq('track', 'PageView');
	</script>
	<noscript><img height="1" width="1" style="display:none"
	  src="https://www.facebook.com/tr?id=<?php echo $code_home;?>&ev=PageView&noscript=1"
	/></noscript>
	<!-- End Facebook Pixel Code -->
	
<?php } ?>
</head>

<body>
<div id="wrapper">
    <div id="menu">
        <div class="container">
            <ul>
                <li><a href="#dathang-form"><img src="<?php echo DOMAIN ?>/blog_livehealthy/images/mobile-logo.png" /></a></li>
                <li><a href="#dathang-form">Tình yêu</a></li>
                <li><a href="#dathang-form">Người nổi tiếng</a></li>
                <li><a href="#dathang-form">Sắc đẹp</a></li>
                <li><a href="#dathang-form">Ý tưởng quà tặng</a></li>
            </ul>
        </div>
    </div>
    <div class="xoa"></div>
    
    
    <div id="content">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-8 col-sm-12 content">
                    <div class="article">
                        <div class="detail-article">
                            <div class="main-content">
                                <!-- content -->
                                <?php echo $content_for_layout; ?>
                            </div>
                            <div class="xoa"></div>
                            <p style="background: #ccc; font-size: 12px; margin: 15px 0; padding: 15px; text-align: center;">
                                <span>2018</span>
                                 Bản quyền. Bản quyền đã được bảo hộ. 
                            </p>
                        </div>
                    </div>
                </div>
                
                <div class="col-lg-4 col-md-4 col-sm-12 slide-wing">
                    <div class="article-wing">
                        <div class="wing-title">
                            <h3>Đọc nhiều nhất</h3>
                        </div>
                    </div>
                    <div class="xoa"></div>
                    <div class="litle-new">
                        <div class="litle-img">
                            <a href="#"  >
                                <img src="<?php echo DOMAIN ?>/blog_livehealthy/images/collage_lacelesssneakers-450x270.jpg" />
                            </a>
                        </div>
                        <div class="litle-title">
                            <a href="#"  >
                            Đi sneakers không dây buộc mới là sành điệu?</a>
                        </div>
                        <div style="clear: both;"></div>
                    </div>
                    <div class="litle-new">
                        <div class="litle-img">
                            <a href="#"  >
                                <img src="<?php echo DOMAIN ?>/blog_livehealthy/images/Mỹ-Phẩm-Nhật-450x270.jpg" />
                            </a>
                        </div>
                        <div class="litle-title">
                            <a href="#"  >
                            15 món mỹ phẩm nâng tầm Nhật Bản trên “đấu trường” làm đẹp
                            </a>
                        </div>
                        <div style="clear: both;"></div>
                    </div>
                    <div class="litle-new">
                        <div class="litle-img">
                            <a href="#"  >
                                <img src="<?php echo DOMAIN ?>/blog_livehealthy/images/minh-hang.jpg" />
                            </a>
                        </div>
                        <div class="litle-title">
                            <a href="#"  >
                            Kiểm tra độ ẩm của da bằng 3 mẹo đơn giản tại nhà
                            </a>
                        </div>
                        <div style="clear: both;"></div>
                    </div>
                    <div class="litle-new">
                        <div class="litle-img">
                            <a href="#"  >
                                <img src="<?php echo DOMAIN ?>/blog_livehealthy/images/tui-kinh-1.jpg" />
                            </a>
                        </div>
                        <div class="litle-title">
                            <a href="#"  >
                            Đặc quyền của phái đẹp
                            </a>
                        </div>
                        <div style="clear: both;"></div>
                    </div>
                    
                    </div>
                <div class="xoa"></div>
                </div>
            </div>
        </div>
</div> 
</body>
</html>