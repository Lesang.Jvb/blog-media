
<link href="<?php echo DOMAIN ?>css/button.css" rel="stylesheet">
<div class="content-detail">
<p id="author-info">Đăng ngày <span class=""><?php echo date('d-m-Y', strtotime($detailNews['News']['modified'])); ?></span> </p>
<h3><?php echo $detailNews['News']['titledetail'];?></h3>
<?php
 echo $detailNews['News']['content'];
 //pr($detailNews);die; 
?>

<p id="dathang-form" style="font-family: time new roman; text-align: center; font-size: 24px; color: #009221; margin-top: 12px;">ĐĂNG KÝ TƯ VẤN ĐẶT HÀNG</p>


<br>

<div class="row-muahang">
	<div class="image-product">
		   
<center>
	  <img src="<?php echo IMAGEAD; ?>/news/<?php echo $detailNews['News']['images']; ?>" class="img-responsive" style="max-width: 75%;">
</center>
		<p style="color: #4268ff;text-align: center;font-size: 25px; margin-top: 5px;">
			<?php echo number_format(($detailNews['News']['price']),0,'','.') ." "?>/ <?php echo $detailNews['News']['currency_unit']; ?>
		</p>
	</div>
	<div class="form-submit" style="margin-top: 25px;">
      <a id="muahang"></a>
		<form method="POST" action="<?php echo DOMAIN ?>home/order" onsubmit="return submitForm()">
			<input type="text" class="form-control input-border" id="name" name="name" placeholder="Họ tên" required="">
            <div class="js_errorMessage_name" style="color: #F00;font-family: Arial;font-size: 14px; display:none"></div>
			<input type="tel" class="form-control" id="phone" name="phone" placeholder="Số điện thoại" required="">
            <div class="js_errorMessage_phone" style="color: #F00;font-family: Arial;font-size: 14px; display:none"></div>
            <input type="hidden" name="price" value="<?php echo $detailNews['News']['price']; ?>">
			<textarea class="form-control input-border" id="address" name="address" placeholder="Địa chỉ" required></textarea>
            <div class="js_errorMessage_address" style="color: #F00;font-family: Arial;font-size: 14px; display:none"></div>
			<input type="hidden" class="form-control input-border" id="product_name" name="product_name"  value="<?php echo $detailNews['News']['name']; ?>">
			<input type="hidden" class="form-control input-border" id="product_id" name="product_id"  value="<?php echo $detailNews['News']['id']; ?>">
			
			<?php if(isset($_GET['aff_sub1'])) { ?>
				<input type="hidden" name="aff_sub1" value="<?php echo $_GET['aff_sub1']; ?>">
			<?php } ?>
			<div class="text-center">
				<div class="text-center"><button class="button iframe_button" onclick="submitForm()" style="width: 85%" type="submit" name="save">ĐĂNG KÝ</button></div>
			</div>
		</form>
	</div>
</div>
</div>
<script>
			$(".iframe_button").click(function(){
				var name = $("#name").val();
				var phone = $("#phone").val();
				var address = $("#address").val();
				
				if(name.length  == 0){
					$(".js_errorMessage_name").show();
					$(".js_errorMessage_name").html('Bạn vui lòng nhập tên.');
					$("#name").focus(); return false;
				}else{
					$(".js_errorMessage_name").hide();
				}
				if(address.length  == 0){
					$(".js_errorMessage_address").show();
					$(".js_errorMessage_address").html('Bạn vui lòng nhập địa chỉ.');
					$("#address").focus(); return false;
				}else{
					$(".js_errorMessage_address").hide();
				}
				if(phone.length  == 0 || !phone.match(/^(84|0)(1\d{9}|9\d{8}|8\d{8})$/)){
					$(".js_errorMessage_phone").show();
					$(".js_errorMessage_phone").html('Bạn vui lòng nhập đúng định dạng số điện thoại.');
					$("#phone").focus(); return false;
				}else{
					$(".js_errorMessage_phone").hide();
				}
				
			});
			
			
		</script>
<div class="box-comment">
        <div id="comment-title">Bình luận</div>
         <?php  
			$listcomment= $this->requestAction('comment/listcomment/'.$detailNews['News']['id']);
			foreach($listcomment as $row){
		 ?> 
           <div class="comment">
				<img align="left" alt="" class="ava" height="70" width="70" src="<?php echo IMAGEAD ?>comments/<?php echo $row['Comment']['images'] ?>">
				<span class="comment__name"><span style="color:#990000" class="name"><?php echo $row['Comment']['name'] ?></span><br>
							<span style="color:black"><?php echo $row['Comment']['content'] ?></span>
				<p class="comment__info">
					 <?php echo $row['Comment']['created'] ?> |
					<span class="blue-text">Trả lời</span>

				</p>
				</span>
				
				 <?php  
					$listcommentchild= $this->requestAction('comment/listcomment_child/'.$row['Comment']['id']);
					foreach($listcommentchild as $row2){
				 ?> 
				    <div class="comment">
					<img align="left" alt="" class="ava" height="70" width="70" src="<?php echo IMAGEAD ?>comments/<?php echo $row2['Comment']['images'] ?>">
					<span class="comment__name"><span style="color:#990000" class="name"><?php echo $row2['Comment']['name'] ?></span><br>
								<span style="color:black"><?php echo $row2['Comment']['content'] ?></span>
					<p class="comment__info">
						<?php if($row2['Comment']['time'] != null) { ?>
							<?php echo $row2['Comment']['time']; ?> phút trước
						<?php }else{ ?>
						` 	10 phút trước
						<?php } ?>
					</p>
					</span>
					</div>
					<?php } ?>
				
			</div>
              
            
            <?php } ?>
        </div>