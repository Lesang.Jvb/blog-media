
            <?php
    $css = array(   
        '/css/bootstrap.min.css',
        '/css/bootstrap.css',
       '/css/style.css',
    );
    $js = array(
        '/js/jquery.min.js',  
        '/js/bootstrap.min.js',
        '/js/jquery.validate.js',

        
       
    );
    echo $this->Html->css($css);
    echo $this->Html->script($js);
?>

 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> 
  <link rel="stylesheet" href="<?php echo DOMAIN?>css/style.css">
    <script type="text/javascript">
        function scrollTo() {
            $('html, body').animate({ scrollTop: $('#div_id').offset().top }, 'slow');
            return false;
        }
    </script>
<!-- lên đầu trang -->
<style type="text/css">
    .fix-icon-right{
        position: fixed;
        right: 10px;
        bottom: 35px;
        display: none;
        z-index: 9999999;
    }
    .fix-icon-right img{
        margin-bottom: 5px;
    }
    ul.unstyled
     {
		margin-left: 0;
		list-style: none;
	}
	.form-group {
		margin-bottom: 0px !important;
	}
	.box_rs1{
		margin: 0 auto;
		margin-top: 50px;
		float: inherit;
		border: 1px #ececec solid;
		padding: 20px;
		border-radius: 3px;
	}
	.box_rs h2{
		margin-top: 0px;
		margin-bottom: 15px;
		font-weight: bold;
		text-transform: uppercase;
		font-size: 20px;		
	}
</style>

<div class="fix-icon-right" id="fixedmenu">
    <div class="row-fluid">
        <ul class="unstyled">
            <li>
                <a href="" title="Top"><img src="<?php echo DOMAIN?>images/gotop.gif" class="scrollup" alt="Top" title="Top"/></a>
            </li>
        </ul>
    </div>
</div>
<script type="text/javascript">
    $(document).scroll(function(){
        var curPos = $(document).scrollTop();
        if(curPos >= 350) {
            $('#fixedmenu').show(10);
        } else {
            $('#fixedmenu').hide(10);
        }
    });
    $('.scrollup').click(function(){
        $("html, body").animate({ scrollTop: 0 }, 600);
        return false;
    });
</script> 	
<?php 
	$gia = 499000;
	$price = ((100 - $giamgia )/100)*$gia;
?>

<div class="container ">
	<div class="row">
		<div class="col-md-6 box_rs1">
		<div class="box_rs">
		
		
<form class="form-horizontal"  id="dathang-form" method="POST" action="<?php echo DOMAIN?>contact/index" onsubmit="return Payment()">

	<div class="form-group">
		<label for="inputEmail3" class="col-sm-4 control-label"></label>
		<div class="col-sm-8">
			<h2>Xác nhận đơn hàng</h2>
		</div>
	  </div>

	  <div class="form-group">
		<label for="inputEmail3" class="col-sm-4 control-label">Name</label>
		<div class="col-sm-8">
			<input value="<?php echo $name ?>" id="name" name="name" type="text" class="form-control" aria-describedby="sizing-addon1" readonly>
		</div>
	  </div>
	    <div class="form-group">
		<label for="inputEmail3" class="col-sm-4 control-label">Email</label>
		<div class="col-sm-8">
			<input placeholder="Email:" value="<?php echo $email ?>" id="email" name="email" type="email" class="form-control" aria-describedby="sizing-addon1"   readonly>
		</div>
	  </div>
	  <div class="form-group">
		<label for="inputEmail3" class="col-sm-4 control-label">Phone</label>
		<div class="col-sm-8">
			<input  value="<?php echo $mobile ?>" id="phone" name="mobile" type="tel" class="form-control" readonly
		aria-describedby="sizing-addon1" onkeypress="return forceNumber(event);" maxlength="11"> 
		</div>
	  </div>
	  
	   <div class="form-group">
		<label for="inputEmail3" class="col-sm-4 control-label">Address</label>
		<div class="col-sm-8">
			<input  value="<?php echo $address ?>" id="address" name="address" type="text" class="form-control" aria-describedby="sizing-addon1" readonly>
		</div>
	  </div>
	  
	    <div class="form-group">
		<label for="inputEmail3" class="col-sm-4 control-label">Số lượng</label>
		<div class="col-sm-8">
			<input  value="<?php echo $soluong ?>" id="soluong" name="soluong" type="number" class="form-control" aria-describedby="sizing-addon1" readonly>
		</div>
	  </div>
	 

	  
	     <div class="form-group">
		<label for="inputEmail3" class="col-sm-4 control-label">Thông tin</label>
		<div class="col-sm-8">
		 <input value="Thông tin được gửi từ Slimtosen" id="title" name="title" type="text" class="form-control"  readonly >
		</div>
	  </div>
	     <div class="form-group">
		<label for="inputEmail3" class="col-sm-4 control-label">Mã giám giá</label>
		<div class="col-sm-8">
			<input  value="<?php echo $code ?>" id="code" name="code" type="text" class="form-control" readonly>
		</div>
	  </div>
	  
	   <div class="form-group">
		<label for="inputEmail3" class="col-sm-4 control-label">Giá sản phẩm (Đã trừ % giảm giá )</label>
		<div class="col-sm-8">
			<input id="price" name="price" type="text" value="<?php echo $price ?>" class="form-control" readonly >
		</div>
	  </div>
	  
	  <div class="form-group">
		<label for="inputEmail3" class="col-sm-4 control-label"></label>
		<div class="col-sm-8">
			<button type="submit" class="btn btn-primary">Gửi thông tin</button> 
		</div>
	  </div>  
</form>
</div>
</div></div>
</div>