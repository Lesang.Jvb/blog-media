<?php
 App::import('Vendor', 'ckeditor');
App::import('Vendor', 'ckfinder');
class LoginController extends AppController {
    public $name = 'Login';
    public $uses = array('Usermember', 'City', 'Setting', 'Product','News','Catproduct','Comment','Slideshow','Email');
    //public $components = array('Email'); 
	
	   public function beforeFilter() {
        parent::beforeFilter();
       // $this->layout = 'quantri';
		if(!$this->Session->check('user_id')){
				echo "<script>alert('".json_encode('Bạn phải đăng nhập')."');</script>";
				echo "<script>location.href='".DOMAIN."'</script>";
		}
       
    }
	
    public function index() {
        
    }
	
		  //close quang cao
    function close_nangcap($id = null) {
        $this->Usermember->id = $id;
		$user=$this->Usermember->findById($id);
		if($user['Usermember']['role_old'] !=null)
		$user['Usermember']['role']=$user['Usermember']['role_old'];
		
        $this->Usermember->save($user);
        $this->redirect($this->referer());
    }
 
	public function quantri() {	
        $this->layout = 'quantri';
        $user = $this->Session->read('user');
		  if($user['Usermember']['role']==4){
	   echo "<script>location.href='" . DOMAIN . "lich-su-mua-hang.html'</script>";die;
	   }
		
       if($user['Usermember']['role']>2){
	   echo "<script>location.href='" . DOMAIN . "danh-sach-tin.html'</script>";die;
	   }
	    
	   
	   
	   if($user['Usermember']['role']==1) {
        $khuvuc = $this->City->find("first", array(
            'conditions' => array(
                "City.id" => $user["Usermember"]["city_id"]
            )
                ));

        $this->set('title_product', 'Danh sách đơn vị cấp 1 thuộc khu vực <span>' . $khuvuc["City"]["name"] . "</span>");
		}
		elseif($user['Usermember']['role']==2){
		$this->set('title_product', 'Danh sách đơn vị con <span>');
		}
      
            $this->paginate = array(
                'fields' => array(
                    'Usermember.email',
                    'Usermember.id',
                    'Usermember.company',
                    'Usermember.avata',
                    'Usermember.name',
                    'Usermember.status',
                    'Usermember.phone',
					"Usermember.role",
					"Usermember.role_old",
                ),
                "conditions" => array(
                
                    "Usermember.parent_id" => $user["Usermember"]["id"],
                    "Usermember.role <" => 3
                ),
                "limit" => 10,
            );
			
            $listQuanTriCap1 = $this->paginate('Usermember');					
            $this->set('listQuanTriCap1', $listQuanTriCap1);
        
        $this->set('title_for_layout', 'Quản trị');
		$this->set('tin',0);
		
		}
	
	public function listusermember() {
        $this->layout = 'quantri';
        $this->set("title_product", "Danh sách các thành viên con");
		$this->set('tin',1);
        $user = $this->Session->read('user');
        // $listUsermember = $this->Usermember->find("all", array(
            // "fields" => array(
                // "Usermember.name",
                // "Usermember.id",
                // "Usermember.email",
                // "Usermember.company",
            // ),
            // "conditions" => array(
                // "Usermember.city_id" => $user["Usermember"]["city_id"],
                // "Usermember.role" => 2,
            // )
                // ));
				
        //lấy danh sách id của các đơn vị
        // foreach ($listUsermember as $listUsermembers) {
            // $listIdUsermember[$listUsermembers["Usermember"]["id"]] = $listUsermembers["Usermember"]["id"];
        // }
        $this->paginate = array(
            "fields" => array(
                "Usermember.name",
                "Usermember.avata",
                "Usermember.phone",
                "Usermember.id",
                "Usermember.email",
                "Usermember.status",
                "Usermember.role",
                "Usermember.role_old",
            ),
            "conditions" => array(
                "Usermember.parent_id" => $user['Usermember']['id'],
				  "Usermember.role" => 3,
            )
        );
        $listThanhVien = $this->paginate('Usermember');
        $this->set("listThanhVien", $listThanhVien);         
    }
	
//Created by ONG_TRUM =))
	public function dsgianhang() {
        $this->layout = 'quantri';
        $this->set("title_product", "Danh sách các gian hàng thuộc khu vực");
		$this->set('tin',2);
        $user = $this->Session->read('user');
		if($user['Usermember']['role']!=1) {
		  echo "<script>alert('" . json_encode('Chỉ có quản trị khu vực mới được truy cập địa chỉ này') . "');</script>";
            echo "<script>location.href='" . DOMAIN . "'</script>";
		}
	   
        $this->paginate = array(
            "fields" => array(
                "Usermember.name",
                "Usermember.avata",
                "Usermember.phone",
                "Usermember.id",
                "Usermember.email",
                "Usermember.status",
                "Usermember.gianhang",
				"Usermember.role",
				"Usermember.role_old",
            ),
            "conditions" => array(
                "Usermember.city_id" => $user['Usermember']['city_id'],
                "Usermember.role" => 5,
            )
        );
        $listThanhVien = $this->paginate('Usermember');
        $this->set("listThanhVien", $listThanhVien);         
    }
	
	
	
    public function viewdetail($id) {
        $view = $this->Usermember->findById($id);
        $this->set("view", $view);
        $this->set("title_product", "Thông tin chi tiết đơn vị cấp 1");
        $this->layout = 'quantri';
    }

    public function active_usermember($id) {
        $Usermember["id"] = $id;
        $Usermember["status"] = 1;
        $this->Usermember->save($Usermember);
        $this->redirect($this->referer());
    }

    public function close_usermember($id) {
        $Usermember["id"] = $id;
        $Usermember["status"] = 0;
        $this->Usermember->save($Usermember);
       $this->redirect($this->referer());
    }

    public function delete_usermember($id) {
        $data["id"] = $id;
        $this->Usermember->delete($data);
       $this->redirect($this->referer());
    }
	
	
	
	//Thông tin tài khoản
	public function thongtincanhan($id=null){
		$this->layout='quantri';
		$this->set('tin',100);
		$user=$this->Session->read('user');
		
				
		 $this->data = $this->Usermember->read(null, $user['Usermember']['id']);
           //pr($user); die;
		   $edit=$this->Usermember->findById($user['Usermember']['id']);
        $this->set('edit', $edit);
		if($id!=null) { $this->set('id', $id);
		
		
			//lấy ra danh sách các tỉnh đã được đăng ký
		
		$list_usermember = $this->Usermember->find('list',array(
		'fields'=>array(			
			'Usermember.city_id'
		),
			'conditions'=>array(
				'Usermember.status'=>1,
				'Usermember.role'=>1,				
				),			
			));	
		$list_usermember['0']=0;
			
		//lấy ra danh sách các tỉnh
        $city = $this->City->find('list', array(
            'conditions' => array(
                'City.parent_id' => null,		
				'City.id not' => $list_usermember,		
				
            ),
            'order' => 'City.pos'
                )
        );
		
        $this->set('city', $city);
		}
	
	}
	
	
	
	//Thay đổi mật khẩu
	
	public function doimatkhau()
	{
	//	$this->layout='quantri';
		$this->set('tin',11);
		if(!empty($_POST['password'])&& (!empty($_POST['security']))){
				
				if($this->Session->read('security_code')!=$_POST['security']){
					echo "<script>alert('".json_encode('Ban nhập không đúng mã bảo vệ !')."');</script>";
					echo "<script>history.back(-1);</script>";
				}
				
				$user_id=$this->Session->read('user_id');
				
				$this->Usermember->id=$user_id;
				$this->Usermember->saveField('password',md5($_POST['password']));
				
				echo "<script>alert('".json_encode('Đổi mật khẩu thành công')."');</script>";
				echo "<script>location.href='".DOMAIN."doi-mat-khau.html'</script>";
				
				}
				
			$setting=$this->Setting->find('first');
			$this->set('title_for_layout', $setting['Setting']['title'].' - Đổi mật khẩu');	
	
	}	
	
	
	
	
	function get_name_city($id=null){
		$city=$this->City->findById($id);
		return $city['City']['name'];
	
	}

// TIN TUC CREADTED BY ONG_TRUM
	
	function dstin(){
	
		$user=$this->Session->read('user');
		
		// if($user['Usermember']['role']==1 || $user['Usermember']['role']==4) {
		// echo "<script>alert('".json_encode('Khu vực và thành viên thường không được truy cập địa chỉ này')."');</script>";
				// echo "<script>location.href='".DOMAIN."quan-tri.html'</script>";
		
		// }
		
		$this->paginate=array(
			'conditions'=>array(
				'News.user_id'=>$user['Usermember']['id'],
			),
			'order'=>'News.pos',
			'limit'=>6,
			);
			
			$this->set('news',$this->paginate('News',array()));
			$this->set('tin',3);
			
			$urlTmp = DOMAIN . $this->request->url;
        $urlTmp = explode(":", $urlTmp);
        if (isset($urlTmp[2])) {
            $startPage = ($urlTmp[2] - 1) * 10 + 1;
        } else {
            $startPage = 1;
        }
        $this->set('startPage', $startPage);
			
		$this->set('title_new','Danh sách tin quản trị');
	}
	
	
	function dstinthanhvien(){
	
		$user=$this->Session->read('user');
		
		
		$user_sub=$this->Usermember->find('list',array(
			'fields'=>array(
				'Usermember.id'
			),
			'conditions'=>array(
				'Usermember.parent_id'=>$user['Usermember']['id'],
				'Usermember.role'=>3,
			),
			
		
		));
		
		$this->paginate=array(
			'conditions'=>array(
				'News.user_id'=>$user_sub,
			),
			'order'=>'News.pos',
			'limit'=>6,
			);
			
			$this->set('news',$this->paginate('News',array()));
			$this->set('tin',4);
			
			$urlTmp = DOMAIN . $this->request->url;
        $urlTmp = explode(":", $urlTmp);
        if (isset($urlTmp[2])) {
            $startPage = ($urlTmp[2] - 1) * 10 + 1;
        } else {
            $startPage = 1;
        }
        $this->set('startPage', $startPage);
			
	$this->set('title_new','Danh sách tin của thành viên');
	$this->render('dstin');
	}
	
	
	 public function active_new($id=null) {
       $this->News->id=$id;
	   $this->News->saveField('status',1);
        $this->redirect($this->referer());
    }

    public function close_new($id=null) {
      $this->News->id=$id;
	   $this->News->saveField('status',0);
       $this->redirect($this->referer());
    }

    public function delete_new($id=null) {
		$new=$this->News->findById($id);
		$this->delete_url($new['News']['images'],$new['News']['user_id']);
		$this->News->delete($id);
		$this->redirect($this->referer());
    }
	
	
	
 public function active_product1($id=null) {
 $checkLogin = $this->Session->read('user');
		
		$checkLogin = $this->Session->read('user');			
		if($checkLogin['Usermember']['role']!=5) {
				echo "<script>alert('Chỉ gian hàng mới được truy cập địa chỉ này !');</script>";
				echo "<script>location.href='".DOMAIN."quan-tri.html'</script>";
		
		}
       $this->Product->id=$id;
	   $this->Product->saveField('user_status',1);
        $this->redirect($this->referer());
    }

    public function close_product1($id=null) {
	$checkLogin = $this->Session->read('user');
		
		$checkLogin = $this->Session->read('user');			
		if($checkLogin['Usermember']['role']!=5) {
				echo "<script>alert('Chỉ gian hàng mới được truy cập địa chỉ này !');</script>";
				echo "<script>location.href='".DOMAIN."quan-tri.html'</script>";
		
		}
      $this->Product->id=$id;
	   $this->Product->saveField('user_status',0);
       $this->redirect($this->referer());
    }
	
	
		 public function active_product($id=null) {
		 $checkLogin = $this->Session->read('user');
		
		$checkLogin = $this->Session->read('user');			
		if($checkLogin['Usermember']['role']!=1) {
				echo "<script>alert('Chỉ khu vực mới được truy cập địa chỉ này !');</script>";
				echo "<script>location.href='".DOMAIN."quan-tri.html'</script>";
		
		}
       $this->Product->id=$id;
	   $this->Product->saveField('status',1);
        $this->redirect($this->referer());
    }

    public function close_product($id=null) {
	$checkLogin = $this->Session->read('user');
		
		$checkLogin = $this->Session->read('user');			
		if($checkLogin['Usermember']['role']!=1) {
				echo "<script>alert('Chỉ khu vực mới được truy cập địa chỉ này !');</script>";
				echo "<script>location.href='".DOMAIN."quan-tri.html'</script>";
		
		}
      $this->Product->id=$id;
	   $this->Product->saveField('status',0);
       $this->redirect($this->referer());
    }

    public function delete_product($id=null) {
	$checkLogin = $this->Session->read('user');
		
		$checkLogin = $this->Session->read('user');			
		if($checkLogin['Usermember']['role']!=1 && $checkLogin['Usermember']['role']!=5) {
				echo "<script>alert('Chỉ khu vực mới được truy cập địa chỉ này !');</script>";
				echo "<script>location.href='".DOMAIN."quan-tri.html'</script>";
		
		}
		$new=$this->Product->findById($id);
		$this->delete_url($new['Product']['images'],$new['Product']['user_id']);
		$this->Product->delete($id);
		$this->redirect($this->referer());
    }
	
	
	 public function active_sptb($id=null) {
	 
		$checkLogin = $this->Session->read('user');
		$checkLogin = $this->Session->read('user');			
		if($checkLogin['Usermember']['role']!=2) {
				echo "<script>alert('Chỉ khu vực mới được truy cập địa chỉ này !');</script>";
				echo "<script>location.href='".DOMAIN."quan-tri.html'</script>";
		
		}
       $this->Product->id=$id;
	   $this->Product->saveField('tieubieu',1);
        $this->redirect($this->referer());
    }

    public function close_sptb($id=null) {
	$checkLogin = $this->Session->read('user');
		
		$checkLogin = $this->Session->read('user');			
		if($checkLogin['Usermember']['role']!=2) {
				echo "<script>alert('Chỉ khu vực mới được truy cập địa chỉ này !');</script>";
				echo "<script>location.href='".DOMAIN."quan-tri.html'</script>";
		
		}
      $this->Product->id=$id;
	   $this->Product->saveField('tieubieu',0);
       $this->redirect($this->referer());
    }
	
		//Them bai viet
	public function dangtin($id=null) {
	//$this->check_account();
	 $this->loadModel("News.News");
		if (!empty($this->data)) {
			
			
			if($this->Session->read('security_code')!=$_POST['security']){
					echo "<script>alert('".json_encode('Bạn nhập không đúng mã bảo vệ !')."');</script>";
					echo "<script>history.back(-1);</script>"; 
				}
		
			
			$this->News->create();
			$data['News'] = $this->data['News'];
			//pr($data['Product']); die;
			$user = $this->Session->read('user');
		
			
			$data['News']['user_id']=$user['Usermember']['id'];
			
		
			
			$data['News']['name']=$_POST['name'];
		
			$data['News']['images']=isset($_POST['userfile'])?$_POST['userfile']:'';
			
			//Xoa anh cu
			if($data['News']['images']!=''){
			
			$new=$this->News->findById($id);
			
			if($data['News']['images']!=$new['News']['images'] && $id!=null){
			
			if(file_exists(SITE_DIR.'admin/webroot/'.$new['News']['images']))
			{
			
				$size=filesize(SITE_DIR.'admin/webroot/'.$new['News']['images']);
				$user["Usermember"]["capacity"]=$user["Usermember"]["capacity"]-$size;
				
				$this->Usermember->save($user);
			
			
			@unlink($new['News']['images']);
			}
			
			}
			
			}
			//End xoa anh cu
			
			$data['News']['city_id']=$user['Usermember']['city_id'];
		
		
			
			$data['News']['content']=$data['News']['content'];
			$data['News']['shortdes']=$data['News']['shortdes'];
			
			//pr($data['Product']);die;
			if ($this->News->save($data['News'])) {
			
			//Tinh dung luong anh
		
				
			if(file_exists(SITE_DIR.'admin/webroot/'.$data['News']['images']))
			{
				$size=filesize(SITE_DIR.'admin/webroot/'.$data['News']['images']);
				$user["Usermember"]["capacity"]=$user["Usermember"]["capacity"]+$size;
				
				$this->Usermember->save($user);
			}	
			//End tin dung luong anh
			
				echo "<script>alert('Cập nhật thành công !');</script>";
				echo "<script>location.href='".DOMAIN."danh-sach-tin.html'</script>";
			} else {
				$this->Session->setFlash(__('Thêm mới danh mục thất bại. Vui lòng thử lại', true));
			}
		}
		else {
		$this->data = $this->News->read(null, $id);
			
		$this->set('edit',$this->News->findById($id));
		}
		
		
	}
	
	
	 function new_changepos() {
        $vitri = $_REQUEST['order'];
           
        // Update order
        foreach ($vitri as $k => $v) {
			if($v == "") {$v = null;}
            $this->News->updateAll(
                array(
                'News.pos' => $v,
               
                ), array(
                'News.id' => $k)
            );
        }
       $this->redirect($this->referer());
    }
	
	
	 function new_process() {
        $process = $_REQUEST['process'];
        $chon = $_REQUEST['chon'];
		if (count($chon) == 0 || $process < 1) {
			$this->redirect($this->referer());     
        }
        switch ($process) {
            case '1' :
                // Update active
                foreach ($chon as $k => $v) {
                    $this->News->updateAll(
                        array(
                        'News.status' => 1
                        ), array(
                        'News.id' => $k)
                    );
                }
                break;

            case '2' :
                // Update deactive
                foreach ($chon as $k => $v) {
                    $this->News->updateAll(
                        array(
                        'News.status' => 0
                        ), array(
                        'News.id' => $k)
                    );
                }
                break;

            case '3' :
                // delete many rows
                $groupId = "";
                foreach ($chon as $k => $v) {
                    $groupId .= "," . $k;
					
					$new=$this->News->findById($k);
					$this->delete_url($new['News']['images'],$new['News']['user_id']);
                }
                $groupId = substr($groupId, 1);
                $conditions = array(
                    'News.id IN (' . $groupId . ')'
                );
                $this->News->deleteAll($conditions);
                break;
        }
        
            $this->redirect($this->referer());
        
    }
	
	
	public function delete_url($url=null,$user_id=null){
	
	$link=SITE_DIR.'admin/webroot/'.$url;
	if($url!=null && $url!='' && file_exists($link)){
	
	$user=$this->Usermember->findById($user_id);

				$size=filesize($link);
				$user["Usermember"]["capacity"]=$user["Usermember"]["capacity"]-$size;
				
				$this->Usermember->save($user);

	
	@unlink($link);
	}
	}
	
	// danh sách sản phẩm
	public function listproduct(){
		$this->set("title_new","Danh sách sản phẩm");
		$checkLogin = $this->Session->read('user');
		
		$checkLogin = $this->Session->read('user');	
		
		if($checkLogin['Usermember']['role']!=1) {
				echo "<script>alert('Chỉ khu vực mới được truy cập địa chỉ này !');</script>";
				echo "<script>location.href='".DOMAIN."quan-tri.html'</script>";
		
		}
		
		$user=$this->Usermember->find('list',array('fields'=>array('Usermember.id'),'conditions'=>array('Usermember.status'=>1,'Usermember.city_id'=>$checkLogin['Usermember']['city_id'],'Usermember.role'=>5)));
		
		
		//pr($user); die;
		$this->paginate = array(
			"fields"=>array(
				"Product.id",
				"Product.images",
				"Product.status",
				"Product.name", 
				"Product.created",				
				"Product.user_id",				
				"Product.tieubieu",				
				"Product.pos",				
			),  
			'conditions'=>array(
				"Product.user_id"=>$user,			
			)			
		);
        $product = $this->paginate('Product');
        $this->set('product', $product);
		
		$this->set('tin',5);
	}
	
	// danh sách sản phẩm
	public function listproduct1($user_id=null){
		$this->set("title_new","Danh sách sản phẩm");
		$checkLogin = $this->Session->read('user');
		$this->set('tin',6);
		$this->paginate = array(
			"fields"=>array(
				"Product.id",
				"Product.images",
				"Product.user_status",
				"Product.name", 
				"Product.created",				
				"Product.status",				
				"Product.pos",				
			),
			'conditions'=>array(
				"Product.user_id"=>$checkLogin["Usermember"]["id"],			
			)			
		);
        $product = $this->paginate('Product');
        $this->set('product', $product);
		
		
	}
	
	
	public function addproduct($id=null){
	$this->loadModel("Product.Product");
	$this->set('tin',7);
		$checkLogin = $this->Session->read('user');			
		if($checkLogin['Usermember']['role']!=5&& $checkLogin['Usermember']['role']!=1) {
				echo "<script>alert('Chỉ gian hàng và khu vực mới được truy cập địa chỉ này !');</script>";
				echo "<script>location.href='".DOMAIN."quan-tri.html'</script>";
		
		}
		
		$this->set("city_id",$checkLogin["Usermember"]["city_id"]);
		//lấy danh sách catproduct
		// $Catproduct = $this->Catproduct->find('list',array(
			// "fields"=>array(
				// 'Catproduct.id',
				// 'Catproduct.name',		 
			// ),
			// "conditions"=>array(
				// 'Catproduct.status'=>1,
			// )
		// ));
		// $this->set("list_cat",$Catproduct);
		   $this->loadModel("Catproduct.Catproduct");
        $list_cat = $this->Catproduct->generateTreeList(null, null, null, '-- ');
        $this->set(compact('list_cat'));
		
		if (!empty($this->request->data)) {
		
			if($this->Session->read('security_code')!=$_POST['security']){
					echo "<script>alert('".json_encode('Bạn nhập không đúng mã bảo vệ !')."');</script>";
					echo "<script>history.back(-1);</script>"; 
				}
		
			$data = $this->request->data;
			$data['Product']['name']=$_POST['name'];
			
			$data['Product']['images']=isset($_POST['userfile'])?$_POST['userfile']:'';
				
				//Xoa anh cu
			if($data['Product']['images']!=''){
			
			$new=$this->Product->findById($id);
			
			if($data['Product']['images']!=$new['Product']['images'] && $id!=null){
			
			if(file_exists(SITE_DIR.'admin/webroot/'.$new['Product']['images']))
			{
			
				$size=filesize(SITE_DIR.'admin/webroot/'.$new['Product']['images']);
				$checkLogin["Usermember"]["capacity"]=$checkLogin["Usermember"]["capacity"]-$size;
				
				$this->Usermember->save($checkLogin);
			
			
			@unlink($new['Product']['images']);
			}
			
			}
			
			}
			//End xoa anh cu
			
			
			
		
			
			if(!isset($data["Product"]["user_id"]) || $data["Product"]["user_id"]==null) $data["Product"]["user_id"]=$checkLogin["Usermember"]["id"];
			$data["Product"]["status"]=0;
			$data["Product"]["user_status"]=1;
			$this->Product->save($data["Product"]);
			
			$usermember=$this->Usermember->findById($checkLogin["Usermember"]["id"]);
			
				
			if(file_exists(SITE_DIR.'admin/webroot/'.$data["userfile"]))
			{
				$size=filesize(SITE_DIR.'admin/webroot/'.$data["userfile"]);
				$usermember["Usermember"]["capacity"]=$usermember["Usermember"]["capacity"]+$size;
				$this->Usermember->save($usermember);
			}	
			
			if($checkLogin['Usermember']['role']==5)
			echo "<script>location.href='".DOMAIN."san-pham.html'</script>";
			else echo "<script>location.href='".DOMAIN."san-pham-gian-hang.html'</script>";
		}
		else {
			$this->data = $this->Product->read(null, $id);
				
			$this->set('edit',$this->Product->findById($id));
		
		}
		
	}
	
	 function product_process1() {
	 
	 $checkLogin = $this->Session->read('user');			
		if($checkLogin['Usermember']['role']!=5) {
				echo "<script>alert('Chỉ gian hàng mới được truy cập địa chỉ này !');</script>";
				echo "<script>location.href='".DOMAIN."quan-tri.html'</script>";
		
		}
	 
        $process = $_REQUEST['process'];
        $chon = $_REQUEST['chon'];
		if (count($chon) == 0 || $process < 1) {
			$this->redirect($this->referer());     
        }
        switch ($process) {
            case '1' :
                // Update active
                foreach ($chon as $k => $v) {
                    $this->Product->updateAll(
                        array(
                        'Product.user_status' => 1
                        ), array(
                        'Product.id' => $k)
                    );
                }
                break;

            case '2' :
                // Update deactive
                foreach ($chon as $k => $v) {
                    $this->Product->updateAll(
                        array(
                        'Product.user_status' => 0
                        ), array(
                        'Product.id' => $k)
                    );
                }
                break;

            case '3' :
                // delete many rows
                $groupId = "";
                foreach ($chon as $k => $v) {
                    $groupId .= "," . $k;
					
					$new=$this->Product->findById($k);
					$this->delete_url($new['Product']['images'],$new['Product']['user_id']);
                }
                $groupId = substr($groupId, 1);
                $conditions = array(
                    'Product.id IN (' . $groupId . ')'
                );
                $this->Product->deleteAll($conditions);
                break;
        }
        
            $this->redirect($this->referer());
        
    }
	
	 function product_process() {
	 
	 $checkLogin = $this->Session->read('user');			
		if($checkLogin['Usermember']['role']!=1) {
				echo "<script>alert('Chỉ khu vực mới được truy cập địa chỉ này !');</script>";
				echo "<script>location.href='".DOMAIN."quan-tri.html'</script>";
		
		}
	 
        $process = $_REQUEST['process'];
        $chon = $_REQUEST['chon'];
		if (count($chon) == 0 || $process < 1) {
			$this->redirect($this->referer());     
        }
        switch ($process) {
            case '1' :
                // Update active
                foreach ($chon as $k => $v) {
                    $this->Product->updateAll(
                        array(
                        'Product.status' => 1
                        ), array(
                        'Product.id' => $k)
                    );
                }
                break;

            case '2' :
                // Update deactive
                foreach ($chon as $k => $v) {
                    $this->Product->updateAll(
                        array(
                        'Product.status' => 0
                        ), array(
                        'Product.id' => $k)
                    );
                }
                break;

            case '3' :
                // delete many rows
                $groupId = "";
                foreach ($chon as $k => $v) {
                    $groupId .= "," . $k;
					
					$new=$this->Product->findById($k);
					$this->delete_url($new['Product']['images'],$new['Product']['user_id']);
                }
                $groupId = substr($groupId, 1);
                $conditions = array(
                    'Product.id IN (' . $groupId . ')'
                );
                $this->Product->deleteAll($conditions);
                break;
				
				
				  case '4' :
                // Update active
                foreach ($chon as $k => $v) {
                    $this->Product->updateAll(
                        array(
                        'Product.tieubieu' => 1
                        ), array(
                        'Product.id' => $k)
                    );
                }
                break;

            case '5' :
                // Update deactive
                foreach ($chon as $k => $v) {
                    $this->Product->updateAll(
                        array(
                        'Product.tieubieu' => 0
                        ), array(
                        'Product.id' => $k)
                    );
                }
                break;

				
        }
        
            $this->redirect($this->referer());
        
    }
	
	function product_changepos() {
        $vitri = $_REQUEST['order'];
           
        // Update order
        foreach ($vitri as $k => $v) {
			if($v == "") {$v = null;}
            $this->Product->updateAll(
                array(
                'Product.pos' => $v,
               
                ), array(
                'Product.id' => $k)
            );
        }
       $this->redirect($this->referer());
    }
	
	public function get_usermember_name($id=null){
	
	$user=$this->Usermember->findById($id);
	return $user['Usermember']['gianhang'];
	}
	
	public function get_usermember_email($id=null){
	
	$user=$this->Usermember->findById($id);
	return $user['Usermember']['email'];
	}
	public function get_new($id=null){
	
	$user=$this->News->findById($id);
	return $user['News']['name'];
	}
	
	
	//COMMENT create by ONG_TRUM
	public function danhsachcomment(){
		$user=$this->Session->read('user');
		
			if($user['Usermember']['role']!=2) {
				echo "<script>alert('Chỉ đơn vị mới được truy cập địa chỉ này !');</script>";
				echo "<script>location.href='".DOMAIN."quan-tri.html'</script>";
		
		}
			$urlTmp = DOMAIN . $this->request->url;
        $urlTmp = explode(":", $urlTmp);
        if (isset($urlTmp[2])) {
            $startPage = ($urlTmp[2] - 1) * 10 + 1;
        } else {
            $startPage = 1;
        }
        $this->set('startPage', $startPage);
		
		$this->set('tin',8);
		
		$mang=$this->Usermember->find('list',array('fields'=>array('Usermember.id'),'conditions'=>array('Usermember.parent_id'=>$user['Usermember']['id'],'Usermember.role'=>3)));
		$mang[]=$user['Usermember']['id'];
		
		
		$this->paginate=array('conditions'=>array('Comment.user_id'=>$mang),'order'=>'Comment.id','limit'=>12);
		$this->set('Comment',$this->paginate('Comment',array()));
		
	
	}
	
	 function comment_process() {
	 if($user['Usermember']['role']!=2) {
				echo "<script>alert('Chỉ đơn vị mới được truy cập địa chỉ này !');</script>";
				echo "<script>location.href='".DOMAIN."quan-tri.html'</script>";
		
		}
        $process = $_REQUEST['process'];
        $chon = $_REQUEST['chon'];
		if (count($chon) == 0 || $process < 1) {
			$this->redirect($this->referer());     
        }
        switch ($process) {
            case '1' :
                // Update active
                foreach ($chon as $k => $v) {
                    $this->Comment->updateAll(
                        array(
                        'Comment.status' => 1
                        ), array(
                        'Comment.id' => $k)
                    );
                }
                break;

            case '2' :
                // Update deactive
                foreach ($chon as $k => $v) {
                    $this->Comment->updateAll(
                        array(
                        'Comment.status' => 0
                        ), array(
                        'Comment.id' => $k)
                    );
                }
                break;

            case '3' :
                // delete many rows
                $groupId = "";
                foreach ($chon as $k => $v) {
                    $groupId .= "," . $k;
				
                }
                $groupId = substr($groupId, 1);
                $conditions = array(
                    'Comment.id IN (' . $groupId . ')'
                );
                $this->Comment->deleteAll($conditions);
                break;
        }
        
            $this->redirect($this->referer());
        
    }
	public function active_comment($id=null) {
	if($user['Usermember']['role']!=2) {
				echo "<script>alert('Chỉ đơn vị mới được truy cập địa chỉ này !');</script>";
				echo "<script>location.href='".DOMAIN."quan-tri.html'</script>";
		
		}
       $this->Comment->id=$id;
	   $this->Comment->saveField('status',1);
        $this->redirect($this->referer());
    }

    public function close_comment($id=null) {
	if($user['Usermember']['role']!=2) {
				echo "<script>alert('Chỉ đơn vị mới được truy cập địa chỉ này !');</script>";
				echo "<script>location.href='".DOMAIN."quan-tri.html'</script>";
		
		}
      $this->Comment->id=$id;
	   $this->Comment->saveField('status',0);
       $this->redirect($this->referer());
    }

    public function delete_comment($id=null) {
	if($user['Usermember']['role']!=2) {
				echo "<script>alert('Chỉ đơn vị mới được truy cập địa chỉ này !');</script>";
				echo "<script>location.href='".DOMAIN."quan-tri.html'</script>";
		
		}
		
		$this->Comment->delete($id);
		$this->redirect($this->referer());
    }
	//END COMMENT create by ONG_TRUM
	
	
	//SLIDESHOW create by ONG_TRUM
	public function slideshow(){
		$user=$this->Session->read('user');
		
			if($user['Usermember']['role']!=5) {
				echo "<script>alert('Chỉ gian hàng mới được truy cập địa chỉ này !');</script>";
				echo "<script>location.href='".DOMAIN."quan-tri.html'</script>";
		
		}
			$urlTmp = DOMAIN . $this->request->url;
        $urlTmp = explode(":", $urlTmp);
        if (isset($urlTmp[2])) {
            $startPage = ($urlTmp[2] - 1) * 10 + 1;
        } else {
            $startPage = 1;
        }
        $this->set('startPage', $startPage);
		
		$this->set('tin',9);
		
		
		
		$this->paginate=array('conditions'=>array('Slideshow.user_id'=>$user['Usermember']['id']),'order'=>'Slideshow.id','limit'=>12);
		$this->set('Slideshow',$this->paginate('Slideshow',array()));
		
	
	}
	
	 function slideshow_process() {
	 if($user['Usermember']['role']!=5) {
				echo "<script>alert('Chỉ gian hàng mới được truy cập địa chỉ này !');</script>";
				echo "<script>location.href='".DOMAIN."quan-tri.html'</script>";
		
		}
        $process = $_REQUEST['process'];
        $chon = $_REQUEST['chon'];
		if (count($chon) == 0 || $process < 1) {
			$this->redirect($this->referer());     
        }
        switch ($process) {
            case '1' :
                // Update active
                foreach ($chon as $k => $v) {
                    $this->Slideshow->updateAll(
                        array(
                        'Slideshow.status' => 1
                        ), array(
                        'Slideshow.id' => $k)
                    );
                }
                break;

            case '2' :
                // Update deactive
                foreach ($chon as $k => $v) {
                    $this->Slideshow->updateAll(
                        array(
                        'Slideshow.status' => 0
                        ), array(
                        'Slideshow.id' => $k)
                    );
                }
                break;

            case '3' :
                // delete many rows
                $groupId = "";
                foreach ($chon as $k => $v) {
                    $groupId .= "," . $k;
					
					$new=$this->Slideshow->findById($k);
					$this->delete_url($new['Slideshow']['images'],$new['Slideshow']['user_id']);
				
                }
                $groupId = substr($groupId, 1);
                $conditions = array(
                    'Slideshow.id IN (' . $groupId . ')'
                );
                $this->Slideshow->deleteAll($conditions);
                break;
        }
        
            $this->redirect($this->referer());
        
    }
	public function active_Slideshow($id=null) {
	if($user['Usermember']['role']!=5) {
				echo "<script>alert('Chỉ gian hàng mới được truy cập địa chỉ này !');</script>";
				echo "<script>location.href='".DOMAIN."quan-tri.html'</script>";
		
		}
       $this->Slideshow->id=$id;
	   $this->Slideshow->saveField('status',1);
        $this->redirect($this->referer());
    }

    public function close_Slideshow($id=null) {
	if($user['Usermember']['role']!=5) {
				echo "<script>alert('Chỉ gian hàng mới được truy cập địa chỉ này !');</script>";
				echo "<script>location.href='".DOMAIN."quan-tri.html'</script>";
		
		}
      $this->Slideshow->id=$id;
	   $this->Slideshow->saveField('status',0);
       $this->redirect($this->referer());
    }

    public function delete_Slideshow($id=null) {
	if($user['Usermember']['role']!=5) {
				echo "<script>alert('Chỉ gian hàng mới được truy cập địa chỉ này !');</script>";
				echo "<script>location.href='".DOMAIN."quan-tri.html'</script>";
		
		}
		
		
		$new=$this->Slideshow->findById($id);
		$this->delete_url($new['Slideshow']['images'],$new['Slideshow']['user_id']);
		
		$this->Slideshow->delete($id);
		$this->redirect($this->referer());
    }
	
	//Them bai viet
	public function addslideshow($id=null) {
	//$this->check_account();
	$this->set('tin',9);
		if (!empty($this->data)) {
			
			
			if($this->Session->read('security_code')!=$_POST['security']){
					echo "<script>alert('".json_encode('Bạn nhập không đúng mã bảo vệ !')."');</script>";
					echo "<script>history.back(-1);</script>";  die;
				}
		
			
			$this->Slideshow->create();
			$data['Slideshow'] = $this->data['Slideshow'];
			//pr($data['Product']); die;
			$user = $this->Session->read('user');
			
			
			$data['Slideshow']['user_id']=$user['Usermember']['id'];
			
		
			
			$data['Slideshow']['name']=$_POST['name'];
		
			$data['Slideshow']['images']=isset($_POST['userfile'])?$_POST['userfile']:'';
			
			//Xoa anh cu
			if($data['Slideshow']['images']!=''&& $id=null){
			
			$new=$this->Slideshow->findById($id);
			
			if($data['Slideshow']['images']!=$new['Slideshow']['images'] && $id!=null){
			
				if(file_exists(SITE_DIR.'admin/webroot/'.$new['Slideshow']['images']))
					{
					
						$size=filesize(SITE_DIR.'admin/webroot/'.$new['Slideshow']['images']);
						$user["Usermember"]["capacity"]=$user["Usermember"]["capacity"]-$size;
						
						$this->Usermember->save($user);
					
					
					@unlink($new['Slideshow']['images']);
					}
			
			}
			
			}
			//End xoa anh cu
			
			
		
		
			
			
			//pr($data['Product']);die;
			if ($this->Slideshow->save($data['Slideshow'])) {
			
				//Tinh dung luong anh
		
				
			if(file_exists(SITE_DIR.'admin/webroot/'.$data['Slideshow']['images']))
			{
				$size=filesize(SITE_DIR.'admin/webroot/'.$data['Slideshow']['images']);
				$user["Usermember"]["capacity"]=$user["Usermember"]["capacity"]+$size;
				
				$this->Usermember->save($user);
			}	
			//End tin dung luong anh
			
			
				echo "<script>alert('Cập nhật thành công !');</script>";
				echo "<script>location.href='".DOMAIN."slide-show.html'</script>";
			} else {
				$this->Session->setFlash(__('Thêm mới danh mục thất bại. Vui lòng thử lại', true));
			}
		}
		else {
		$this->data = $this->Slideshow->read(null, $id);
			
		$this->set('edit',$this->Slideshow->findById($id));
		}
		
		
	}
	
	function slideshow_changepos() {
        $vitri = $_REQUEST['order'];
           
        // Update order
        foreach ($vitri as $k => $v) {
			if($v == "") {$v = null;}
            $this->Slideshow->updateAll(
                array(
                'Slideshow.pos' => $v,
               
                ), array(
                'Slideshow.id' => $k)
            );
        }
       $this->redirect($this->referer());
    }
	
	
	
	//END SLIDESHOW create by ONG_TRUM
	
	public function get_usermember_all($id=null){
	
	return $this->Usermember->findById($id);
	
	}
	
	//END QUAN LY DON HANG created 
	public function quanlydonhang(){
	
	$user=$this->Session->read('user');
	
	$this->paginate=array(
			'conditions'=>array(
				'Email.user_id'=>$user['Usermember']['id'],
				
			),
			'order' =>'Email.id',
			'limit'=>6
	
	);
	
	$this->set('Email',$this->paginate('Email',array()));
	
		$this->set('tin',12);
			
			$urlTmp = DOMAIN . $this->request->url;
        $urlTmp = explode(":", $urlTmp);
        if (isset($urlTmp[2])) {
            $startPage = ($urlTmp[2] - 1) * 10 + 1;
        } else {
            $startPage = 1;
        }
        $this->set('startPage', $startPage);
			
		
	}
	
	 public function delete_email($id) {
        
        $this->Email->delete($id);
       $this->redirect($this->referer());
    }
	
	public function detail_email($id=null){
	
	$this->set('views',$this->Email->findById($id));
	}
	
	 function email_process() {
        $process = $_REQUEST['process'];
        $chon = $_REQUEST['chon'];
		if (count($chon) == 0 || $process < 1) {
			$this->redirect($this->referer());     
        }
        switch ($process) {
            case '1' :
                // Update active
                foreach ($chon as $k => $v) {
                    $this->Email->updateAll(
                        array(
                        'Email.status' => 1
                        ), array(
                        'Email.id' => $k)
                    );
                }
                break;

            case '2' :
                // Update deactive
                foreach ($chon as $k => $v) {
                    $this->Email->updateAll(
                        array(
                        'Email.status' => 0
                        ), array(
                        'Email.id' => $k)
                    );
                }
                break;

            case '3' :
                // delete many rows
                $groupId = "";
                foreach ($chon as $k => $v) {
                    $groupId .= "," . $k;
					
					
					
                }
                $groupId = substr($groupId, 1);
                $conditions = array(
                    'Email.id IN (' . $groupId . ')'
                );
                $this->Email->deleteAll($conditions);
                break;
        }
        
            $this->redirect($this->referer());
        
    }
	
	
	
	 public function active_email($id=null) {
       $this->Email->id=$id;
	   $this->Email->saveField('status',1);
        $this->redirect($this->referer());
    }

    public function close_email($id=null) {
		$this->Email->id=$id;
		$this->Email->saveField('status',0);
		$this->redirect($this->referer());
    }

	
	//END QUAN LY DON HANG
	
	public function lichsumuahang(){
	
	
	}
	
	
}

?>
