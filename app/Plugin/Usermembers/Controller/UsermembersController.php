<?php

class UsermembersController extends AppController {

    public $name = 'Usermembers';
    public $uses = array(
		'Usermember', 
		'City', 
		'Setting', 
		'Product',
		'Slideshow',
		'New'
		);
    public $components = array('Email');


    public function index() {
        
    }



    function getsecuritycode() {
        echo $this->Session->read('security_code');
        die;
    }

    function create_image() {
        $md5_hash = md5(rand(0, 999));
        $security_code = substr($md5_hash, 15, 5);
        $this->Session->write('security_code', $security_code);
        $width = 80;
        $height = 22;
        $image = ImageCreate($width, $height);
        $black = ImageColorAllocate($image, 37, 170, 226);
        $white = ImageColorAllocate($image, 255, 255, 255);
        ImageFill($image, 0, 0, $black);
        ImageString($image, 5, 18, 3, $security_code, $white);
        header("Content-Type: image/jpeg");
        ImageJpeg($image);
        ImageDestroy($image);
        die;
    }

    public function creatcaptcha() {
        //session_start(); // Khởi tạo session
        $ranStr = md5(microtime()); // Lấy chuỗi rồi mã hóa md5
        $ranStr = substr($ranStr, 0, 6); // Cắt chuỗi lấy 6 ký tự
        $_SESSION['cap_code'] = $ranStr; // Lưu giá trị vào session
        $newImage = imagecreatefromjpeg(DOMAIN . "/images/bg_captcha.jpg"); // Tạo hình ảnh từ bg_captcha.jpg
        $txtColor = imagecolorallocate($newImage, 0, 0, 0); // Thêm màu sắc cho hình ảnh 
        imagestring($newImage, 5, 5, 5, $ranStr, $txtColor); // Vẽ ra chuỗi string
        header("Content-type: image/jpeg"); // Xuất định dạng là hình ảnh
        imagejpeg($newImage); // Xuất hình ảnh ra trình như 1 file
    }

    function create_image1($random) {

        $md5_hash = md5(rand(0, 999));
        $security_code = substr($md5_hash, 15, 5);
        $this->Session->write('security_code', $security_code);
        $width = 80;
        $height = 22;
        $image = ImageCreate($width, $height);
        $black = ImageColorAllocate($image, 37, 170, 226);
        $white = ImageColorAllocate($image, 255, 255, 255);
        ImageFill($image, 0, 0, $black);
        ImageString($image, 5, 18, 3, $security_code, $white);
        header("Content-Type: image/jpeg");
        ImageJpeg($image);
        ImageDestroy($image);
        die;
    }

    /* Ham lay lai mat khau */

    function forgot_password() {
        
    }

    function ck_email_forgot_pass() {
        $this->layout = 'ajax';
        $email = $_GET['email_forgot_password'];
        $this->Usermember->unbindModel(array('hasMany' => array('Immovable')));
        $check_mail = $this->Usermember->findByEmail($email);
        if ($check_mail['Usermember']['email']) {
            echo "<span style='color:#00FF00;'>Đã xác nhận email</span>";
        } else {
            echo "<span style='color:#FF0000;font-size:11px;'>Email không đúng</span>";
        }
    }

    function security_forgot_pass() {
        $this->layout = 'ajax';
        $security = $_GET['security'];
        if ($security == $this->Session->read('security_code')) {
            echo "<span style='color:#00FF00;margin:150px;'>ok</span>";
        } else {
            echo "<span style='color:#FF0000;margin:150px;font-size:11px;'>Mã bảo mật không đúng</span>";
        } 
    }

 
    function checkIfLogged() {
        if (!$this->Session->read("email") || !$this->Session->read("id")) {
            $this->redirect('/login');
        }
    }

  


	public function logout() {
			$this->Session->delete('user');
			$this->redirect('/');
	}	
	

	
}

?>
