/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


$(function(){    
    //xác nhân khi click submit
    $("#xacNhanDongY").click(function(){
        alert("OK \n\ Bạn đã đồng ý với các điều khoản của chúng tôi");
        $("#accept").val(1);
    })
    
 
    $("#quanTriKhuVuc").submit(function(){        
        if($("#cmnd").val()==""){
            alert("Bạn cần nhập vào số chứng minh nhân dân của mình");
            $("#cmnd").focus();
            return false;
        }
        if($("#email").val()==""){
            alert("Bạn cần nhập vào Email của mình");
            $("#email").focus();
            return false;
        }
        
        // check email
        var email = $("#email").val();        
        var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if (!filter.test(email)) {
            alert('Xin hãy kiểm tra lại email bạn nhập vào');
            $("#email").focus();
            return false;
        }
        
        
        
        if($("#password").val()==""){
            alert("Bạn cần nhập vào passwork");
            $("#password").focus();
            return false;
        }
        if($("#repasswork").val()==""){
            alert("Bạn cần xác nhận lại passwork");
            $("#repasswork").focus();
            return false;
        }
         if($("#repasswork").val()!=$("#password").val()){
            alert("Hai mật khẩu không giống nhau.");
            $("#repasswork").focus();
            return false;
        }
        if($("#birthday").val()==""){
            alert("Bạn cần nhập vào năm sinh của mình");
            $("#birthday").focus();
            return false;
        }
        if($("#address").val()==""){
            alert("Bạn cần địa chỉ của mình");
            $("#address").focus();
            return false;
        }
        if($("#phone").val()==""){
            alert("Bạn cần nhập vào số điện thoại của mình");
            $("#phone").focus();
            return false;
        }
        if($("#accept").val() != 1){//nếu chưa click vào xác nhận đồng ý với các điều khoản thì hiện lên thông báo.
            alert("Bạn phải đồng ý với các điều kiện của chúng tôi\n\Xin hãy click vào nút 'Xác nhận đồng ý' ");
            return false;
        }
    })
})