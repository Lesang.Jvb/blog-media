function keypress(e){
    //Hàm dùng d? ngan ngu?i dùng nh?p các ký t? khác ký t? s? vào TextBox
    var keypressed = null;
    if (window.event)
    {
        keypressed = window.event.keyCode; //IE
    }
    else
    { 
        keypressed = e.which; //NON-IE, Standard
    }
    if (keypressed < 48 || keypressed > 57)
    { //CharCode c?a 0 là 48 (Theo b?ng mã ASCII)
        //CharCode c?a 9 là 57 (Theo b?ng mã ASCII)
        if (keypressed == 8 || keypressed == 127)
        {//Phím Delete và Phím Back
            return;
        }
        if (keypressed == 45 || keypressed == 32)
        {//Phím Delete và Phím Back
            return true;
        }
        return false;
    }
}



function OpenPopup(url, name){
	popupWin = window.open(url, name, 'scrollbars=yes,resizable=yes,width=500,height=300');
}

  jQuery(document).ready(function($) {
	
        $("#quanTriKhuVuc").validate({
            rules: {
				 name: {
                    required: true,
                  
                },
				 gianhang: {
                    required: true,
                  
                },
				company: {
                    required: true,
                  
                },
                address: {
                    required: true,
                  
                },
				   phone: {
                    required: true,
                  
                },
				
                password: {
                    required: true,
                    minlength: 5
                },
                confirm_password: {
                   required: true,
                   minlength: 5,
                   equalTo: "#password1"
                },	
			
                email: {
                    required: true,
                    email: true
                },
                confirm_email: {
                    required: true,
                    equalTo: "#email1"
                },	
			
                security: {
                    required: true,
                    minlength: 5,
                    maxlength:5
                },
                birthday:{
                    required: true
                },
                cmnd:{
                    required: true
                },
				
				  security: {
                    required: true,
                    minlength: 5,
                    maxlength:5
                },
			
		
		 info: {
                    required: true,
                   
                    maxlength:500
                },
			
            },
            messages: {
			
				name: {
                    required: " <br><span style='color:#FF0000;'>Xin vui lòng nhập họ tên!</span>",
                    
                },
				gianhang: {
                    required: " <br><span style='color:#FF0000;'>Xin vui lòng nhập tên gian hàng!</span>",
                    
                },
				
				
				company: {
                    required: " <br><span style='color:#FF0000;'>Xin vui lòng tên đơn vị!</span>",
                    
                },
				
                address: {
                    required: " <br><span style='color:#FF0000;'>Xin vui lòng nhập vào địa chỉ!</span>",
                    
                },
				 phone: {
                    required: " <br><span style='color:#FF0000;'>Xin vui lòng nhập vào số điện thoại!</span>",
                    
                },
                email: {
                    required: " <br><span style='color:#FF0000;'>Xin vui lòng nhập vào Email!</span>",
                    email: " <br><span style='color:#FF0000;'>Email không đúng!</span>"
                },
                password: {
                    required: "<br><span style='color:#FF0000;' >Xin vui lòng nhập password !</span>",
                    minlength: "<br><span style='color:#FF0000;' > Xin vui lòng nhập password có chiều dài hơn 5 ký tự !</span>"
                },
                security: {
                    required: " <br><span style='color:#FF0000;' >Xin vui lòng nhập mã bảo vệ!</span>",
                    minlength: "<br><span style='color:#FF0000;' > Mã bảo vệ chỉ bao gồm ít nhất 5 kí tự!</span>",
                    maxlength: "<br><span style='color:#FF0000;' > Mã bảo vệ chỉ bao gồm ít nhất 5 kí tự!</span>"
                },
                confirm_password: {
                   required: "<br><span style='color:#FF0000;' >Xin vui lòng nhập lại password !</span>",
                   minlength: "<br><span style='color:#FF0000; ' >Xin vui lòng nhập password có chiều dài hơn 5 ký tự !</span>",
                   equalTo: "<br><span style='color:#FF0000;' > password không đúng !</span>"
                },
                confirm_email: {
                    required: "<br><span style='color:#FF0000;' >Xin vui lòng nhập lại email!</span>",
			
                    equalTo: "<br><span style='color:#FF0000;' > Không giống email ở trên !</span>"
                },
                birthday: {
                    required: " <br><span style='color:#FF0000;'>Nhập năm sinh của bạn!</span>"
				
                },
                cmnd: {
                    required: " <br><span style='color:#FF0000;'>Xin vui lòng nhập số chứng minh nhân dân!</span>"
				
                },
				  security: {
                    required: " <br><span style='color:#FF0000;' >Xin vui lòng nhập mã bảo vệ!</span>",
                    minlength: "<br><span style='color:#FF0000;' > Mã bảo vệ chỉ bao gồm ít nhất 5 kí tự!</span>",
                    maxlength: "<br><span style='color:#FF0000;' > Mã bảo vệ chỉ bao gồm ít nhất 5 kí tự!</span>"
                },
			 info: {
                    required: " <br><span style='color:#FF0000;' >Xin vui lòng nhập các thông tin năng lực của tổ chức cá nhân!</span>",
                  
                    maxlength: "<br><span style='color:#FF0000;' > Các thông tin năng lực của tổ chức cá nhân chỉ bao gồm nhiều nhất 500 kí tự!</span>"
                },
			
            }
        });	
		
		    //xác nhân khi click submit
    $("#xacNhanDongY").click(function(){
        alert("OK \n\ Bạn đã đồng ý với các điều khoản của chúng tôi");
        $("#accept").val(1);
    })
    
	


	
    });
	function test(){

        if ($('#checkboxAccept').is(":checked"))
        {
        /// 
		return true;
        }else{
            alert("Bạn cần phải đồng ý với các điều khoản của chúng tôi");
            $('#checkboxAccept').focus();
            return false;
        }
	
	}
	
	
	  function reload()
    {  
        var random1= Math.random()*5
        jQuery.ajax({
            type: "GET", 
            url: "<?php echo DOMAIN; ?>"+'usermembers/usermembers/create_image1/'+random1,
            data: null,
            success: function(msg){	
                jQuery('#abc').find('img').remove().end();
                jQuery('#abc').append('<img alt="" id="captcha" src="<?php echo DOMAIN ?>usermembers/usermembers/create_image1/'+random1+'" />');				
            }
        });	
    }
	
		$(function($){
		$("#email").change(function(){
			var email=$("#email").val();
			$.ajax({
				type: "GET", 
				url: "<?php echo DOMAIN;?>"+'usermembers/usermembers/ck_mail_register/',
				data: 'email='+email,
				success: function(msg){	
					//alert (msg);	
					$('#validate-emai-register').find('span').remove().end();										
					$('#validate-emai-register').append(msg);					
				}
			});
			
		});
	
	});

