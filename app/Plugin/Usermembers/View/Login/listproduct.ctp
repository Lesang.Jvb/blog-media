<link rel="stylesheet" type="text/css" href="<?php echo DOMAIN ?>usermembers/css/dangnhap.css" />  


<?php
echo $this->Html->css(array('phantrang'));
?>

<script>
    function confirmDelete(delUrl)
    {
        if (confirm("Bạn có chắc chắn xóa tin rao vặt này không!"))
        {
            document.location = delUrl;
        }
    }
</script>
<script type="text/javascript">
    function changepos() {
        document.frm1.action = "<?php echo DOMAIN; ?>usermembers/login/new_changepos";
        document.frm1.submit();
    }
    function process() {
        document.frm1.action = "<?php echo DOMAIN; ?>usermembers/login/product_process";
        document.frm1.submit();
    }
    function MM_jumpMenu(targ,selObj,restore){ //v3.0
        eval(targ+".location='"+selObj.options[selObj.selectedIndex].value+"'");
        if (restore) selObj.selectedIndex=0;
    }
	$(document).ready(function(){
    $('#checkall:checkbox').change(function () {
        if($(this).attr("checked")) $('input:checkbox').attr('checked','checked');
        else $('input:checkbox').removeAttr('checked');
    });
})
</script>
<style>
.button{
	  background: url("<?php echo DOMAINAD?>images/bg-button-green.gif") repeat-x scroll left top #459300 !important;
    border: 1px solid #459300 !important;
    color: #FFFFFF !important;
    cursor: pointer;
    display: inline-block;
    font-family: Verdana,Arial,sans-serif;
    font-size: 11px !important;
    padding: 4px 7px !important;
}
#select{
	border: 1px solid #CCCCCC;
    padding: 4px;
}
.bulk-actions{
	float:left;
	padding:10px 0px;;
}
.pagination{
	padding:10px 0px;
}
.tb_tin td{
	padding:10px !important;
}
</style>
<div id="Content">
    <?php echo $this->element('usermember_left'); ?>

    <div class="cot2">
        <h2 class="h2_title"> <?php echo $title_new?></h2>
        <div class="col_product">
            <div class="text-main">
			<form name="frm1">
                <table style="width:100%" class="tb_tin">
				
				<thead>
					
                    <tr class="tbTitle">
					  <td width="2%"><input type="checkbox" name="all" id="checkall" /></td>
                        <td width="4%">STT</td>
                        <td width="100px">Ảnh đại diện</td>
                        <td>Tiêu đề</td>                        
                        <td>Ngày đăng</td>
                        <td>Thuộc gian hàng</td>
                        <td>Sp tiêu biểu</td>
                        <td>Thao tác</td>
                    </tr>
					 </thead>
					  <tfoot>
                        <tr>
                            <td colspan="9"><div class="bulk-actions align-left">
                                    <select name="process" id="select">
                                        <option value="0">Lựa chọn</option>
                                        <option value="1">Active</option>
                                        <option value="2">Hủy Active</option>
                                        <option value="3">Delete</option>
										 <option value="4">Active tiêu biểu</option>
                                        <option value="5">Hủy Active tiêu biểu</option>
                                    </select>
                                    <a class="button" href="#" onclick="process()">Thực hiện</a> </div>
                                <div class="pagination" style="float:right">
                                    <?php
                                        echo $this->Paginator->first('« Đầu ', null, null, array('class' => 'disabled'));     
                                        echo $this->Paginator->prev('« Trước ', null, null, array('class' => 'disabled')); 
                                        echo $this->Paginator->numbers()." ";
                                        echo $this->Paginator->next(' Tiếp »', null, null, array('class' => 'disabled')); 
                                        echo $this->Paginator->last('« Cuối ', null, null, array('class' => 'disabled')); 
                                        echo " Page ".$this->Paginator->counter();
                                    ?>
                                </div>
                                <div class="clear"></div></td>
                        </tr>
                    </tfoot>
					
					 <tbody>
                    <?php 
					
					foreach ($product as $key=>$products) { ?>
					
					
					
                        <tr style="background-color: #DEDEDE;">
						
						 <td>
							<input type="checkbox" name="chon[<?php echo $products['Product']['id']; ?>]" value="1" />
						 </td>
                            <td><?php echo $key+1;?></td>
						
                            <td>                                
                                <img src="<?php echo DOMAINAD . $products['Product']['images']; ?>" width="100" />
                            </td>
                            <td>                                 
                               <?php echo $products['Product']['name']; ?> 
                            </td>                                                         
                            <td class="proccess">                                
                                <p><?php echo date('d-m-Y',strtotime($products['Product']['created'])); ?></p>
                            </td>  
							<td class="proccess">                                
                                <p>
								<?php $usermem=$this->requestAction('usermembers/login/get_usermember_all/'.$products['Product']['user_id']);
									echo $usermem['Usermember']['company'];
								?>
								</br>Địa chỉ:
								<a href="<?php echo DOMAIN?>gian-hang/<?php echo $usermem['Usermember']['gianhang']?>">
								<?php echo DOMAIN.'gian-hang/'.$usermem['Usermember']['gianhang'];?></a>
								</br>
								<?php echo 'Email: ' .$usermem['Usermember']['email'];?>
								</p>
                            </td> 	
							<td class="proccess">
							<?php if ($products['Product']['tieubieu'] == 1) { ?>
                                    <a href="<?php echo DOMAIN . "usermembers/login/close_sptb/" . $products['Product']['id'] ?>"><img src="<?php echo DOMAINAD ?>images/icons/success-icon.png" /></a>
                                <?php } else { ?>
                                    <a href="<?php echo DOMAIN . "usermembers/login/active_sptb/" . $products['Product']['id'] ?>"><img src="<?php echo DOMAINAD ?>images/icons/Play-icon.png" /></a>                                        
                                <?php } ?>                                
                                                              
                            </td>							
							
                            <td class="proccess">     
							
							<a href="<?php echo DOMAIN ?>usermembers/login/addproduct/<?php echo $products['Product']['id']; ?>" title="Edit"><img src="<?php echo DOMAINAD ?>images/icons/pencil.png" alt="Edit" /></a>
								<a href="<?php echo DOMAIN . "usermembers/login/delete_product/" . $products['Product']['id'] ?>"><img src="<?php echo DOMAINAD ?>images/icons/cross.png" /></a>  	
                                <?php if ($products['Product']['status'] == 1) { ?>
                                    <a href="<?php echo DOMAIN . "usermembers/login/close_product/" . $products['Product']['id'] ?>"><img src="<?php echo DOMAINAD ?>images/icons/success-icon.png" /></a>
                                <?php } else { ?>
                                    <a href="<?php echo DOMAIN . "usermembers/login/active_product/" . $products['Product']['id'] ?>"><img src="<?php echo DOMAINAD ?>images/icons/Play-icon.png" /></a>                                        
                                <?php } ?>                                
                                                              
                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
				</form>

            </div>
        </div>



    </div><!-- End dangky-->





</div>
