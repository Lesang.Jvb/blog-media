<link rel="stylesheet" type="text/css" href="<?php echo DOMAIN ?>usermembers/css/dangnhap.css" />  


<?php
echo $this->Html->css(array('phantrang'));
?>

<script>
    function confirmDelete(delUrl)
    {
        if (confirm("Bạn có chắc chắn xóa tin rao vặt này không!"))
        {
            document.location = delUrl;
        }
    }
</script>
<script type="text/javascript">
    function changepos() {
        document.frm1.action = "<?php echo DOMAIN; ?>usermembers/login/email_changepos";
        document.frm1.submit();
    }
    function process() {
        document.frm1.action = "<?php echo DOMAIN; ?>usermembers/login/email_process";
        document.frm1.submit();
    }
    function MM_jumpMenu(targ,selObj,restore){ //v3.0
        eval(targ+".location='"+selObj.options[selObj.selectedIndex].value+"'");
        if (restore) selObj.selectedIndex=0;
    }
	$(document).ready(function(){
    $('#checkall:checkbox').change(function () {
        if($(this).attr("checked")) $('input:checkbox').attr('checked','checked');
        else $('input:checkbox').removeAttr('checked');
    });
})
</script>
<style>
.button{
	  background: url("<?php echo DOMAINAD?>images/bg-button-green.gif") repeat-x scroll left top #459300 !important;
    border: 1px solid #459300 !important;
    color: #FFFFFF !important;
    cursor: pointer;
    display: inline-block;
    font-family: Verdana,Arial,sans-serif;
    font-size: 11px !important;
    padding: 4px 7px !important;
}
#select{
	border: 1px solid #CCCCCC;
    padding: 4px;
}
.bulk-actions{
	float:left;
	padding:10px 0px;;
}
.pagination{
	padding:10px 0px;
}
.tb_tin td{
	padding:10px !important;
}
</style>
<div id="Content">
    <?php echo $this->element('usermember_left'); ?>

    <div class="cot2">
        <h2 class="h2_title"> Quản lý đơn hàng</h2>
        <div class="col_product">
            <div class="text-main">
			<form name="frm1">
                <table style="width:100%" class="tb_tin">
				
				<thead>
					
                    <tr class="tbTitle">
					  <td width="2%"><input type="checkbox" name="all" id="checkall" /></td>
                       <td width="4%">STT</td>
                       <th>Ngày gửi</th>
                       <td>Tên người gửi</td>
                       <td>Email</td>
					  
                       <td>Điện thoại</td>
					   
                        <td>Thanh lý</td>
                        <td>Thao tác</td>
                    </tr>
					 </thead>
					  <tfoot>
                        <tr>
                            <td colspan="9"><div class="bulk-actions align-left">
                                    <select name="process" id="select">
                                        <option value="0">Lựa chọn</option>
                                        <option value="1">Active</option>
                                        <option value="2">Hủy Active</option>
                                        <option value="3">Delete</option>
                                    </select>
                                    <a class="button" href="#" onclick="process()">Thực hiện</a> </div>
                                <div class="pagination" style="float:right">
                                    <?php
                                        echo $this->Paginator->first('« Đầu ', null, null, array('class' => 'disabled'));     
                                        echo $this->Paginator->prev('« Trước ', null, null, array('class' => 'disabled')); 
                                        echo $this->Paginator->numbers()." ";
                                        echo $this->Paginator->next(' Tiếp »', null, null, array('class' => 'disabled')); 
                                        echo $this->Paginator->last('« Cuối ', null, null, array('class' => 'disabled')); 
                                        echo " Page ".$this->Paginator->counter();
                                    ?>
                                </div>
                                <div class="clear"></div></td>
                        </tr>
                    </tfoot>
					
					 <tbody>
                    <?php 
					
					foreach ($Email as $key=>$value) { ?>
					
					
					
                        <tr style="background-color: #DEDEDE;">
						
						
                            <td><input type="checkbox" name="<?php echo $value['Email']['id']; ?>" /></td>
                        <td><?php $j=$key+1; echo $j;?></td>
						 <td><?php echo date('d-m-Y', strtotime($value['Email']['created'])); ?></td>
						
                        <td><a href="<?php echo DOMAIN?>chi-tiet-don-hang/<?php echo $value['Email']['id']?>"><?php echo $value['Email']['name']; ?></a></td>
                        <td><?php  echo $value['Email']['email'];?></td>
						
						 <td><?php  echo $value['Email']['phone'];?></td>
                       <td class="proccess">
					     <?php if ($value['Email']['status'] == 1) { ?>
                                    <a href="<?php echo DOMAIN . "usermembers/login/close_email/" . $value['Email']['id'] ?>"><img src="<?php echo DOMAINAD ?>images/icons/success-icon.png" /></a>
                                <?php } else { ?>
                                    <a href="<?php echo DOMAIN . "usermembers/login/active_email/" . $value['Email']['id'] ?>"><img src="<?php echo DOMAINAD ?>images/icons/Play-icon.png" /></a>                                        
                                <?php } ?>     
					   </td>
					
                        <td class="proccess">
                             
                             <a href="javascript:confirmDelete('<?php echo DOMAIN?>usermembers/login/delete_email/<?php echo $value['Email']['id'] ?>')" title="Delete"><img src="<?php echo DOMAINAD?>images/icons/cross.png" alt="Delete" /></a>
                     
                        </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
				</form>

            </div>
        </div>



    </div><!-- End dangky-->





</div>
