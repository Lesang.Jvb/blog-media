<link rel="stylesheet" type="text/css" href="<?php echo DOMAIN ?>usermembers/css/dangnhap.css" />  

<link rel="stylesheet" type="text/css" href="<?php echo DOMAIN ?>usermembers/css/mystyle.css" />  

<?php
echo $this->Html->css(array('phantrang'));
?>

<?php echo $this->Html->script('jquery.validate', true); ?>
<script type="text/javascript" src="<?php echo DOMAIN?>Usermembers/js/my_script.js"></script> 


<div id="Content">
<?php echo $this->element('usermember_left');?>

  <div class="cot2">

   <h2 class="h2_title">Thông tin tài khoản của <?php $user=$this->Session->read('user'); echo $user['Usermember']['name']?></h2>
    <div class="col_product">
        <div class="text-main">
	
         <div id="main-register">
              <form id="quanTriKhuVuc" action="<?php echo DOMAIN . "usermembers/usermembers/add/1" ?>" method="POST" name="image" onsubmit="return test();">    
        <div class="main-bottom"> 
            <table style="width:95%">
                <tr>
                    <td width="40%">
                        <p><span>1) </span>Khu vực quản trị</p>
                        <div id="selectKhuVuc">
								<?php if(!isset($id)) echo $this->requestAction('usermembers/login/get_name_city/'.$edit['Usermember']['city_id']);
								else { ?>													

                        <div id="selectKhuVuc" style="margin-left:0px;">
                            <p>TỈNH / THÀNH PHỐ</p>
                            <select  name="data[Usermember][city_id]" size="10">                                
                                <?php $ok = 0; foreach ($city as $key => $citys) { ?> 
                                    <option <?php  if($ok == 0){echo "selected";} $ok = 1; ?> value="<?php echo $key ?>"><?php echo $citys; ?></option>
                                <?php } ?>                                
                            </select>                                                        
                        </div>
						<?php } ?>
                        </div>
						<div id="name">
                            <p><span>6) </span>Họ tên</p>
                            <input class="input" name="name" type="text" value="<?php echo $edit['Usermember']['name']?>" />
                        </div>
                        <div id="soCMND">
                            <p><span>7) </span>Số CMND</p>
                            <input class="input" value="<?php echo $edit['Usermember']['cmnd']?>" name="cmnd" maxlength="9" type="text" onkeypress="return keypress(event);" />
                        </div>
						   
						 
                        </div>
						
						<input type="hidden" name="data[Usermember][role]" value="1"/>
						<input type="hidden" name="data[Usermember][role_old]" value="<?php if($edit['Usermember']['role']!=1) echo $edit['Usermember']['role']?>"/>
						<input type="hidden" name="data[Usermember][id]" value="<?php echo $edit['Usermember']['id']?>"/>
                    </td>
                    <td>
                        <ul>
                            <li>
                                <p><span>2) </span>Email đăng ký</p>
                                <input id="email" name="email" type="text"  value="<?php echo $edit['Usermember']['email']?>" readonly="true" />
								<div id="validate-emai-register"><span id="error"></span></div>
                            </li>
                           
                            <li>
                                <p><span>3) </span>Năm sinh</p>
                                <input id="birthday" name="birthday" type="text" onkeypress="return keypress(event);" value="<?php echo $edit['Usermember']['birthday']?>"  />
                            </li>
                            <li>
                                <p><span>4) </span>Địa chỉ</p>
                                <input id="address" name="address" type="text" value="<?php echo $edit['Usermember']['address']?>" />
                            </li>
                            <li>
                                <p><span>5) </span>Số điện thoại liên hệ</p>
                                <input id="phone" name="phone" value="<?php echo $edit['Usermember']['phone']?>" type="text" onkeypress="return keypress(event);" />
                            </li>                                                        
                        </ul>
                    </td>
                </tr>
				
				<tr>
					<td colspan="2">
					<p><span>8) </span>Đưa vào 1 ảnh / chứng chỉ / bằng cấp xác nhận</p>
					
					
					Hình ảnh:  <input style="border:1px solid #BBBBBB; width:322px; padding:3px;" type="text" size="50" class="text-input medium-input datepicker" name="userfile" value="<?php echo $edit['Usermember']['avata']?>" readonly="true" > &nbsp;
						 <input type="button" value="Chọn ảnh" class="button" onclick="javascript:OpenPopup('<?php echo DOMAINAD; ?>upload_picnews.php', 'remote');" />
						 	<input type="checkbox" name="" id="checkboxAccept" checked="checked" style="display:none" />
					</td>
				</tr>
		

				<tr>
					<td colspan="2">
						
            <div class="divXacNhan">
			<p><span>9) </span> Mã xác nhận</p>

                        <div class="content-register" id="code-register">
                            <label for="email"><i></i> </label><a1 id="abc">
                                <img alt="" id="captcha" src="<?php echo DOMAIN ?>usermembers/usermembers/create_image" /></a1>&nbsp;&nbsp;<a href="javascript: reload()"><img src="<?php echo DOMAIN ?>images/change-image.gif"/></a>
                        </div>
                        <div class="content-register" id="privacy-register">
                          <br/><input maxlength="5"class="input_text" id="security" class="text-input-register" name="security" />
                        </div> 
						
						
						
                    </div>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<p>
							<input type="submit" value="Sửa thông tin" id="submitDangKy"/>
						</p> 
					</td>
				</tr>
            </table>
     
        </div>
    </form>
        </div>
     </div>
    </div>
	
  </div><!-- End dangky-->
 
</div>
