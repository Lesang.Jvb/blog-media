<?php echo $this->Html->script('jquery.validate', true); ?>
<script type="text/javascript" src="<?php echo DOMAINAD;?>js/ckeditor/ckeditor.js"></script>
<script src="<?php echo DOMAINAD;?>js/ckeditor/sample.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo DOMAINAD;?>js/ckfinder/ckfinder.js"></script>
<script>

function OpenPopup(url, name){
	popupWin = window.open(url, name, 'scrollbars=yes,resizable=yes,width=500,height=300');
}

function reload()
{  
	var random1= Math.random()*5
	jQuery.ajax({
		type: "GET", 
		url: "<?php echo DOMAIN;?>"+'usermembers/usermembers/create_image1/'+random1,
		data: null,
		success: function(msg){	
		jQuery('#abc').find('img').remove().end();
		 jQuery('#abc').append('<img alt="" id="captcha" src="<?php echo DOMAIN?>usermembers/usermembers/create_image1/'+random1+'" />');				
		}
	});	
}

function get_alias(){
	
	var str = (document.getElementById("idtitle").value);
	str= str.toLowerCase();
	str= str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g,"a");
	str= str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g,"e");
	str= str.replace(/ì|í|ị|ỉ|ĩ/g,"i");
	str= str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g,"o");
	str= str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g,"u");
	str= str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g,"y");
	str= str.replace(/đ/g,"d");
	str= str.replace(/!|@|\$|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\'| |\"|\&|\#|\[|\]|~/g,"-");
	str= str.replace(/-+-/g,"-"); //thay thế 2- thành 1-
	str= str.replace(/^\-+|\-+$/g,"");//cắt bỏ ký tự - ở đầu và cuối chuỗi
	document.getElementById("name").value = str;
	return str;
}


jQuery(document).ready(function($) {


	$("#myform").validate({
		rules: {
			name: {
				required: true,
				minlength:8,
				maxlength:100,
			},
			
			userfile: {
				required: true,
			},
			security: {
				required: true,
				minlength: 5,
				maxlength:5
			},
			
		},
		messages: {
			name: {
				required: " <br><span style='color:#FF0000;'>Xin vui lòng nhập vào tên sản phẩm!</span>",
				minlength: " <br><span style='color:#FF0000;'>Tiêu đề bao gồm ít nhất 8 kí tự!</span>",
				maxlength: " <br><span style='color:#FF0000;'>Tiêu đề không được quá 100 ký tự!</span>",
			},
			userfile: {
				required: " <br><span style='color:#FF0000;'>Xin vui lòng up ảnh sản phẩm!</span>",
			},
			security: {
				required: " <br><span style='color:#FF0000;' >Xin vui lòng nhập mã bảo vệ!</span>",
				minlength: "<br><span style='color:#FF0000;' > Mã bảo vệ chỉ bao gồm ít nhất 5 kí tự!</span>",
				maxlength: "<br><span style='color:#FF0000;' > Mã bảo vệ chỉ bao gồm ít nhất 5 kí tự!</span>"
			},
			
				
		
			
		}
	});
	});
	


</script>

<style>
#idtitle{height:20px;}

</style>
<link rel="stylesheet" type="text/css" href="<?php echo DOMAIN ?>usermembers/css/dangnhap.css" />  
<link rel="stylesheet" type="text/css" href="<?php echo DOMAIN ?>usermembers/css/dangtin.css" />  

<?php
echo $this->Html->css(array('phantrang'));
?>


<div id="Content">
    <?php echo $this->element('usermember_left'); ?>

	 <div class="cot2">
        <h2 class="h2_title"> Đăng sản phẩm</h2>
        <div class="col_product">
			

<?php echo $this->Form->create(null, array( 'url' => DOMAIN.'usermembers/login/addproduct','type' => 'post','enctype'=>'multipart/form-data','name'=>'image','id'=>'myform')); ?>
            <table class="input">
                <tr>
                 
					  <td width="30%" nowrap="" height="28" align="left" class="label1">
                        Tên sản phẩm
                        <span class="required_field">(*)</span></td>
					 <td width="70%">
											
						<input name="name" value="<?php if(isset($edit['Product']['name'])) echo $edit['Product']['name']?>" id="idtitle" class="textfield1" maxlength="255" />
                         
                         </td>
                </tr>
				
				<?php echo $this->Form->input('Product.id',array());?>
				<?php echo $this->Form->input('Product.user_id',array('type'=>'hidden'));?>
				
                
				 <tr>
                 
					  <td width="30%" nowrap="" height="28" align="left" class="label1">
                        Thuộc danh mục
                        <span class="required_field">(*)</span></td>
					 <td width="70%">
											
						<select name="data[Product][cat_id]" id="jumpMenu">
                            <?php foreach ($list_cat as $k => $v) { ?>
                            <option value="<?php echo $k; ?>" <?php if(isset($edit['Product']) && $edit['Product']['cat_id']==$k) {echo 'selected="selected"';} ?>><?php echo $v; ?></option>
                            <?php } ?>
                        </select>
                         
                         </td>
                </tr>
				
				
				 <tr>
                 
					  <td width="30%" nowrap="" height="28" align="left" class="label1">
                        Loại sản phẩm
                        <span class="required_field">(*)</span></td>
					 <td width="70%" align="left">
											
						<select name="data[Product][loai]" id="jumpMenu">
                          
                            <option value="Cần bán" <?php if(isset($edit['Product']) && $edit['Product']['loai']=='Cần bán') {echo 'selected="selected"';} ?>>Cần bán</option>
                            <option  <?php if(isset($edit['Product']) && $edit['Product']['loai']=='Cần cho thuê') {echo 'selected="selected"';} ?> value="Cần cho thuê">Cần cho thuê </option>
                           
                        </select>
                         
                         </td>
                </tr>
				
					
					<tr>
                        <td width="30%" nowrap="" height="28" align="left" class="label1">
                        Giá:
                        </td>
                        <td width="70%">
					  
							<?php echo $this->Form->input('Product.pos',array('label'=>'','class'=>'text-input medium-input','maxlength' => '250','id' => 'idtitle'));?>
               
							
                         </td>
						 
                    </tr>
				
				<tr>
                 
					  <td width="30%" nowrap="" height="28" align="left" class="label1">
                        Vị trí:
                        </td>
					
					  <td width="70%">
							<?php echo $this->Form->input('Product.pos',array('label'=>'','class'=>'text-input medium-input','maxlength' => '250','id' => 'idtitle'));?>
                         </td>		
					
				
                         
                </tr>
			

			
                <?php $user=$this->Session->read('user');
				
				if($user['Usermember']['role']==1) { ?>
                <tr>
                    <td class="label">Trang thái:</td>
                    <td><input type="radio" <?php if(isset($edit) && $edit['Product']['status']==0) echo 'checked="checked"';?> value="0" id="ProductStatus0" name="data[Product][status]">
                        Chưa Active a
                        &nbsp;&nbsp;&nbsp;
                        <input type="radio"  <?php if(isset($edit) && $edit['Product']['status']==1) echo 'checked="checked"';?> value="1" id="ProductStatus1" name="data[Product][status]">
                        Đã Active </td>
                </tr>
				<?php }?>
				
				<tr>
                   
					 <td width="30%" nowrap="" height="28" align="left" class="label1">
                        Trang thái gian hàng:
                        </td>
                    <td><input type="radio" <?php if(isset($edit) && $edit['Product']['user_status']==0) echo 'checked="checked"';?> value="0" id="ProductStatus0" name="data[Product][user_status]">
                        Chưa Active a
                        &nbsp;&nbsp;&nbsp;
                        <input type="radio"  <?php if(!isset($edit) || $edit['Product']['user_status']==1) echo 'checked="checked"';?> value="1" id="ProductStatus1" name="data[Product][user_status]">
                        Đã Active </td>
                </tr>
				
                <tr>
                    <td width="30%" nowrap="" height="28" align="left" class="label1">Hình ảnh đại diện:</td>
                    <td width="70%">					
						<input  value="<?php if(isset($edit['Product']['images'])) echo $edit['Product']['images']?>" style="border:1px solid #BBBBBB; width:322px; padding:3px;" type="text" size="50" class="text-input medium-input datepicker" name="userfile" readonly="true" > &nbsp;						
						<input type="button" value="Chọn ảnh" class="button" onclick="javascript:OpenPopup('<?php echo DOMAINAD; ?>upload_picnews.php?id=<?php echo $city_id?>', 'remote');" />
                    </td>
                </tr>
				
                <tr>
                    <td width="30%" nowrap="" height="28" align="left" class="label1">Giới thiệu sản phẩm</td>
                    <td style="widht:100%"  colspan="2">
                         <?php
                            $CKEditor = new CKEditor();
                            $CKEditor->config['width'] = '98%';
                            $CKEditor->config['height'] = '150';
                            CKFinder::SetupCKEditor( $CKEditor ) ;
                            
                            $initialValue = isset($edit['Product']['shortdes'])?$edit['Product']['shortdes']:"";
                            echo $CKEditor->editor("data[Product][shortdes]", $initialValue, "compact");
                        ?>
                       </td>
                </tr>                 
                <tr>
                    <td width="30%" nowrap="" height="28" align="left" class="label1">Chi tiết sản phẩm</td>
                    <td style="widht:100%"  colspan="2">
                         <?php
                            $CKEditor = new CKEditor();
                            $CKEditor->config['width'] = '98%';
                            $CKEditor->config['height'] = '150';
                            CKFinder::SetupCKEditor( $CKEditor ) ;
                            
                            $initialValue = isset($edit['Product']['content'])?$edit['Product']['content']:"";
                            echo $CKEditor->editor("data[Product][content]", $initialValue, "compact");
                        ?>
                    </td>
                </tr>
  <tr>
					<td width="30%" nowrap="" height="28" align="left" class="label1">
                        Mã bảo mật 
                      </td>
						<td>
			   <a1 id="abc">
               <img alt="" id="captcha" src="<?php echo DOMAIN?>usermembers/usermembers/create_image" /></a1>&nbsp;&nbsp;
			   <a href="javascript: reload()"><img src="<?php echo DOMAIN?>images/change-image.gif"/></a>
			   </td></tr>
			   <tr>
          
						<td width="30%" nowrap="" height="28" align="left" class="label1">
                        Nhập mã bảo mật <span class="required_field">(*)</span>
                      </td>
				<td>
				<input id="security" maxlength="5" class="text-input-register" name="security" style="padding:3px" />
            </td> 
					  
		</tr>
				
			
			  <tr>
                        <td width="100%" height="50" align="center" colspan="2">
						<input type="submit" name="B1" value="Xong"></td>
                      </tr>
              
			
            </table>
			<?php echo $this->Form->end(); ?>
           
        </div>



    </div><!-- End dangky-->
	
</div>