<link rel="stylesheet" type="text/css" href="<?php echo DOMAIN ?>usermembers/css/dangnhap.css" />  

<?php
echo $this->Html->css(array('phantrang'));
?>

<script>
    function confirmDelete(delUrl)
    {
        if (confirm("Bạn có chắc chắn xóa tin rao vặt này không!"))
        {
            document.location = delUrl;
        }
    }
</script>

<style>
    .tb_tin tr td{border: 1px solid #cccccc;}
    .tb_tin tr td a{color: #0000FF}
    .tbTitle{text-align: center; font-size: 14px; font-weight: bold; color: }
    #uploadcontent {
        color: #333333;
        height: 20px;
        float:right;
        width: 372px;
    }
    #uploadcontent a {
        color: #258294;
        text-decoration: none;
    }

    #birthday option{
        padding-left:5px;
    }
    #sex{
        border: 1px solid #898989;
        color: #333333;
        height: 20px;
        width: 150px;	
    }	
    #images{
        border: 1px solid #898989;
        color: #333333;
        height: 20px;
        width: 245px;	
    }	
    .tb_tin td{
        padding:5px;
    }
    .h2_title span{color: yellow;}
    .proccess{text-align: center;}
</style>

<div id="Content">
    <?php echo $this->element('usermember_left'); ?>

    <div class="cot2">
        <h2 class="h2_title"> <?php echo $title_product ?></h2>
        <div class="col_product">
            <div class="text-main">
                <table style="width:100%" class="tb_tin">
                    <tr class="tbTitle">
                        <td width="100px">Ảnh đại diện</td>
                        <td>Tên </td>                        
                        <td>phone</td>
                        <td>email</td>
                        <td>Thao tác</td>
                        <td>Quyền cũ</td>
                        <td>Nâng cấp</td>
                    </tr>
                    <?php foreach ($listThanhVien as $listThanhVien) { ?>
                        <tr style="background-color: #DEDEDE;">
                            <td>                                
                                <img src="<?php echo DOMAINAD . $listThanhVien['Usermember']['avata']; ?>" width="100" />
                            </td>
                            <td>                                 
                                <a href="<?php echo DOMAIN . "chi-tiet-don-vi-cap1/" . $listThanhVien['Usermember']['id'] ?>"><?php echo $listThanhVien['Usermember']['name']; ?></a>
                            </td>                                                         
                            <td>                                
                                <p><?php echo $listThanhVien['Usermember']['phone']; ?></p>
                            </td>                            
                            <td><p>(<?php echo $listThanhVien['Usermember']['email']; ?>)</p></td>
                            <td class="proccess">                                
                                <?php if ($listThanhVien['Usermember']['status'] == 1) { ?>
                                    <a href="<?php echo DOMAIN . "usermembers/login/close_usermember/" . $listThanhVien['Usermember']['id'] ?>"><img src="<?php echo DOMAINAD ?>images/icons/success-icon.png" /></a>
                                <?php } else { ?>
                                    <a href="<?php echo DOMAIN . "usermembers/login/active_usermember/" . $listThanhVien['Usermember']['id'] ?>"><img src="<?php echo DOMAINAD ?>images/icons/Play-icon.png" /></a>                                        
                                <?php } ?>                                
                                <a href="<?php echo DOMAIN . "usermembers/login/delete_usermember/" . $listThanhVien['Usermember']['id'] ?>"><img src="<?php echo DOMAINAD ?>images/icons/cross.png" /></a>                                
                            </td>
							<td>
							<?php if($listThanhVien['Usermember']['role_old']==null) echo 'Đăng ký lần đầu';
							elseif($listThanhVien['Usermember']['role_old']==2 && $listThanhVien['Usermember']['parent_id']==null ) echo 'Đơn vị cha'; elseif($listThanhVien['Usermember']['role_old']==2 && $listThanhVien['Usermember']['parent_id']!=null ) echo 'Đơn vị con của '.$this->requestAction('Usermember/get_name_usermember/'.$listThanhVien['Usermember']['parent_id']);
							elseif($listThanhVien['Usermember']['role_old']==3) echo 'Thành viên thuộc đơn vị '.$this->requestAction('Usermember/get_name_usermember/'.$listThanhVien['Usermember']['parent_id']);
							elseif($listThanhVien['Usermember']['role_old']==4)		echo 'Thành viên thường'; ?>
							</td>
							<td align="center">
								
                                                           
                                <a href="<?php echo DOMAINAD ?>usermembers/login/close_nangcap/<?php echo $listThanhVien['Usermember']['id']; ?>" title="Đóng" class="icon-4 info-tooltip"><img src="<?php echo DOMAINAD ?>images/icons/success-icon.png" alt="Không cho nâng cấp" /></a>
                              
							</td>
                     <?php } ?>
                    <tr>
                        <td class="tbTitle" colspan="7">
                            <?php echo $this->Paginator->numbers(); ?>
                        </td>
                    </tr>
                </table>

            </div>
        </div>



    </div><!-- End dangky-->





</div>
