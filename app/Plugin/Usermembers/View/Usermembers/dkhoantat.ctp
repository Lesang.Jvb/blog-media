<link rel="stylesheet" type="text/css" href="<?php echo DOMAIN ?>usermembers/css/dkquantrikhuvuc.css" />  

<!-- Begin bottom-column -->
<div id="bottom-column" class="clearfix">
    <div class="title-bottom">
        <h3>ĐĂNG KÝ HOÀN TẤT</h3>        
    </div>
    <form id="quanTriKhuVuc" action="<?php echo DOMAIN . "dang-ky-quan-tri-khu-vuc.html" ?>" method="POST">    
        <div class="main-bottom"> 
            <p>Việc đăng ký sẽ hoàn tất sau khi bạn được người quản trị website kích hoạt tài khoản.</p>
            <p>Chúng tôi sẽ cố gắng kiểm tra điều kiện đăng ký của bạn và thông báo cho bạn trong thời gian sớm nhất</p>
            <p>Bạn có thể liên hệ với chúng tôi qua phungvanthuy@gmail.com sdt: 0987654321</p>
        </div>
    </form>

    <!-- End main -->
</div>
<!-- End bottom -->  


