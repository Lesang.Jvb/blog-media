<link rel="stylesheet" type="text/css" href="<?php echo DOMAIN ?>usermembers/css/dkquantrikhuvuc.css" />  
<link rel="stylesheet" type="text/css" href="<?php echo DOMAIN ?>usermembers/css/mystyle.css" />  

<?php echo $this->Html->script('jquery.validate', true); ?>
<script type="text/javascript" src="<?php echo DOMAIN?>Usermembers/js/my_script.js"></script> 


<!-- Begin bottom-column -->
<div id="bottom-column" class="clearfix">
    <div class="title-bottom">
        <h3>ĐĂNG KÝ THÀNH VIÊN QUẢN TRỊ KHU VỰC</h3>        
    </div>
    <form id="quanTriKhuVuc" action="<?php echo DOMAIN . "usermembers/usermembers/add" ?>" method="POST" name="image" onsubmit="return test();">    
        <div class="main-bottom"> 
            <table>
                <tr>
                    <td width="40%">
                        <p><span>1) </span>Chọn khu vực quản trị</p>
                        <div id="selectKhuVuc">
                            <p>TỈNH / THÀNH PHỐ</p>
                            <select  name="data[Usermember][city_id]" size="10">                                
                                <?php $ok = 0; foreach ($city as $key => $citys) { ?> 
                                    <option <?php  if($ok == 0){echo "selected";} $ok = 1; ?> value="<?php echo $key ?>"><?php echo $citys; ?></option>
                                <?php } ?>                                
                            </select>                                                        
                        </div>
						<div id="name">
                            <p><span>7) </span>Họ tên</p>
                            <input class="input" name="name" type="text" />
                        </div>
                        <div id="soCMND">
                            <p><span>8) </span>Số CMND</p>
                            <input class="input" name="cmnd" maxlength="9" type="text" onkeypress="return keypress(event);" />
                        </div>
						   
						 
                        </div>
						
						<input type="hidden" value="1" name="data[Usermember][role]"/>
                    </td>
                    <td>
                        <ul>
                            <li>
                                <p><span>2) </span>Email đăng ký</p>
                                <input id="email" name="email" type="text" />
								<div id="validate-emai-register"><span id="error"></span></div>
                            </li>
                            <li>
                                <p><span>3) </span>Mật khẩu</p>
                                <input id="password1" name="password" type="password" />
                            </li>
                            <li>
                                <p><span>4) </span>Xác nhận mật khẩu</p>
                                <input id="confirm_password" name="confirm_password" type="password" />
                            </li>
                            <li>
                                <p><span>5) </span>Năm sinh</p>
                                <input id="birthday" name="birthday" type="text" onkeypress="return keypress(event);"  />
                            </li>
                            <li>
                                <p><span>6) </span>Địa chỉ</p>
                                <input id="address" name="address" type="text" />
                            </li>
                            <li>
                                <p><span>9) </span>Số điện thoại liên hệ</p>
                                <input id="phone" name="phone" type="text" onkeypress="return keypress(event);" />
                            </li>                                                        
                        </ul>
                    </td>
                </tr>
				
				<tr>
					<td colspan="2">
					<p><span>10) </span>Đưa vào 1 ảnh / chứng chỉ / bằng cấp xác nhận</p>
					
					
					Hình ảnh:  <input style="border:1px solid #BBBBBB; width:322px; padding:3px;" type="text" size="50" class="text-input medium-input datepicker" name="userfile" readonly="true" > &nbsp;
						 <input type="button" value="Chọn ảnh" class="button" onclick="javascript:OpenPopup('<?php echo DOMAINAD; ?>upload_picnews.php', 'remote');" />
					</td>
				</tr>
			<tr>
				<td colspan="2">
						<div class="divXacNhan">
              
				<p id="bottomXacNhanDongY"><span>11) </span>Điều khoản</p>
				<div class="div_quydinh">
				   
					<?php $setting=$this->requestAction('comment/setting');
					
					echo $setting['Setting']['regulations_vie'];
					?>
				
				<p id="note"><input type="checkbox" name="" id="checkboxAccept"  /> Tôi đồng ý với điều khoản và quy tắc</a></p>
				
				
				</div>
				
				   <!--        
                <input type="button" id="xacNhanDongY" value="Xác nhận đồng ý" />            
                <input name="accept" value="0" type="hidden" id="accept" />                 -->
                <div class="clear"></div>
				
            </div>
				</td>
			</tr>	

				<tr>
					<td colspan="2">
						
            <div class="divXacNhan">
			<p><span>12) </span> Mã xác nhận</p>

                        <div class="content-register" id="code-register">
                            <label for="email"><i></i> </label><a1 id="abc">
                                <img alt="" id="captcha" src="<?php echo DOMAIN ?>usermembers/usermembers/create_image" /></a1>&nbsp;&nbsp;<a href="javascript: reload()"><img src="<?php echo DOMAIN ?>images/change-image.gif"/></a>
                        </div>
                        <div class="content-register" id="privacy-register">
                          <br/><input maxlength="5"class="input_text" id="security" class="text-input-register" name="security" />
                        </div> 
						
						
						
                    </div>
					</td>
				</tr>
            </table>
         
            
			
		
			
			
			<!--
            <p><span>Lưu ý</span>
            <p>1. Các đăng ký từ 1 đến 7 là bắt buộc. Mỗi lần thay đổi nội dung đăng ký từ 1-7 phải được quản trị viên kích hoạt thay đổi.</p>
            <p>2. Cần liên hệ với thành viên quản trị website để được xác nhận thông tin cá nhân trước khi được kích hoạt trở thành thành viên quản trị</p>
			--><p>
            <input type="submit" value="Đăng ký" id="submitDangKy"/>
            </p> 
        </div>
    </form>

    <!-- End main -->
</div>
<!-- End bottom -->  


