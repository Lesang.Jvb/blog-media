
<link rel="stylesheet" type="text/css" href="<?php echo DOMAIN ?>usermembers/css/danhsachgianhang.css" />  

<div class="clearfix" id="bottom-column">
    <div class="title-bottom">
        <h3>Tin tức từ quản trị</h3>
        <!--<span><a href="http://develop02.vtmgroup.com.vn/khongten/danh-sach-tin-tu-quan-tri.html">Xem tất cả</a><img align="absmiddle" src="http://develop02.vtmgroup.com.vn/khongten/images/arrow2.png"></span> --></div>
    <div class="main-bottom">
        <!-- begin list news -->
            <?php foreach($news as $newss){?> 
				<div class="list-news"> <span>Đăng vào lúc <?php echo date('h:m:s', strtotime($newss["New"]["created"])); ?> ngày <?php echo date('d-m-Y', strtotime($newss["New"]["created"])); ?></span>
					<div class="img-news"><img src="<?php echo DOMAINAD.$newss["New"]["images"] ?>"></div>
					<div class="title-news"> <a href="<?php echo DOMAIN."chi-tiet-tin/".$newss["New"]["slug"] ?>"><?php echo $newss["New"]["name"] ?></a> </div>
					<div class="text-news">
					<?php echo $newss["New"]["shortdes"] ?>							
					</div>
				</div>
			<?php } ?> 
        <!-- End list news -->
    </div>
    <!-- End main -->
</div>


<table id="tableGianhang">
	<tr>
		<td colspan="5" class="title">Danh sách các gian hàng tại <?php echo "Hà Nội";//$khuvuc ?></td>		
	</tr>
	<tr class="titleContent">
		<td>stt<td>
		<td>Ảnh đại diện</td>
		<td>Thông tin gian hàng</td>		
		<td>Liên hệ và hỗ trợ</td>
	</tr>
	<?php foreach($usermenber as $key=>$usermenbers){?>
	<tr class="contentDanhSachGianHang">
		<td><?php echo $key ?><td>
		<td><img height="80" src="<?php echo DOMAINAD.$usermenbers["Usermember"]["images"] ?>" maxheight="80" maxwidth="80"></td>
		<td>
			<a href="<?php echo DOMAIN."gian-hang/".$usermenbers["Usermember"]["gianhang"] ?>"><p class="nameCty"><?php echo $usermenbers["Usermember"]["company"] ?></p></a>
			<p class="address"><?php echo $usermenbers["Usermember"]["address"]."-".$usermenbers["Usermember"]["diachi"] ?></p>
			<p class="countProduct">Có ~<?php echo $usermenbers["Usermember"]["countProduct"]; ?> sản phẩm</p>
		</td>		
		<td>
			<p class="phone">Điện thoại : <?php echo $usermenbers["Usermember"]["phone"]; ?></p>
			<p>Email liên hệ: <?php echo $usermenbers["Usermember"]["email"]; ?></p>
			<!--p class="website"><a href="#">www.vienthinh.vn</a></p-->
		</td>
	</tr>		
	<?php } ?>	
	<tr>
		<td colspan="5">
			<div class="pagination">
				<?php
					echo $this->Paginator->first('« Đầu ', null, null, array('class' => 'disabled'));     
					echo $this->Paginator->prev('« Trước ', null, null, array('class' => 'disabled')); 
					echo $this->Paginator->numbers()." ";
					echo $this->Paginator->next(' Tiếp »', null, null, array('class' => 'disabled')); 
					echo $this->Paginator->last('« Cuối ', null, null, array('class' => 'disabled')); 
					echo " Page ".$this->Paginator->counter();
				?>
			</div>
		</td>
	</tr>
</table>