
<link rel="stylesheet" type="text/css" href="<?php echo DOMAIN ?>usermembers/css/mystyle.css" />  

<?php echo $this->Html->script('jquery.validate', true); ?>
<script type="text/javascript" src="<?php echo DOMAIN ?>Usermembers/js/my_script.js"></script> 
<script>
    $(function(){
        $('#chinh').change(function(){

            var id=$('#chinh').val();
            //alert(id);
            $.ajax({
                type: "POST", 
                url: "<?php echo DOMAIN; ?>"+'get_donvi',
                data: {cat: id},
                //dataType: "json",
                success: function(msg){	                    
                    var str=$.parseJSON(msg);                     
                    var ok=0;
                    $("#con").html("");
                    $chuoi="<select  name='data[Usermember][parent_id]' size='10' > ";
                    $.each(str,function(i,chuoi){                        
                        if(chuoi['id']!='') {
                            $chuoi+='<option value="'+chuoi['id']+'">'+chuoi['name']+'</option>';                                 
                        }			                                                
                        //$chuoi+="</select>";
                        
                        $("#con").html($chuoi);
                    });
                }
            });	

        });
	

    })
</script>
<!-- Begin bottom-column -->
<div id="bottom-column" class="clearfix">
    <div class="title-bottom">
        <h3>ĐĂNG KÝ THÔNG TIN THÀNH VIÊN THUỘC ĐƠN VỊ</h3>                                        
    </div>

    <form id="quanTriKhuVuc" action="<?php echo DOMAIN . "usermembers/usermembers/add" ?><?php if(isset($edit)) echo '/1';?>" method="POST" name="image" onsubmit="return test();">  

<!--form action="<?php echo DOMAIN ?>dang-ky-thanh-vien-don-vi.html" method="POST"--> 
        <div class="main-bottom"> 

            <table>
                <tr>
                    <td  width="50%">
                        <p><span><?php $dem=1; echo $dem++?>) </span>Chọn khu vực</p>

                        <div id="selectKhuVuc">
                            <p>TỈNH / THÀNH PHỐ</p>
                            <select  name="data[Usermember][city_id]" size="10" id="chinh">                                
                                <?php $ok = 0;
                                foreach ($city as $key => $citys) {
                                    ?> 
                                    <option <?php
                                if ((!isset($city)&&$ok == 0) || (isset($edit) && $edit['Usermember']['city_id']==$key)) {
                                    echo "selected";
                                } $ok = 1;
                                ?> value="<?php echo $key ?>"><?php echo $citys; ?></option>
<?php } ?>                                
                            </select>                                                        
                        </div>                                                 


                        

                    </td>
                    <td  width="50%">
                        <p><span><?php echo $dem++?>) </span>Chọn đơn vị thuộc khu vực</p>

                        <div id="selectKhuVuc">
                            <p>TÊN ĐƠN VỊ</p>
                            <div id="con">
                                <select  name='data[Usermember][parent_id]' size='10' >
								<?php $i=0;
                                foreach ($donvi as $key => $citys) { $i++;
                                    ?> 
                                    <option <?php if((!isset($edit) && $i==1) ||(isset($edit['Usermember'])&& $edit['Usermember']['parent_id']==$key )) echo 'selected="selected"'?> value="<?php echo $key ?>"><?php echo $citys; ?></option>
								<?php } ?>                                
                                </select>                
                                                      
                            </div>                                                 
                        </div>

                        <input type="hidden" value="3" name="data[Usermember][role]"/>
                        <input type="hidden" value="<?php if(isset($edit) && $edit['Usermember']['role']!=3) echo $edit['Usermember']['role']?>" name="data[Usermember][role_old]"/>
                        <input type="hidden" value="<?php if(isset($edit)) echo $edit['Usermember']['id']?>" name="data[Usermember][id]"/>

                    </td>

                </tr>


                <tr>
                    <td>
                        <p><span><?php echo $dem++?>) </span>Họ tên</p>
                        <input name="name" id="name" value="<?php if(isset($edit)) echo $edit['Usermember']['name']?>" type="text" class="input"/>
                    </td> 

                    <td width="316">
                        <p><span><?php echo $dem++?>) </span> Email đăng ký</p>
                        <input id="email" name="email" value="<?php if(isset($edit)) echo $edit['Usermember']['email']?>" <?php if(isset($edit)) echo 'readonly="true"'?> type="text" class="input"/>
                        <div id="validate-emai-register"><span id="error"></span></div>
                    </td>

                </tr>
				<?php if( !isset($edit)) {?>
                <tr>
                    <td>
                        <p><span><?php echo $dem++?>) </span>Mật khẩu</p>
                        <input name="password" id="password1" type="password" class="input"/>
                    </td> 

                    <td>
                        <p><span><?php echo $dem++?>) </span>Xác nhận mật khẩu</p>
                        <input name="confirm_password" id="confirm_password" type="password" class="input"/>
                    </td>                                                    
                </tr>
				<?php } ?>
				
                <tr>
                    <td>
                        <p><span><?php echo $dem++?>) </span> Năm sinh</p>
                        <input name="birthday" id="birthday" value="<?php if(isset($edit)) echo $edit['Usermember']['birthday']?>" type="text" class="input"/>
                    </td>

                    <td>
                        <p><span><?php echo $dem++?>) </span>Số CMND</p>
                        <input name="cmnd" id="cmnd" value="<?php if(isset($edit)) echo $edit['Usermember']['cmnd']?>" class="input" type="text" onkeypress="return keypress(event);" />
                    </td>                                                    
                </tr>
                <tr>

                    <td>
                        <p><span><?php echo $dem++?>) </span> Số/ tên phòng</p>
                        <input name="data[Usermember][tensophong]" id="tensophong" class="input" onkeypress="return keypress(event);" value="<?php if(isset($edit)) echo $edit['Usermember']['tensophong']?>"  type="text"/>
                    </td>

                    <td>
                        <p><span><?php echo $dem++?>) </span> Số điện thoại liên hệ</p>
                        <input name="phone" id="phone" value="<?php if(isset($edit)) echo $edit['Usermember']['phone']?>" class="input" type="text" onkeypress="return keypress(event);" />
                    </td>


                </tr>
                <tr>
                    <td>
                        <p><span><?php echo $dem++?>) </span>Số tài khoản</p>
                        <input name="data[Usermember][taikhoan]" class="input" id="taikhoan" type="text" onkeypress="return keypress(event);" value="<?php if(isset($edit)) echo $edit['Usermember']['taikhoan']?>" />
                    </td>  

                    <td>
                        <p><span><?php echo $dem++?>) </span> Chi nhánh/địa điểm mở tài khoản</p>
                        <input name="data[Usermember][subbank]" class="input" id="subbank" type="text"   value="<?php if(isset($edit)) echo $edit['Usermember']['subbank']?>" />
                    </td>

                </tr>

                <tr>	
                    <td>
                        <p><span><?php echo $dem++?>) </span>Ngân hàng</p>
                        <input name="data[Usermember][bank]" value="<?php if(isset($edit)) echo $edit['Usermember']['bank']?>" id="bank" type="text" class="input"/>
                    </td>  
                    <td></td>										
                </tr>


                <tr>
                    <td colspan="2">
                        <p><span><?php echo $dem++?>) </span> Các thông tin năng lực của tổ chức cá nhân (tối đa 500 từ)</p>
                        <textarea name="data[Usermember][info]" class="textarea" maxlength="500">
						<?php if(isset($edit)) echo $edit['Usermember']['info']?>
						</textarea>
                    </td>
                </tr>

                <tr>
                    <td colspan="2">
                        <p><span><?php echo $dem++?>) </span>Đưa vào 1 ảnh / chứng chỉ / bằng cấp xác nhận</p>


                        Hình ảnh:  <input style="border:1px solid #BBBBBB; width:322px; padding:3px;" type="text" size="50" value="<?php if(isset($edit)) echo $edit['Usermember']['avata']?>" class="text-input medium-input datepicker" name="userfile" readonly="true" > &nbsp;
                        <input type="button" value="Chọn ảnh" class="button" onclick="javascript:OpenPopup('<?php echo DOMAINAD; ?>upload_picnews.php', 'remote');" />
                    </td>
                </tr>
			<?php if(!isset($edit)) { ?>
                <tr>
                    <td colspan="2">

                        <div class="divXacNhan">

                            <p id="bottomXacNhanDongY"><span><?php echo $dem++?>) </span>Điều khoản</p>
                            <div class="div_quydinh">

                                <?php
                                $setting = $this->requestAction('comment/setting');

                                echo $setting['Setting']['regulations_vie'];
                                ?>

                                <p id="note"><input type="checkbox" name="" id="checkboxAccept"  /> Tôi đồng ý với điều khoản và quy tắc</a></p>


                            </div>

                            <div class="clear"></div>

                        </div>


                    </td>
                </tr>
				<?php } else { ?>
				<input type="checkbox" name="" id="checkboxAccept" checked="checked"  style="display:none" /> 
				<?php }
				?>


                <tr>
                    <td colspan="2">
                        <div class="divXacNhan" style="">
                            <p><span><?php echo $dem++?>) </span> Mã xác nhận</p>

                            <div class="content-register" id="code-register">
                                <label for="email"><i></i> </label><a1 id="abc">
                                    <img alt="" id="captcha" src="<?php echo DOMAIN ?>usermembers/usermembers/create_image" /></a1>&nbsp;&nbsp;<a href="javascript: reload()"><img src="<?php echo DOMAIN ?>images/change-image.gif"/></a>
                            </div>
                            <div class="content-register" id="privacy-register">
                                <br/><input style="" maxlength="5"class="input_text" id="security" class="text-input-register" name="security" />
                            </div> 



                        </div>

                    </td>
                </tr>


            </table>
			<p>
                <input type="submit" value="<?php if(!isset($edit)) echo 'Đăng ký'; else echo 'Sửa thông tin';?>" id="submitDangKy"/>
            </p> 


        </div>
    </form>
    <!-- End main -->
</div>
<!-- End bottom -->                                    