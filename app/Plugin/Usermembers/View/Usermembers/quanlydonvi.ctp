<link rel="stylesheet" type="text/css" href="<?php echo DOMAIN ?>usermembers/css/dangnhap.css" />  

<?php
echo $this->Html->css(array('phantrang'));
?>

<script>
    function confirmDelete(delUrl)
    {
        if (confirm("Bạn có chắc chắn xóa tin rao vặt này không!"))
        {
            document.location = delUrl;
        }
    }
</script>

<style>
    .tb_tin tr td{border: 1px solid #cccccc;}
    .tb_tin tr td a{color: #0000FF}
    .tbTitle{text-align: center; font-size: 14px; font-weight: bold; color: }
    #uploadcontent {
        color: #333333;
        height: 20px;
        float:right;
        width: 372px;
    }
    #uploadcontent a {
        color: #258294;
        text-decoration: none;
    }

    #birthday option{
        padding-left:5px;
    }
    #sex{
        border: 1px solid #898989;
        color: #333333;
        height: 20px;
        width: 150px;	
    }	
    #images{
        border: 1px solid #898989;
        color: #333333;
        height: 20px;
        width: 245px;	
    }	
    .tb_tin td{
        padding:5px;
    }
    .h2_title span{color: yellow;} 
    .proccess{text-align: center;}
</style>

<div id="Content">
    <?php echo $this->element('usermember_left'); ?>

    <div class="cot2">
        <h2 class="h2_title"> <?php echo $title_product ?> (<span><?php echo $view["Usermember"]["company"]; ?></span>)</h2>
        <div class="col_product">
            <div class="text-main">                
                <table style="width:100%" class="tb_tin">                    
                    <tr><td>Ảnh đại diện</td><td><img src="<?php echo DOMAINAD.$view["Usermember"]["avata"];?>" width="100" /></td></tr>
                    <tr><td>Người quản trị</td><td><?php echo $view["Usermember"]["name"]; ?></td></tr>
                    <tr><td>Email</td><td><?php echo $view["Usermember"]["email"]; ?></td></tr>
                    <tr><td>Số điện thoại</td><td><?php echo $view["Usermember"]["phone"]; ?></td></tr>
                    <tr><td>Năm sinh</td><td><?php echo $view["Usermember"]["birthday"]; ?></td></tr>
                    <tr><td>Số CMND</td><td><?php echo $view["Usermember"]["cmnd"]; ?></td></tr>
                    <tr><td>Địa chỉ</td><td><?php echo $view["Usermember"]["address"]; ?></td></tr>                    
                    </tr>                        
                </table>

            </div>
        </div>
    </div><!-- End dangky-->
</div>
