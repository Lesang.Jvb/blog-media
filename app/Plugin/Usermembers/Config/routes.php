<?php
Router::connect(
    '/dang-ky-thanh-vien.html',
    array(
        'plugin'=>'Usermembers',
        'controller' => 'usermembers',
        'action' => 'index'
    )
);
Router::connect(
    '/dang-ky-quan-tri-khu-vuc.html',
    array(
        'plugin'=>'Usermembers',
        'controller' => 'usermembers',
        'action' => 'dkquantrikhuvuc'
    )
);
Router::connect(
    '/dang-ky-hoan-tat.html',
    array(
        'plugin'=>'Usermembers',
        'controller' => 'usermembers',
        'action' => 'dkhoantat'
    )
);
Router::connect(
    '/dang-ky-mo-gian-hang.html',
    array(
        'plugin'=>'Usermembers',
        'controller' => 'usermembers',
        'action' => 'dkmogianhang'
    )
);

Router::connect(
    '/dang-ky-don-vi-cap1.html',
    array(
        'plugin'=>'Usermembers',
        'controller' => 'usermembers',
        'action' => 'dkdonvicap1'
    )
);

Router::connect(
    '/dang-ky-thanh-vien-thuong.html',
    array(
        'plugin'=>'Usermembers',
        'controller' => 'usermembers',
        'action' => 'dkthanhvienthuong'
    )
);

Router::connect(
    '/creatcaptcha',
    array(
        'plugin'=>'Usermembers',
        'controller' => 'usermembers',
        'action' => 'creatcaptcha'
    )
);


Router::connect(
    '/getsecuritycode',
    array(
        'plugin'=>'Usermembers',
        'controller' => 'usermembers',
        'action' => 'getsecuritycode'
    )
);

Router::connect(
    '/dang-ky-thanh-vien-don-vi.html',
    array(
        'plugin'=>'Usermembers',
        'controller' => 'usermembers',
        'action' => 'dkthanhvienthuocdonvi'
    )
);

Router::connect(
    '/quan-tri.html',
    array(
        'plugin'=>'usermembers',
        'controller' => 'login',
        'action' => 'quantri'
    )
);

Router::connect(
    '/login.html',
    array(
        'plugin'=>'Usermembers',
        'controller' => 'usermembers',
        'action' => 'login'
    )
);

Router::connect(
    '/checkemail',
    array(
        'plugin'=>'Usermembers',
        'controller' => 'usermembers',
        'action' => 'checkemail'
    )
);

Router::connect(
    '/activeusermemberthanhvien/*',
    array(
        'plugin'=>'Usermembers',
        'controller' => 'login',
        'action' => 'activeusermemberthanhvien'
    )
);
Router::connect(
    '/closeusermemberthanhvien/*',
    array(
        'plugin'=>'Usermembers',
        'controller' => 'login',
        'action' => 'closeusermemberthanhvien'
    )
);

Router::connect(
    '/deleteusermemberthanhvien/*',
    array(
        'plugin'=>'Usermembers',
        'controller' => 'login',
        'action' => 'deleteusermemberthanhvien'
    )
);
Router::connect(
    '/chi-tiet-don-vi-cap1/*',
    array(
        'plugin'=>'Usermembers',
        'controller' => 'usermembers',
        'action' => 'viewdetail'
    )
);
Router::connect(
    '/listusermember',
    array(
        'plugin'=>'Usermembers',
        'controller' => 'login',
        'action' => 'listusermember'
    )
);


Router::connect(
    '/get_donvi',
    array(
        'plugin'=>'Usermembers',
        'controller' => 'usermembers',
        'action' => 'get_donvi'
    )
);

Router::connect(
    '/activeusermember/*',
    array(
        'plugin'=>'Usermembers',
        'controller' => 'login',
        'action' => 'active_usermember'
    )
);
Router::connect(
    '/closeusermember/*',
    array(
        'plugin'=>'Usermembers',
        'controller' => 'login',
        'action' => 'close_usermember'
    )
);
Router::connect(
    '/deleteusermember/*',
    array(
        'plugin'=>'Usermembers',
        'controller' => 'login',
        'action' => 'delete_usermember'
    )
);
Router::connect(
    '/thoat',
    array(
        'plugin'=>'Usermembers',
        'controller' => 'usermembers',
        'action' => 'logout'
    )
);


Router::connect(
    '/qualydonvi.html',
    array(
        'plugin'=>'Usermembers',
        'controller' => 'usermembers',
        'action' => 'quanlydonvi'
    )
);

Router::connect(
    '/danh-sach-gian-hang/*',
    array(
        'plugin'=>'Usermembers',
        'controller' => 'usermembers',
        'action' => 'danhsachgianhang'
    )
);

Router::connect(
    '/gian-hang/*',
    array(
        'plugin'=>'Usermembers',
        'controller' => 'usermembers',
        'action' => 'gianhang'
    )
);

Router::connect('/thong-tin-ca-nhan.html', array('plugin'=>'Usermembers','controller' => 'login', 'action' => 'thongtincanhan'));

Router::connect('/thong-tin-ca-nhan/*', array('plugin'=>'Usermembers','controller' => 'login', 'action' => 'thongtincanhan'));

Router::connect('/doi-mat-khau.html', array('plugin'=>'Usermembers','controller' => 'login', 'action' => 'doimatkhau'));

Router::connect('/danh-sach-gian-hang.html', array('plugin'=>'Usermembers','controller' => 'login', 'action' => 'dsgianhang'));

Router::connect('/danh-sach-tin.html', array('plugin'=>'Usermembers','controller' => 'login', 'action' => 'dstin'));
Router::connect('/san-pham.html', array('plugin'=>'Usermembers','controller' => 'login', 'action' => 'listproduct1'));

Router::connect('/san-pham-gian-hang.html', array('plugin'=>'Usermembers','controller' => 'login', 'action' => 'listproduct'));

Router::connect('/them-san-pham.html', array('plugin'=>'Usermembers','controller' => 'login', 'action' => 'addproduct'));

Router::connect('/danh-sach-tin-thanh-vien.html', array('plugin'=>'Usermembers','controller' => 'login', 'action' => 'dstinthanhvien'));

Router::connect('/dang-tin.html', array('plugin'=>'Usermembers','controller' => 'login', 'action' => 'dangtin'));

Router::connect('/get_usermember_name/*', array('plugin'=>'Usermembers','controller' => 'login', 'action' => 'get_usermember_name'));

Router::connect('/sua-thong-tin-mo-gian-hang.html', array('plugin'=>'Usermembers','controller' => 'usermembers', 'action' => 'suathongtingianhang'));

Router::connect('/sua-thong-tin-don-vi.html', array('plugin'=>'Usermembers','controller' => 'usermembers', 'action' => 'suathongtindonvi'));

Router::connect('/sua-thong-tin-thanh-vien-thuoc-don-vi.html', array('plugin'=>'Usermembers','controller' => 'usermembers', 'action' => 'suathongtinthanhvienthuocdonvi'));

Router::connect('/sua-thong-tin-thanh-vien-thuong.html', array('plugin'=>'Usermembers','controller' => 'usermembers', 'action' => 'suathongtinthanhvienthuong'));

Router::connect('/danh-sach-comment.html', array('plugin'=>'Usermembers','controller' => 'login', 'action' => 'danhsachcomment'));

Router::connect('/slide-show.html', array('plugin'=>'Usermembers','controller' => 'login', 'action' => 'slideshow'));
Router::connect('/them-moi-slide-show.html', array('plugin'=>'Usermembers','controller' => 'login', 'action' => 'addslideshow'));

Router::connect('/quan-ly-don-hang.html', array('plugin'=>'Usermembers','controller' => 'login', 'action' => 'quanlydonhang'));
Router::connect('/chi-tiet-don-hang/*', array('plugin'=>'Usermembers','controller' => 'login', 'action' => 'detail_email'));

Router::connect('/lich-su-mua-hang.html', array('plugin'=>'Usermembers','controller' => 'login', 'action' => 'lichsumuahang'));

Router::connect('/quen-mat-khau.html', array('plugin'=>'Usermembers','controller' => 'usermembers', 'action' => 'forgot_password'));


?>
