<?php


class NewsController extends AppController {
    public $name = 'News';
    public $uses = array('Setting','Comment','Code','Layout','News');
	public function beforeFilter() {
        parent::beforeFilter();       
        
    }
	
	public function nameparent($id=null) { 
		$nameparent = $this->Comment->find('all', array(
            'conditions' => array(
                'Comment.id' => $id,		
            )
        ));
        return $nameparent;
	}
	
    public function detail($id = null) {
		
		$detailNews = $this->News->findById($id);
		//echo DOMAIN;
      //  pr($detailNews['News']['domain']);die;
		
		if($_SERVER['SERVER_NAME'] == $detailNews['News']['domain']){
			
		    $this->set('detailNews', $detailNews);
		
			$listLayout = $this->Layout->find('first', array(
			'conditions' => array(
				'Layout.id' => $detailNews['News']['layout_id'],
			)));
			
			
			$this->layout = $listLayout['Layout']['name'];
			
			
			if(isset($_GET['aff_sub1'])){
				$aff = $_GET['aff_sub1'];
				
				$code = $this->Code->find('first', array(
					'conditions' => array( 
						'Code.name'=>$aff,
						'Code.news_id'=>$id,
					),
				));
				$macode = $code['Code']['code'];
				$this->Session->write('code_home',$macode);
			}
			
		 }else{
		 
		 die;
		
		 }
		$this->set('title_for_layout',$detailNews['News']['name']);
		$this->set('keywords_for_layout', $detailNews['News']['name']);
        $this->set('description_for_layout', $detailNews['News']['name']);
		



    }
 
}