<?php

class CommentController extends AppController {

    public $name = 'Comment';
    public $uses = array('Comment','News','Code');

  
    public function listcomment($id=null)
    {
		
		 $listcomment = $this->Comment->find('all', array(
            'conditions' => array(
           
                'Comment.status' => 1, 
				 'Comment.news_id LIKE' => '%'.'|'.$id.'|'.'%',
				'Comment.parent_id' => null, 
            ),
            'order' => 'Comment.pos ASC',
                ));
        return $listcomment;
		
    }
	
	    public function listcomment_child($id=null)
    {
		
		$listcomment = $this->Comment->find('all', array(
            'conditions' => array(
           
                'Comment.status' => 1, 
				'Comment.parent_id' => $id, 
            ),
            'order' => 'Comment.pos ASC',
                ));
        return $listcomment;
		
    }
	
	//success
	 public function success()
    { 
	 
	  
	    $listcode = $this->Code->find('first', array(
            'conditions' => array(
                'Code.status' => 1,
				'Code.id LIKE' => $this->Session->read('codeid'),
            ),
            'order' =>'Code.pos ASC','Code.modified DESC','limit' => 1
                ));
        return $listcode;
		 
		 
	} 
}
	