<?php

/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
    
       public function beforeFilter() {
	   
		if(isset($_GET['lang'])) {
            if($_GET['lang']=="vie") {
			     $this->Session->write('lang',"");	
			}
			elseif($_GET['lang']=="eng") {
			     $this->Session->write('lang','_eg');	
			}
			
			
		}
		else {
		  if($this->Session->check('lang'))
          {
            $this->Session->write('lang',$this->Session->read('lang'));
          }
          else
          {
            $this->Session->write('lang',"");
          }
           
		}
        
     //  echo $this->Session->read('lang'); die;
   
    }

	
	static public function validatePhone($mobilePhone)
 {
    if($mobilePhone){
      $lengthPhone = strlen($mobilePhone);
            if($lengthPhone < 6 || $lengthPhone > 20 || preg_match('/[^0-9_-]/',$mobilePhone) == 1){
                return false;
            }else{
                return true;
            }
        }
        return false;
 }
 
 
}