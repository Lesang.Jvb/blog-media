<?php
App::uses('AppHelper', 'View/Helper');


class HomeController extends AppController {


    public $name = 'Home';
    public $uses = array('Setting', 'News','Layout','Code','Order');

    public function beforeFilter() {
        parent::beforeFilter();
		 
        $setting = $this->Setting->find('first');
        $this->set('title_for_layout', $setting['Setting']['name']);
        $this->set('keywords_for_layout', $setting['Setting']['meta_key']);
        $this->set('description_for_layout', $setting['Setting']['meta_des']);
    }

	public function order(){
		$this->set('title_for_layout',' ');
		if ($this->request->is('post')) {
			
			
			$data['Order']['name'] = $_POST['name']; 
			$data['Order']['phone'] = $_POST['phone'];
			$data['Order']['address'] = $_POST['address'];
			$data['Order']['news_id'] = $_POST['product_id'];
			$data['Order']['timelog'] = time();
			
			$product_id = $_POST['product_id'];
			
			if(isset($_POST['aff_sub1'])){
				$aff = $_POST['aff_sub1'];
				
				$code = $this->Code->find('first', array(
					'conditions' => array( 
						'Code.name'=>$aff,
						'Code.news_id'=>$product_id,
					),
				));
				$macode = $code['Code']['code'];
				$this->Session->write('code_pixel',$macode);
			}
            if(isset($_POST['aff_sub1'])){
                $aff = $_POST['aff_sub1'];
            }else{
                $aff = 1;
            }
			
			$name = $_POST['name'];
			$phone = $_POST['phone'];
			$address = $_POST['address'];
			
			$product_name = $_POST['product_name'];
			$price = $_POST['price'];


			$this->Order->save($data['Order']);	

			$this->sendAPI($name,$phone,$address,$product_id, $product_name,$price,$aff);
			
			echo '<script language="javascript"> window.location.replace("'.DOMAIN.'success");</script>';	
 
		}
	}	
	
	public function sendAPI($name,$phone,$address,$product_id,$product_name,$price,$aff){

		//$quantity=$_POST['quantity'];
		$note = (empty($_POST['note'])) ? "" : $_POST['note'];
		
		// API send data
		$quantity = 1;
		$key ="Civi@2017Vtmgroup";
		$token = md5($key);
		 
		if($product_id)
		{
			
			$arrDataNews = array(
				"phone"	        => $phone,
				"name"	        => $name,
				"create_time"	=> time(),
                "pid"           => $aff,
				"address"       => $address,
				"quantity"	    => $quantity,
				"price"	        => $price,
				"product_id"	=> $product_id,
				"product_name"	=> $product_name,
				"token"	        => $token,
			);
			
			
			//print_r($_POST);die;
			$data_string = json_encode($arrDataNews);
			$service_url = 'http://sysmedia.vtmgroup.com.vn/get_api_data_form';
			$curl = curl_init($service_url);
	
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl, CURLOPT_POST, true);
			curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);
			$curl_response = curl_exec($curl);
			if ($curl_response === false) {
				$info = curl_getinfo($curl);
				curl_close($curl);
				die('error occured during curl exec. Additioanl info: ' . var_export($info));
			}
			curl_close($curl);
			$decoded = json_decode($curl_response);
			//	var_dump($decoded);die;
			if (isset($decoded->response->status) && $decoded->response->status == 'ERROR') {
				die('error occured: ' . $decoded->response->errormessage);
			}
		}
	}
	
	
	public function index($id = null) {

		$order = $this->Order->find('all', array(
		'conditions' => array( 
		),
		'order' => 'Order.id ASC',
		));
		$this->set('order', $order);
		//pr($order);die;
		 
		 
    } 
	 
		public function  success($id = null) {
	     
		 $this->layout='success';
	
	  
	
}
	
}